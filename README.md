# ItRuns JS Toolkit

This toolkit encapsulates logic and resources shared between environments, including the browser. The library can be included using either NPM or Bower.

### Using with NPM

This is a private module that is fetched from BitBucket.

```
   npm install --save git+ssh://git@bitbucket.org/itrunsinmyfamily/toolkit.git
```

Note, due to limitations in npm, changes to this module will not be detected by `npm update`. Instead, re-install the module using `npm install @itruns/toolkit` (must already exist in project's package.json).

### Using with Bower

```
bower install --save git+ssh://git@bitbucket.org/itrunsinmyfamily/toolkit.git
```

When loaded in the browser, the toolkit and its modules will all be located under the `ItRuns` namespace. Usage example:

```
<script src="/bower_components/itruns-toolkit/dist/itruns-toolkit.min.js"></script>

<script>
jQuery(function ($) {
    // true
    ItRuns.conditions.evaluate("example", "inq", ["this","is","an","example"])
});
</script>
```

## Useful Commands

  * Rebuild browser bundle: `npm run build`
  * Run tests: `npm test`
  * Run the linter: `npm run lint`
  * Update all itruns scoped dependencies: `npm run itruns:update`

## Release Instructions

It's important to properly version releases to ensure frontend and backend dependencies stay in sync. Use the
following process,

  1. Rebuild the bundke: `npm run build`
  2. Bump the package version: `npm version <something>` where 'something' depends on the severity of your change. For example, `patch`
  3. Update bower.json to match your version from step #2. Commit this change.
  4. Push your changes, including tags, to bitbucket: `git push --follow-tags`
  5. In the frontend project, upgrade the bower dependency for the toolkit if/when necessary.

