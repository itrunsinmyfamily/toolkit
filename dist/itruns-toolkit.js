(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("ItRuns", [], factory);
	else if(typeof exports === 'object')
		exports["ItRuns"] = factory();
	else
		root["ItRuns"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 23);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/* eslint no-underscore-dangle: "off" */

var RESPONSE_NONE = 'none';
var RESPONSE_VALUE = 'value';
var RESPONSE_UNKNOWN = 'unknown';
var RESPONSE_REFUSED = 'refused';
var RESPONSES = [RESPONSE_NONE, RESPONSE_VALUE, RESPONSE_UNKNOWN, RESPONSE_REFUSED];

exports.default = {
  RESPONSE_NONE: RESPONSE_NONE,
  RESPONSE_VALUE: RESPONSE_VALUE,
  RESPONSE_UNKNOWN: RESPONSE_UNKNOWN,
  RESPONSE_REFUSED: RESPONSE_REFUSED,
  RESPONSES: RESPONSES,

  RecordBundle: RecordBundle,
  RecordBundleRecord: RecordBundleRecord
};


function RecordBundle(bundle) {
  var _this = this;

  this.__records = {};
  Object.assign(this, bundle);
  Object.keys(this.__records).forEach(function (code) {
    _this.__records[code].forEach(function (record, i) {
      _this.__records[code][i] = new module.exports.default.RecordBundleRecord(record);
    });
  });
}

function RecordBundleRecord(record) {
  this.__changes = {};
  this.__responses = {};
  Object.assign(this, record);
}

RecordBundle.prototype.addRecord = function addRecord(code) {
  var source = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var responses = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  var records = this.__records[code] = this.__records[code] || [];
  var record = new module.exports.default.RecordBundleRecord(source);

  record.setResponses(responses, false);

  records.push(record);
  return record;
};

RecordBundle.prototype.getRecords = function getRecords(code) {
  return this.__records[code] || [];
};

RecordBundle.prototype.getAllRecords = function getAllRecords() {
  return Object.assign({}, this.__records);
};

RecordBundle.prototype.getChanges = function getChanges() {
  var _this2 = this;

  var changedrecords = {};

  Object.keys(this.__records).forEach(function (key) {
    var records = _this2.__records[key];
    var changedBundles = [];

    records.forEach(function (record) {
      if (record.isChanged()) {
        changedBundles.push(record);
      }
    });
    if (changedBundles.length > 0) {
      changedrecords[key] = changedBundles;
    }
  });

  return changedrecords;
};

RecordBundleRecord.prototype.setResponse = function setResponse(code, response, flagChanged) {
  if (RESPONSES.indexOf(response.responseType) < 0) {
    throw new Error('Invalid responseType ' + response.responseType + ' ' + ('in response ' + JSON.stringify(response)));
  }

  if (flagChanged !== false) {
    this.__changes[code] = true;
  }
  this.__responses[code] = response;
};

RecordBundleRecord.prototype.setValue = function setValue(code, value, flagChanged) {
  return this.setResponse(code, { responseType: RESPONSE_VALUE, value: value }, flagChanged);
};

RecordBundleRecord.prototype.setResponses = function setResponses(responses, flagChanged) {
  var _this3 = this;

  Object.keys(responses).forEach(function (code) {
    _this3.setResponse(code, responses[code], flagChanged);
  });
};

RecordBundleRecord.prototype.setValues = function setValues(values, flagChanged) {
  var _this4 = this;

  Object.keys(values).forEach(function (code) {
    _this4.setValue(code, values[code], flagChanged);
  });
};

RecordBundleRecord.prototype.getResponse = function getResponse(code) {
  return this.__responses[code];
};

RecordBundleRecord.prototype.getValue = function getValue(code) {
  var response = this.getResponse(code);

  if (response && response.responseType === RESPONSE_VALUE) {
    return response.value;
  }

  return undefined;
};

RecordBundleRecord.prototype.isChanged = function isChanged() {
  return Object.keys(this.__changes).length > 0;
};

RecordBundleRecord.prototype.getChangedResponses = function getChangedResponses() {
  var _this5 = this;

  var changed = {};

  Object.keys(this.__changes).forEach(function (code) {
    changed[code] = _this5.__responses[code];
  });

  return changed;
};

RecordBundleRecord.prototype.clearChanged = function clearChanged() {
  this.__changes = {};
};

RecordBundleRecord.prototype.getResponses = function getResponses() {
  return this.__responses;
};

RecordBundleRecord.prototype.getValues = function getValues() {
  var _this6 = this;

  var values = {};

  Object.keys(this.__responses).forEach(function (code) {
    var response = _this6.__responses[code];

    if (response.responseType === RESPONSE_VALUE) {
      values[code] = response.value;
    }
  });

  return values;
};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var degreeByRelation = exports.degreeByRelation = {
  proband: 0,
  parent: 1,
  child: 1,
  'parent.child': 1,
  'parent.parent': 2,
  'child.child': 2,
  'parent.child.child': 2,
  'parent.parent.child': 2,
  'parent.parent.parent': 3,
  'child.child.child': 3,
  'parent.parent.child.child': 3,
  'parent.child.child.child': 3,
  'parent.parent.parent.child': 3
};
var MAX_DEGREE = exports.MAX_DEGREE = 3;
var GENETIC_COUNSELING = exports.GENETIC_COUNSELING = 'genetic-counseling';
var EARLIER_SCREENING = exports.EARLIER_SCREENING = 'earlier-screening';
var PROBAND = exports.PROBAND = 0;
var FDR = exports.FDR = 1;
var SDR = exports.SDR = 2;

exports.default = {
  degreeByRelation: degreeByRelation,
  MAX_DEGREE: MAX_DEGREE
};

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _VALIDATORS, _TYPECHECKS, _EVALUATORS;

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var types = __webpack_require__(3).default;

var OP_EQ = 'eq';
var OP_NEQ = 'neq';
var OP_GT = 'gt';
var OP_LT = 'lt';
var OP_GTE = 'gte';
var OP_LTE = 'lte';
var OP_BTW = 'btw';
var OP_INQ = 'inq';
var OP_NIN = 'nin';
var OP_REGEX = 're';

var OPERATORS = [OP_EQ, OP_NEQ, OP_GT, OP_LT, OP_GTE, OP_LTE, OP_BTW, OP_INQ, OP_NIN, OP_REGEX];

var INDEXOF_NOT_FOUND = -1;

var OP_BTW_VAL_LEN = 2;
var OP_REGEX_VAL_LEN = 2;

// Validators check whether values match the appropriate shape for their operators
var VALIDATORS = (_VALIDATORS = {}, _defineProperty(_VALIDATORS, OP_EQ, types.isSimpleType), _defineProperty(_VALIDATORS, OP_NEQ, types.isSimpleType), _defineProperty(_VALIDATORS, OP_GT, types.isSimpleType), _defineProperty(_VALIDATORS, OP_LT, types.isSimpleType), _defineProperty(_VALIDATORS, OP_GTE, types.isSimpleType), _defineProperty(_VALIDATORS, OP_LTE, types.isSimpleType), _defineProperty(_VALIDATORS, OP_BTW, function (value) {
  return Array.isArray(value) && value.length === OP_BTW_VAL_LEN;
}), _defineProperty(_VALIDATORS, OP_INQ, Array.isArray), _defineProperty(_VALIDATORS, OP_NIN, Array.isArray), _defineProperty(_VALIDATORS, OP_REGEX, function (value) {
  if (!Array.isArray(value)) {
    return typeof value === 'string';
  }

  // Optionally support two-parameter array, [regexp, flags]
  return value.length === OP_REGEX_VAL_LEN && value.every(function (v) {
    return typeof v === 'string';
  });
}), _VALIDATORS);

// Typechecks ensure that subject and value conform to required types
var TYPECHECKS = (_TYPECHECKS = {}, _defineProperty(_TYPECHECKS, OP_EQ, typesMatch), _defineProperty(_TYPECHECKS, OP_NEQ, typesMatch), _defineProperty(_TYPECHECKS, OP_GT, typesMatch), _defineProperty(_TYPECHECKS, OP_LT, typesMatch), _defineProperty(_TYPECHECKS, OP_GTE, typesMatch), _defineProperty(_TYPECHECKS, OP_LTE, typesMatch), _defineProperty(_TYPECHECKS, OP_BTW, function (subject, value) {
  return typesMatch.apply(undefined, [subject].concat(_toConsumableArray(value)));
}), _defineProperty(_TYPECHECKS, OP_INQ, function (subject, value) {
  return typesMatch.apply(undefined, [subject].concat(_toConsumableArray(value)));
}), _defineProperty(_TYPECHECKS, OP_NIN, function (subject, value) {
  return typesMatch.apply(undefined, [subject].concat(_toConsumableArray(value)));
}), _defineProperty(_TYPECHECKS, OP_REGEX, function (subject) {
  return typeof subject === 'string';
}), _TYPECHECKS);

// Evaluators actually execute the comparison
var EVALUATORS = (_EVALUATORS = {}, _defineProperty(_EVALUATORS, OP_EQ, function (subject, value) {
  if (subject instanceof Date) {
    return subject.getTime() === value.getTime();
  }

  return subject === value;
}), _defineProperty(_EVALUATORS, OP_NEQ, function (subject, value) {
  if (subject instanceof Date) {
    return subject.getTime() !== value.getTime();
  }

  return subject !== value;
}), _defineProperty(_EVALUATORS, OP_GT, function (subject, value) {
  return subject > value;
}), _defineProperty(_EVALUATORS, OP_LT, function (subject, value) {
  return subject < value;
}), _defineProperty(_EVALUATORS, OP_GTE, function (subject, value) {
  return subject >= value;
}), _defineProperty(_EVALUATORS, OP_LTE, function (subject, value) {
  return subject <= value;
}), _defineProperty(_EVALUATORS, OP_BTW, function (subject, value) {
  return subject >= value[0] && subject <= value[1];
}), _defineProperty(_EVALUATORS, OP_INQ, function (subject, value) {
  return value.indexOf(subject) !== INDEXOF_NOT_FOUND;
}), _defineProperty(_EVALUATORS, OP_NIN, function (subject, value) {
  return value.indexOf(subject) === INDEXOF_NOT_FOUND;
}), _defineProperty(_EVALUATORS, OP_REGEX, function (subject, value) {
  // Optionally support two-parameter array, [regexp, flags]
  var re = Array.isArray(value) ? new (Function.prototype.bind.apply(RegExp, [null].concat(_toConsumableArray(value))))() : new RegExp(value);

  return re.test(subject);
}), _EVALUATORS);

exports.default = {
  OPERATORS: OPERATORS,
  VALIDATORS: VALIDATORS,
  TYPECHECKS: TYPECHECKS,
  EVALUATORS: EVALUATORS,

  validateSet: function validateSet(conditionSet) {
    var _this = this;

    return Object.keys(conditionSet).map(function (op) {
      return _this.validate(op, conditionSet[op]);
    }).filter(function (err) {
      return !!err;
    });
  },

  validate: function validate(op, value) {
    if (!VALIDATORS[op]) {
      return new Error('Invalid operator \'' + op + '\'');
    }
    if (!VALIDATORS[op](value)) {
      return new Error('Validation failure at ' + op + ' ' + value);
    }

    return undefined;
  },

  // Expects a subject and an object mapping operators to values
  evaluateSet: function evaluateSet(subject, conditions) {
    var _this2 = this;

    var failures = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

    var failed = Object.keys(conditions).filter(function (op) {
      return !_this2.evaluate(subject, op, conditions[op]);
    });

    failures.push.apply(failures, _toConsumableArray(failed));
    return failed.length === 0;
  },

  evaluate: function evaluate(subject, op, value) {
    var err = this.validate(op, value);

    if (err) {
      throw err;
    }

    if (subject === undefined) {
      return false;
    }

    if (!TYPECHECKS[op](subject, value)) {
      throw new Error('Typecheck failure at ' + subject + ' ' + op + ' ' + value);
    }

    return EVALUATORS[op](subject, value);
  }

  // Checks whether all parameters are the same type
};
function typesMatch() {
  for (var _len = arguments.length, params = Array(_len), _key = 0; _key < _len; _key++) {
    params[_key] = arguments[_key];
  }

  if (!params.length) {
    return true;
  }

  var type = _typeof(params[0]);

  if (type === 'number') {
    // Enforce not only that they are all numbers but that they are ints
    return params.every(Number.isInteger);
  }

  return params.every(function (p) {
    return (typeof p === 'undefined' ? 'undefined' : _typeof(p)) === type;
  });
}

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var TYPE_NUMERIC = 'numeric';
var TYPE_TEXT = 'text';
var TYPE_DATETIME = 'datetime';
var TYPES = [TYPE_NUMERIC, TYPE_TEXT, TYPE_DATETIME];

// eslint-disable-next-line import/prefer-default-export
var types = {
  TYPE_NUMERIC: TYPE_NUMERIC,
  TYPE_TEXT: TYPE_TEXT,
  TYPE_DATETIME: TYPE_DATETIME,
  TYPES: TYPES,

  getSimpleType: function getSimpleType(value) {
    var type = typeof value === 'undefined' ? 'undefined' : _typeof(value);

    if (type === 'string') {
      return TYPE_TEXT;
    }
    if (value instanceof Date) {
      return TYPE_DATETIME;
    }
    if (type === 'number' && Number.isInteger(value)) {
      return TYPE_NUMERIC;
    }

    return undefined;
  },

  isSimpleType: function isSimpleType(value) {
    return !!types.getSimpleType(value);
  }
};

exports.default = types;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var records = __webpack_require__(0).default;

exports.default = {
  calculate: calculate,
  calculateFamilyTree: calculateFamilyTree,
  calculateFamilyInfo: calculateFamilyInfo,
  calculateFamilyCancer: calculateFamilyCancer,
  calculateFamilyCancerInfo: calculateFamilyCancerInfo,
  calculatePersonal: calculatePersonal,
  calculateSocial: calculateSocial
};


function calculateConceptAttributes(currentPatient, results, healthCreated, member) {
  var patientInfo = {
    total: 0,
    score: 0
  };
  var restricted = ['Life', 'Smoking', 'Drinking', 'Patient'];
  var included = [];
  var HEALTH_SCORE_TOPICS = ['age-start'];
  var updatedResults = results;
  var patientRecords = currentPatient.getAllRecords();

  Object.keys(patientRecords).forEach(function (conceptId) {
    if (restricted.indexOf(conceptId) === -1) {
      patientRecords[conceptId].forEach(function (recordInstance) {
        patientInfo.total++;
        updatedResults.total++;
        included.push(recordInstance);
      });
    }
  });

  included.forEach(function (disease) {
    var ageStartResponse = disease.getResponse('age-start');

    if (ageStartResponse && ageStartResponse.responseType !== records.RESPONSE_NONE) {
      patientInfo.score++;
      updatedResults.score++;
    }

    if (disease.values) {
      var healthUpdated = disease.values.filter(function (l) {
        return HEALTH_SCORE_TOPICS.indexOf(l.attributeId) > -1;
      }).map(function (l) {
        return l.created;
      });

      if (healthUpdated.length) {
        healthCreated.push(healthUpdated.map(function (p) {
          return new Date(p);
        }).sort(function (dateOne, dateTwo) {
          return dateTwo.getTime() - dateOne.getTime();
        })[0]);
      }
    }
  });

  if (healthCreated.length) {
    updatedResults.updated = healthCreated.map(function (p) {
      return new Date(p);
    }).sort(function (dateOne, dateTwo) {
      return dateTwo.getTime() - dateOne.getTime();
    })[0];
  }
  updatedResults.members[member.patientId] = patientInfo;
  return updatedResults;
}

function convertPedigree(pedigree) {
  var patients = [];

  Object.keys(pedigree).forEach(function (patientId) {
    if (pedigree[patientId].PHR) {
      patients.push({ accountId: pedigree[patientId].accountId,
        patientId: pedigree[patientId].patientId,
        PHR: new records.RecordBundle(pedigree[patientId].PHR) });
    }
  });
  return patients;
}

function calculate(state) {
  var progressResult = {};
  var currentPatientBundle = new records.RecordBundle(state.pedigree[state.account.patientId].PHR);

  progressResult.familyTree = calculateFamilyTree(state.account.created);
  progressResult.familyInfo = calculateFamilyInfo(state.pedigree);
  progressResult.personalHealth = calculateFamilyCancer(state.account.created, state.ui);
  progressResult.familyHealth = calculateFamilyCancerInfo(state.pedigree, state.account.patientId, state.account.created);
  progressResult.personal = calculatePersonal(currentPatientBundle);
  progressResult.social = calculateSocial(currentPatientBundle);
  progressResult.score = progressResult.familyTree.score + progressResult.familyInfo.score + progressResult.personalHealth.score + progressResult.familyHealth.score + progressResult.personal.score + progressResult.social.score;
  progressResult.total = progressResult.familyTree.total + progressResult.familyInfo.total + progressResult.personalHealth.total + progressResult.familyHealth.total + progressResult.personal.total + progressResult.social.total;
  return progressResult;
}

function calculateFamilyTree(created) {
  return {
    updated: created,
    score: 7,
    total: 7
  };
}

function calculateFamilyInfo(pedigree) {
  var members = convertPedigree(pedigree);
  var results = {
    updated: null,
    score: 0,
    total: 0,
    members: {}
  };
  var FAMILYINFO_SCORE_TOPICS = ['first-name', 'last-name', 'birth-year', 'death-year'];
  var familyInfoCreated = [];

  members.forEach(function (member) {
    var patientInfo = {
      total: 0,
      score: 0
    };

    var life = member.PHR.getRecords('Life')[0];
    var firstNameResponse = life.getResponse('first-name');
    var lastNameResponse = life.getResponse('last-name');
    var birthYearResponse = life.getResponse('birth-year');
    var deathYearResponse = life.getResponse('death-year');
    var deathYearValue = life.getValue('death-year');
    var aliveStatus = !(deathYearResponse && deathYearResponse.responseType !== records.RESPONSE_NONE);

    results.total++;
    patientInfo.total++;
    if (firstNameResponse && firstNameResponse.responseType !== records.RESPONSE_NONE || lastNameResponse && lastNameResponse.responseType !== records.RESPONSE_NONE) {
      results.score++;
      patientInfo.score++;
    }

    results.total++;
    patientInfo.total++;
    if (aliveStatus) {
      if (birthYearResponse && birthYearResponse.responseType !== records.RESPONSE_NONE) {
        results.score++;
        patientInfo.score++;
      }
    } else if (birthYearResponse && birthYearResponse.responseType !== records.RESPONSE_NONE && deathYearValue) {
      results.score++;
      patientInfo.score++;
    }

    results.members[member.patientId] = patientInfo;

    if (life.values) {
      var familyInfoUpdated = life.values.filter(function (l) {
        return FAMILYINFO_SCORE_TOPICS.indexOf(l.attributeId) > -1;
      }).map(function (l) {
        return l.created;
      });

      if (familyInfoUpdated.length) {
        familyInfoCreated.push(familyInfoUpdated.map(function (p) {
          return new Date(p);
        }).sort(function (dateOne, dateTwo) {
          return dateTwo.getTime() - dateOne.getTime();
        })[0]);
      }
    }
  });

  if (familyInfoCreated.length) {
    results.updated = familyInfoCreated.map(function (p) {
      return new Date(p);
    }).sort(function (dateOne, dateTwo) {
      return dateTwo.getTime() - dateOne.getTime();
    })[0];
  }
  return results;
}

function calculateFamilyCancer(created, ui) {
  var results = {
    updated: null,
    score: 0,
    total: 0
  };

  if (ui.confirmedFamilyHealthHistory) {
    results.score++;
    results.total++;
    results.updated = created;
  }
  return results;
}

function calculateFamilyCancerInfo(statePedigree, patientId, created) {
  var pedigree = convertPedigree(statePedigree);
  var results = {
    updated: created,
    score: 0,
    total: 0,
    members: {}
  };

  var familyHealthScore = pedigree.filter(function (p) {
    return p.patientId !== patientId;
  }).map(function (member) {
    return calculateConceptAttributes(member.PHR, results, [], member);
  });

  if (!familyHealthScore.length) {
    return results;
  }
  return familyHealthScore[0];
}

function calculatePersonal(bundlePatient) {
  var PERSONAL_SCORE_TOPICS = ['height', 'weight', 'eye-color', 'hair-color', 'skin-tone', 'handedness', 'blood-type', 'rh-type'];
  var results = {
    updated: null,
    score: 0,
    total: PERSONAL_SCORE_TOPICS.length
  };
  var lifeRecord = bundlePatient.getRecords('Life')[0];

  if (!lifeRecord) {
    return results;
  }
  PERSONAL_SCORE_TOPICS.forEach(function (key) {
    var response = lifeRecord.getResponse(key);

    if (response && response.responseType !== records.RESPONSE_NONE) {
      results.score++;
    }
  });
  if (lifeRecord.values) {
    var personalUpdated = lifeRecord.values.filter(function (l) {
      return PERSONAL_SCORE_TOPICS.indexOf(l.attributeId) > -1;
    }).map(function (l) {
      return l.created;
    });

    if (personalUpdated.length) {
      results.updated = personalUpdated.map(function (p) {
        return new Date(p);
      }).sort(function (dateOne, dateTwo) {
        return dateTwo.getTime() - dateOne.getTime();
      })[0];
    }
  }

  return results;
}

function calculateSocial(bundlePatient) {
  var SOCIAL_SCORE_TOPICS = ['zip-code', 'frequency', 'smoking-values'];
  var SMOKING_TOPICS = ['status', 'frequency', 'age-start', 'age-end'];
  var socialUpdated = [];
  var results = {
    updated: null,
    score: 0,
    total: SOCIAL_SCORE_TOPICS.length
  };
  var lifeRecord = bundlePatient.getRecords('Life')[0];
  var smokingRecord = bundlePatient.getRecords('Smoking')[0];
  var drinkingRecord = bundlePatient.getRecords('Drinking')[0];

  if (drinkingRecord) {
    var response = drinkingRecord.getResponse('frequency');

    if (response && response.responseType !== records.RESPONSE_NONE) {
      results.score++;
    }

    if (drinkingRecord.values) {
      var drinkingCreated = drinkingRecord.values.map(function (l) {
        return l.created;
      });

      if (drinkingCreated.length) {
        socialUpdated.push(drinkingCreated);
      }
    }
  }

  if (lifeRecord) {
    var _response = lifeRecord.getResponse('zip-code');

    if (_response && _response.responseType !== records.RESPONSE_NONE) {
      results.score++;
    }
    if (lifeRecord.values) {
      var lifeCreated = lifeRecord.values.filter(function (l) {
        return SOCIAL_SCORE_TOPICS.indexOf(l.attributeId) > -1;
      }).map(function (l) {
        return l.created;
      });

      if (lifeCreated.length) {
        socialUpdated.push(lifeCreated);
      }
    }
  }

  if (smokingRecord) {
    var _response2 = smokingRecord.getResponse('status');

    if (_response2 && _response2.responseType !== records.RESPONSE_NONE) {
      if (_response2.responseType === records.RESPONSE_VALUE) {
        var status = smokingRecord.getValue('status');

        results.score++;
        if (status === 'current') {
          results.total++;
          var frequencyResponse = smokingRecord.getResponse('frequency');

          if (frequencyResponse && frequencyResponse.responseType !== records.RESPONSE_NONE) {
            if (frequencyResponse.responseType === records.RESPONSE_VALUE) {
              var frequency = smokingRecord.getValue('frequency');

              if (frequency === 'everyday' || frequency === 'some-days') {
                var ageResponse = smokingRecord.getResponse('age-start');

                if (ageResponse && ageResponse.responseType !== records.RESPONSE_NONE) {
                  results.score++;
                }
              }
            } else {
              results.score++;
            }
          }
        } else if (status === 'resolved') {
          results.total++;
          var ageStartResponse = smokingRecord.getResponse('age-start');
          var ageEndResponse = smokingRecord.getResponse('age-end');

          if (ageStartResponse && ageStartResponse.responseType !== records.RESPONSE_NONE) {
            results.score++;
          }
          results.total++;
          if (ageEndResponse && ageEndResponse.responseType !== records.RESPONSE_NONE) {
            results.score++;
          }
        }
      } else {
        results.score++;
      }
    }
    if (smokingRecord.values) {
      var smokingCreated = smokingRecord.values.filter(function (l) {
        return SMOKING_TOPICS.indexOf(l.attributeId) > -1;
      }).map(function (l) {
        return l.created;
      });

      if (smokingCreated.length) {
        socialUpdated.push(smokingCreated);
      }
    }
  }

  if (socialUpdated.length) {
    results.updated = socialUpdated.map(function (p) {
      return new Date(p);
    }).sort(function (dateOne, dateTwo) {
      return dateTwo.getTime() - dateOne.getTime();
    })[0];
  }
  return results;
}

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
// eslint-disable-next-line import/prefer-default-export
var Filters = exports.Filters = {
  trim: function trim(value) {
    return typeof value === 'string' ? value.trim() : value;
  },
  parseInt: function (_parseInt) {
    function parseInt(_x) {
      return _parseInt.apply(this, arguments);
    }

    parseInt.toString = function () {
      return _parseInt.toString();
    };

    return parseInt;
  }(function (value) {
    return parseInt(value, 10);
  }),
  parseFloat: function (_parseFloat) {
    function parseFloat(_x2) {
      return _parseFloat.apply(this, arguments);
    }

    parseFloat.toString = function () {
      return _parseFloat.toString();
    };

    return parseFloat;
  }(function (value) {
    return parseFloat(value);
  }),
  toUpperCase: function toUpperCase(value) {
    return typeof value === 'string' ? value.toUpperCase() : value;
  },
  toLowerCase: function toLowerCase(value) {
    return typeof value === 'string' ? value.toLowerCase() : value;
  },
  isSet: function isSet(value) {
    return !!value;
  },
  isNumeric: function isNumeric(value) {
    return typeof value === 'number' && !Number.isNaN(value);
  },
  isString: function isString(value) {
    return typeof value === 'string';
  },
  isInteger: function isInteger(value) {
    return Filters.isNumeric(value) && Number.isInteger(value);
  },
  getCurrentYear: function getCurrentYear() {
    return new Date().getUTCFullYear();
  }
};

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Parser = __webpack_require__(8).Parser;

var Inquiry = function () {
  function Inquiry(topic) {
    var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var filters = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

    _classCallCheck(this, Inquiry);

    this.topic = topic;
    this.state = state;
    this.filters = filters;

    this.type = topic.type;
    this.response = undefined;
    this.actions = undefined;
  }

  _createClass(Inquiry, [{
    key: 'filterResponse',
    value: function filterResponse(response) {
      var _this = this;

      this.topic.filters.forEach(function (filter) {
        // eslint-disable-next-line no-param-reassign
        response = _this.eval(filter, { response: response });
      });

      return response;
    }
  }, {
    key: 'validateResponse',
    value: function validateResponse(response) {
      if (this.type === 'choice') {
        var option = this.topic.options.find(function (o) {
          return o.id === response;
        });

        if (!option) {
          return 'Invalid option: ' + response;
        }
      }

      if (!this.topic.validate) {
        return null;
      }

      var result = this.eval(this.topic.validate, { response: response });

      if (!result) {
        return this.topic.validate;
      }

      return null;
    }
  }, {
    key: 'handleResponse',
    value: function handleResponse(response) {
      var _this2 = this;

      var actions = [];

      if (this.topic.actions) {
        actions.push.apply(actions, _toConsumableArray(this.topic.actions));
      }

      if (this.type === 'choice') {
        var option = this.topic.options.find(function (o) {
          return o.id === response;
        });

        if (option.actions) {
          actions.push.apply(actions, _toConsumableArray(option.actions));
        }
      }

      this.response = response;

      this.actions = actions.filter(function (a) {
        return !a.if || _this2.eval(a.if);
      }).map(function (a) {
        return _this2.interpolateAll(a);
      });

      return this.actions;
    }
  }, {
    key: 'interpolateAll',
    value: function interpolateAll(obj) {
      var _this3 = this;

      var result = {};

      Object.keys(obj).forEach(function (k) {
        if (obj[k] === null) {
          result[k] = obj[k];
          return;
        }
        switch (_typeof(obj[k])) {
          case 'object':
            result[k] = _this3.interpolateAll(obj[k]);
            break;
          case 'string':
            result[k] = _this3.interpolate(obj[k]);
            break;
          default:
            result[k] = obj[k];
        }
      });

      return result;
    }
  }, {
    key: 'eval',
    value: function _eval(expression, additionalParams) {
      var params = Object.assign({}, this.state, this.filters, { response: this.response }, additionalParams);

      return Parser.evaluate(expression, params);
    }
  }, {
    key: 'interpolate',
    value: function interpolate(input) {
      var _this4 = this;

      if (!input) {
        return input;
      }

      var result = input;
      var matches = input.match(/{{([\s\S]*?)}}/g);

      if (!matches) {
        return result;
      }

      matches.forEach(function (match) {
        var expression = match.replace(/^{{([\s\S]+)}}$/, '$1');

        result = result.replace(match, _this4.eval(expression));
      });

      return result;
    }
  }, {
    key: 'question',
    get: function get() {
      return this.interpolate(this.topic.question);
    }
  }, {
    key: 'content',
    get: function get() {
      return this.interpolate(this.topic.content);
    }
  }, {
    key: 'options',
    get: function get() {
      var _this5 = this;

      if (Array.isArray(this.topic.options)) {
        return this.topic.options.map(function (o) {
          return Object.assign({}, o, { label: _this5.interpolate(o.label) });
        });
      }

      return this.topic.options;
    }
  }]);

  return Inquiry;
}();

exports.default = Inquiry;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var records = __webpack_require__(0).default;
var progress = __webpack_require__(4).default;
var mapping = __webpack_require__(18);

var getAge = function getAge(dYear, dMonth, dDay, bYear, bMonth, bDay) {
  var today = new Date();
  var age = null;
  var monthDiff = void 0;
  var dayCheck = void 0;

  if (bYear) {
    age = dYear ? dYear - bYear : today.getFullYear() - bYear;
    if (bMonth && bDay) {
      if (dMonth && dDay) {
        monthDiff = dMonth - bMonth;
        dayCheck = dDay < bDay;
      } else {
        monthDiff = today.getMonth() + 1 - bMonth;
        dayCheck = today.getDate() < bDay;
      }

      if (monthDiff <= 0 && dayCheck) {
        age--;
      }
    }
  }
  return age;
};

var getBirthDate = function getBirthDate(bYear, bMonth, bDay) {
  if (bYear && bMonth && bDay) {
    var bDate = new Date(bYear, bMonth - 1, bDay + 1);

    return bDate;
  }
  return 'ASKU';
};

var getConditions = function getConditions(currentPatient) {
  var restricted = ['Life', 'Smoking', 'Drinking', 'Patient'];
  var conditions = [];
  var included = [];
  var patientRecords = currentPatient.getAllRecords();

  Object.keys(patientRecords).forEach(function (conceptId) {
    if (restricted.indexOf(conceptId) === -1) {
      patientRecords[conceptId].forEach(function (recordInstance) {
        included.push(recordInstance);
      });
    }
  });

  included.forEach(function (disease) {
    var ageStartValue = disease.getValue('age-start');
    var status = disease.getResponse('status');
    var condition = {
      code: disease.conceptId,
      onsetAge: ageStartValue,
      status: status
    };

    conditions.push(condition);
  });

  return conditions;
};
var personProgress = function personProgress(pedigree, member, proband) {
  var familyInfo = progress.calculateFamilyInfo(pedigree);
  var infoProgress = familyInfo.members[member.patientId];
  var PHR = new records.RecordBundle(member.PHR);
  var percentMax = 100;
  var percentMin = 0;
  var result = null;

  if (member.patientId === proband) {
    var personalInfo = progress.calculatePersonal(PHR);
    var socialInfo = progress.calculateSocial(PHR);
    var score = personalInfo.score + socialInfo.score + infoProgress.score;
    var total = personalInfo.total + socialInfo.total + infoProgress.total;

    result = Math.round(score / total * percentMax);
  } else {
    var healthInfo = progress.calculateFamilyCancerInfo(pedigree, null, null);
    var healthProgress = healthInfo.members[member.patientId];
    var _score = infoProgress.score + healthProgress.score;
    var _total = infoProgress.total + healthProgress.total;

    result = Math.round(_score / _total * percentMax);
  }

  if (result === percentMax) {
    return 'complete';
  } else if (result === percentMin) {
    return 'health-unknown';
  }

  return 'partial';
};

var personInfo = function personInfo(patient) {
  var PHR = new records.RecordBundle(patient.PHR);
  var Life = PHR.getRecords('Life')[0];
  var deathYearResponse = Life.getResponse('death-year');
  var deathYear = Life.getValue('death-year');
  var deathMonth = Life.getValue('death-month');
  var deathDay = Life.getValue('death-day');
  var birthYear = Life.getValue('birth-year');
  var birthMonth = Life.getValue('birth-month');
  var birthDay = Life.getValue('birth-day');
  var age = getAge(deathYear, deathMonth, deathDay, birthYear, birthMonth, birthDay);
  var conditions = getConditions(PHR);

  return {
    date: Life.modified,
    name: [Life.getValue('first-name'), Life.getValue('last-name')].join(' ').trim(),
    gender: Life.getValue('gender'),
    birthDate: getBirthDate(birthYear, birthMonth, birthDay),
    birthTime: [birthYear, birthMonth, birthDay].join('').trim(),
    deceased: !!(deathYearResponse && deathYearResponse.responseType !== records.RESPONSE_NONE),
    age: age,
    conditions: conditions
  };
};

var ageInfo = function ageInfo(member) {
  var age = personInfo(member).age;

  if (!age) return 'ASKU';
  var ageData = {
    value: age,
    unit: 'years',
    system: 'http://unitsofmeasure.org'
  };

  return ageData;
};
var familySides = function familySides(pedigree, probandId) {
  var relations = pedigree[probandId].relations;
  var family = {
    maternal: [],
    paternal: []
  };

  if (!relations.parent) return family;
  relations.parent.forEach(function (parent) {
    var data = pedigree[parent];
    var display = personInfo(data).gender;

    if (display === 'male') {
      family.paternal.push(parent);
      Object.keys(data.relations).forEach(function (relation) {
        if (relation.startsWith('parent')) {
          var _family$paternal;

          (_family$paternal = family.paternal).push.apply(_family$paternal, _toConsumableArray(data.relations[relation]));
        }
      });
      return;
    }

    family.maternal.push(parent);
    Object.keys(data.relations).forEach(function (relation) {
      if (relation.startsWith('parent')) {
        var _family$maternal;

        (_family$maternal = family.maternal).push.apply(_family$maternal, _toConsumableArray(data.relations[relation]));
      }
    });
  });

  return family;
};
var relationInfo = function relationInfo(pedigree, probandId, member) {
  var relations = pedigree[probandId].relations;
  var relationsMap = mapping.relationCodes;
  var relationData = {
    coding: [{
      system: 'http://hl7.org/fhir/v3/RoleCode',
      code: 'EXT',
      display: ''
    }]
  };

  if (!relations) return relationData;
  var familySidesGroup = familySides(pedigree, probandId);
  var relationPath = 'proband';

  Object.keys(relations).forEach(function (relation) {
    if (relations[relation].indexOf(member.patientId) > -1) {
      relationPath = relation;
      return relation;
    }
    return relationPath;
  });

  var gender = personInfo(member).gender;
  var relationship = relationsMap[relationPath][gender];
  var relationCode = null;

  if (familySidesGroup.maternal.includes(member.patientId)) {
    relationCode = relationship.code.maternal;
  } else {
    relationCode = relationship.code.paternal;
  }
  relationData.coding[0].code = relationCode;
  relationData.coding[0].display = relationship.display;
  return relationData;
};

module.exports = {
  personInfo: personInfo,
  personProgress: personProgress,
  relationInfo: relationInfo,
  ageInfo: ageInfo,
  getAge: getAge
};

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
	 true ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global.exprEval = factory());
}(this, (function () { 'use strict';

var INUMBER = 'INUMBER';
var IOP1 = 'IOP1';
var IOP2 = 'IOP2';
var IOP3 = 'IOP3';
var IVAR = 'IVAR';
var IFUNCALL = 'IFUNCALL';
var IEXPR = 'IEXPR';
var IMEMBER = 'IMEMBER';

function Instruction(type, value) {
  this.type = type;
  this.value = (value !== undefined && value !== null) ? value : 0;
}

Instruction.prototype.toString = function () {
  switch (this.type) {
    case INUMBER:
    case IOP1:
    case IOP2:
    case IOP3:
    case IVAR:
      return this.value;
    case IFUNCALL:
      return 'CALL ' + this.value;
    case IMEMBER:
      return '.' + this.value;
    default:
      return 'Invalid Instruction';
  }
};

function unaryInstruction(value) {
  return new Instruction(IOP1, value);
}

function binaryInstruction(value) {
  return new Instruction(IOP2, value);
}

function ternaryInstruction(value) {
  return new Instruction(IOP3, value);
}

function simplify(tokens, unaryOps, binaryOps, ternaryOps, values) {
  var nstack = [];
  var newexpression = [];
  var n1, n2, n3;
  var f;
  for (var i = 0; i < tokens.length; i++) {
    var item = tokens[i];
    var type = item.type;
    if (type === INUMBER) {
      nstack.push(item);
    } else if (type === IVAR && values.hasOwnProperty(item.value)) {
      item = new Instruction(INUMBER, values[item.value]);
      nstack.push(item);
    } else if (type === IOP2 && nstack.length > 1) {
      n2 = nstack.pop();
      n1 = nstack.pop();
      f = binaryOps[item.value];
      item = new Instruction(INUMBER, f(n1.value, n2.value));
      nstack.push(item);
    } else if (type === IOP3 && nstack.length > 2) {
      n3 = nstack.pop();
      n2 = nstack.pop();
      n1 = nstack.pop();
      if (item.value === '?') {
        nstack.push(n1.value ? n2.value : n3.value);
      } else {
        f = ternaryOps[item.value];
        item = new Instruction(INUMBER, f(n1.value, n2.value, n3.value));
        nstack.push(item);
      }
    } else if (type === IOP1 && nstack.length > 0) {
      n1 = nstack.pop();
      f = unaryOps[item.value];
      item = new Instruction(INUMBER, f(n1.value));
      nstack.push(item);
    } else if (type === IEXPR) {
      while (nstack.length > 0) {
        newexpression.push(nstack.shift());
      }
      newexpression.push(new Instruction(IEXPR, simplify(item.value, unaryOps, binaryOps, ternaryOps, values)));
    } else if (type === IMEMBER && nstack.length > 0) {
      n1 = nstack.pop();
      nstack.push(new Instruction(INUMBER, n1.value[item.value]));
    } else {
      while (nstack.length > 0) {
        newexpression.push(nstack.shift());
      }
      newexpression.push(item);
    }
  }
  while (nstack.length > 0) {
    newexpression.push(nstack.shift());
  }
  return newexpression;
}

function substitute(tokens, variable, expr) {
  var newexpression = [];
  for (var i = 0; i < tokens.length; i++) {
    var item = tokens[i];
    var type = item.type;
    if (type === IVAR && item.value === variable) {
      for (var j = 0; j < expr.tokens.length; j++) {
        var expritem = expr.tokens[j];
        var replitem;
        if (expritem.type === IOP1) {
          replitem = unaryInstruction(expritem.value);
        } else if (expritem.type === IOP2) {
          replitem = binaryInstruction(expritem.value);
        } else if (expritem.type === IOP3) {
          replitem = ternaryInstruction(expritem.value);
        } else {
          replitem = new Instruction(expritem.type, expritem.value);
        }
        newexpression.push(replitem);
      }
    } else if (type === IEXPR) {
      newexpression.push(new Instruction(IEXPR, substitute(item.value, variable, expr)));
    } else {
      newexpression.push(item);
    }
  }
  return newexpression;
}

function evaluate(tokens, expr, values) {
  var nstack = [];
  var n1, n2, n3;
  var f;
  for (var i = 0; i < tokens.length; i++) {
    var item = tokens[i];
    var type = item.type;
    if (type === INUMBER) {
      nstack.push(item.value);
    } else if (type === IOP2) {
      n2 = nstack.pop();
      n1 = nstack.pop();
      f = expr.binaryOps[item.value];
      nstack.push(f(n1, n2));
    } else if (type === IOP3) {
      n3 = nstack.pop();
      n2 = nstack.pop();
      n1 = nstack.pop();
      if (item.value === '?') {
        nstack.push(evaluate(n1 ? n2 : n3, expr, values));
      } else {
        f = expr.ternaryOps[item.value];
        nstack.push(f(n1, n2, n3));
      }
    } else if (type === IVAR) {
      if (item.value in expr.functions) {
        nstack.push(expr.functions[item.value]);
      } else {
        var v = values[item.value];
        if (v !== undefined) {
          nstack.push(v);
        } else {
          throw new Error('undefined variable: ' + item.value);
        }
      }
    } else if (type === IOP1) {
      n1 = nstack.pop();
      f = expr.unaryOps[item.value];
      nstack.push(f(n1));
    } else if (type === IFUNCALL) {
      var argCount = item.value;
      var args = [];
      while (argCount-- > 0) {
        args.unshift(nstack.pop());
      }
      f = nstack.pop();
      if (f.apply && f.call) {
        nstack.push(f.apply(undefined, args));
      } else {
        throw new Error(f + ' is not a function');
      }
    } else if (type === IEXPR) {
      nstack.push(item.value);
    } else if (type === IMEMBER) {
      n1 = nstack.pop();
      nstack.push(n1[item.value]);
    } else {
      throw new Error('invalid Expression');
    }
  }
  if (nstack.length > 1) {
    throw new Error('invalid Expression (parity)');
  }
  return nstack[0];
}

function expressionToString(tokens, toJS) {
  var nstack = [];
  var n1, n2, n3;
  var f;
  for (var i = 0; i < tokens.length; i++) {
    var item = tokens[i];
    var type = item.type;
    if (type === INUMBER) {
      if (typeof item.value === 'number' && item.value < 0) {
        nstack.push('(' + item.value + ')');
      } else {
        nstack.push(escapeValue(item.value));
      }
    } else if (type === IOP2) {
      n2 = nstack.pop();
      n1 = nstack.pop();
      f = item.value;
      if (toJS) {
        if (f === '^') {
          nstack.push('Math.pow(' + n1 + ', ' + n2 + ')');
        } else if (f === 'and') {
          nstack.push('(!!' + n1 + ' && !!' + n2 + ')');
        } else if (f === 'or') {
          nstack.push('(!!' + n1 + ' || !!' + n2 + ')');
        } else if (f === '||') {
          nstack.push('(String(' + n1 + ') + String(' + n2 + '))');
        } else if (f === '==') {
          nstack.push('(' + n1 + ' === ' + n2 + ')');
        } else if (f === '!=') {
          nstack.push('(' + n1 + ' !== ' + n2 + ')');
        } else {
          nstack.push('(' + n1 + ' ' + f + ' ' + n2 + ')');
        }
      } else {
        nstack.push('(' + n1 + ' ' + f + ' ' + n2 + ')');
      }
    } else if (type === IOP3) {
      n3 = nstack.pop();
      n2 = nstack.pop();
      n1 = nstack.pop();
      f = item.value;
      if (f === '?') {
        nstack.push('(' + n1 + ' ? ' + n2 + ' : ' + n3 + ')');
      } else {
        throw new Error('invalid Expression');
      }
    } else if (type === IVAR) {
      nstack.push(item.value);
    } else if (type === IOP1) {
      n1 = nstack.pop();
      f = item.value;
      if (f === '-' || f === '+') {
        nstack.push('(' + f + n1 + ')');
      } else if (toJS) {
        if (f === 'not') {
          nstack.push('(' + '!' + n1 + ')');
        } else if (f === '!') {
          nstack.push('fac(' + n1 + ')');
        } else {
          nstack.push(f + '(' + n1 + ')');
        }
      } else if (f === '!') {
        nstack.push('(' + n1 + '!)');
      } else {
        nstack.push('(' + f + ' ' + n1 + ')');
      }
    } else if (type === IFUNCALL) {
      var argCount = item.value;
      var args = [];
      while (argCount-- > 0) {
        args.unshift(nstack.pop());
      }
      f = nstack.pop();
      nstack.push(f + '(' + args.join(', ') + ')');
    } else if (type === IMEMBER) {
      n1 = nstack.pop();
      nstack.push(n1 + '.' + item.value);
    } else if (type === IEXPR) {
      nstack.push('(' + expressionToString(item.value, toJS) + ')');
    } else {
      throw new Error('invalid Expression');
    }
  }
  if (nstack.length > 1) {
    throw new Error('invalid Expression (parity)');
  }
  return String(nstack[0]);
}

function escapeValue(v) {
  if (typeof v === 'string') {
    return JSON.stringify(v).replace(/\u2028/g, '\\u2028').replace(/\u2029/g, '\\u2029');
  }
  return v;
}

function contains(array, obj) {
  for (var i = 0; i < array.length; i++) {
    if (array[i] === obj) {
      return true;
    }
  }
  return false;
}

function getSymbols(tokens, symbols, options) {
  options = options || {};
  var withMembers = !!options.withMembers;
  var prevVar = null;

  for (var i = 0; i < tokens.length; i++) {
    var item = tokens[i];
    if (item.type === IVAR && !contains(symbols, item.value)) {
      if (!withMembers) {
        symbols.push(item.value);
      } else if (prevVar !== null) {
        if (!contains(symbols, prevVar)) {
          symbols.push(prevVar);
        }
        prevVar = item.value;
      } else {
        prevVar = item.value;
      }
    } else if (item.type === IMEMBER && withMembers && prevVar !== null) {
      prevVar += '.' + item.value;
    } else if (item.type === IEXPR) {
      getSymbols(item.value, symbols, options);
    } else if (prevVar !== null) {
      if (!contains(symbols, prevVar)) {
        symbols.push(prevVar);
      }
      prevVar = null;
    }
  }

  if (prevVar !== null && !contains(symbols, prevVar)) {
    symbols.push(prevVar);
  }
}

function Expression(tokens, parser) {
  this.tokens = tokens;
  this.parser = parser;
  this.unaryOps = parser.unaryOps;
  this.binaryOps = parser.binaryOps;
  this.ternaryOps = parser.ternaryOps;
  this.functions = parser.functions;
}

Expression.prototype.simplify = function (values) {
  values = values || {};
  return new Expression(simplify(this.tokens, this.unaryOps, this.binaryOps, this.ternaryOps, values), this.parser);
};

Expression.prototype.substitute = function (variable, expr) {
  if (!(expr instanceof Expression)) {
    expr = this.parser.parse(String(expr));
  }

  return new Expression(substitute(this.tokens, variable, expr), this.parser);
};

Expression.prototype.evaluate = function (values) {
  values = values || {};
  return evaluate(this.tokens, this, values);
};

Expression.prototype.toString = function () {
  return expressionToString(this.tokens, false);
};

Expression.prototype.symbols = function (options) {
  options = options || {};
  var vars = [];
  getSymbols(this.tokens, vars, options);
  return vars;
};

Expression.prototype.variables = function (options) {
  options = options || {};
  var vars = [];
  getSymbols(this.tokens, vars, options);
  var functions = this.functions;
  return vars.filter(function (name) {
    return !(name in functions);
  });
};

Expression.prototype.toJSFunction = function (param, variables) {
  var expr = this;
  var f = new Function(param, 'with(this.functions) with (this.ternaryOps) with (this.binaryOps) with (this.unaryOps) { return ' + expressionToString(this.simplify(variables).tokens, true) + '; }'); // eslint-disable-line no-new-func
  return function () {
    return f.apply(expr, arguments);
  };
};

var TEOF = 'TEOF';
var TOP = 'TOP';
var TNUMBER = 'TNUMBER';
var TSTRING = 'TSTRING';
var TPAREN = 'TPAREN';
var TCOMMA = 'TCOMMA';
var TNAME = 'TNAME';

function Token(type, value, index) {
  this.type = type;
  this.value = value;
  this.index = index;
}

Token.prototype.toString = function () {
  return this.type + ': ' + this.value;
};

function TokenStream(parser, expression) {
  this.pos = 0;
  this.current = null;
  this.unaryOps = parser.unaryOps;
  this.binaryOps = parser.binaryOps;
  this.ternaryOps = parser.ternaryOps;
  this.consts = parser.consts;
  this.expression = expression;
  this.savedPosition = 0;
  this.savedCurrent = null;
  this.options = parser.options;
}

TokenStream.prototype.newToken = function (type, value, pos) {
  return new Token(type, value, pos != null ? pos : this.pos);
};

TokenStream.prototype.save = function () {
  this.savedPosition = this.pos;
  this.savedCurrent = this.current;
};

TokenStream.prototype.restore = function () {
  this.pos = this.savedPosition;
  this.current = this.savedCurrent;
};

TokenStream.prototype.next = function () {
  if (this.pos >= this.expression.length) {
    return this.newToken(TEOF, 'EOF');
  }

  if (this.isWhitespace() || this.isComment()) {
    return this.next();
  } else if (this.isRadixInteger() ||
      this.isNumber() ||
      this.isOperator() ||
      this.isString() ||
      this.isParen() ||
      this.isComma() ||
      this.isNamedOp() ||
      this.isConst() ||
      this.isName()) {
    return this.current;
  } else {
    this.parseError('Unknown character "' + this.expression.charAt(this.pos) + '"');
  }
};

TokenStream.prototype.isString = function () {
  var r = false;
  var startPos = this.pos;
  var quote = this.expression.charAt(startPos);

  if (quote === '\'' || quote === '"') {
    var index = this.expression.indexOf(quote, startPos + 1);
    while (index >= 0 && this.pos < this.expression.length) {
      this.pos = index + 1;
      if (this.expression.charAt(index - 1) !== '\\') {
        var rawString = this.expression.substring(startPos + 1, index);
        this.current = this.newToken(TSTRING, this.unescape(rawString), startPos);
        r = true;
        break;
      }
      index = this.expression.indexOf(quote, index + 1);
    }
  }
  return r;
};

TokenStream.prototype.isParen = function () {
  var c = this.expression.charAt(this.pos);
  if (c === '(' || c === ')') {
    this.current = this.newToken(TPAREN, c);
    this.pos++;
    return true;
  }
  return false;
};

TokenStream.prototype.isComma = function () {
  var c = this.expression.charAt(this.pos);
  if (c === ',') {
    this.current = this.newToken(TCOMMA, ',');
    this.pos++;
    return true;
  }
  return false;
};

TokenStream.prototype.isConst = function () {
  var startPos = this.pos;
  var i = startPos;
  for (; i < this.expression.length; i++) {
    var c = this.expression.charAt(i);
    if (c.toUpperCase() === c.toLowerCase()) {
      if (i === this.pos || (c !== '_' && c !== '.' && (c < '0' || c > '9'))) {
        break;
      }
    }
  }
  if (i > startPos) {
    var str = this.expression.substring(startPos, i);
    if (str in this.consts) {
      this.current = this.newToken(TNUMBER, this.consts[str]);
      this.pos += str.length;
      return true;
    }
  }
  return false;
};

TokenStream.prototype.isNamedOp = function () {
  var startPos = this.pos;
  var i = startPos;
  for (; i < this.expression.length; i++) {
    var c = this.expression.charAt(i);
    if (c.toUpperCase() === c.toLowerCase()) {
      if (i === this.pos || (c !== '_' && (c < '0' || c > '9'))) {
        break;
      }
    }
  }
  if (i > startPos) {
    var str = this.expression.substring(startPos, i);
    if (this.isOperatorEnabled(str) && (str in this.binaryOps || str in this.unaryOps || str in this.ternaryOps)) {
      this.current = this.newToken(TOP, str);
      this.pos += str.length;
      return true;
    }
  }
  return false;
};

TokenStream.prototype.isName = function () {
  var startPos = this.pos;
  var i = startPos;
  var hasLetter = false;
  for (; i < this.expression.length; i++) {
    var c = this.expression.charAt(i);
    if (c.toUpperCase() === c.toLowerCase()) {
      if (i === this.pos && c === '$') {
        continue;
      } else if (i === this.pos || !hasLetter || (c !== '_' && (c < '0' || c > '9'))) {
        break;
      }
    } else {
      hasLetter = true;
    }
  }
  if (hasLetter) {
    var str = this.expression.substring(startPos, i);
    this.current = this.newToken(TNAME, str);
    this.pos += str.length;
    return true;
  }
  return false;
};

TokenStream.prototype.isWhitespace = function () {
  var r = false;
  var c = this.expression.charAt(this.pos);
  while (c === ' ' || c === '\t' || c === '\n' || c === '\r') {
    r = true;
    this.pos++;
    if (this.pos >= this.expression.length) {
      break;
    }
    c = this.expression.charAt(this.pos);
  }
  return r;
};

var codePointPattern = /^[0-9a-f]{4}$/i;

TokenStream.prototype.unescape = function (v) {
  var index = v.indexOf('\\');
  if (index < 0) {
    return v;
  }

  var buffer = v.substring(0, index);
  while (index >= 0) {
    var c = v.charAt(++index);
    switch (c) {
      case '\'':
        buffer += '\'';
        break;
      case '"':
        buffer += '"';
        break;
      case '\\':
        buffer += '\\';
        break;
      case '/':
        buffer += '/';
        break;
      case 'b':
        buffer += '\b';
        break;
      case 'f':
        buffer += '\f';
        break;
      case 'n':
        buffer += '\n';
        break;
      case 'r':
        buffer += '\r';
        break;
      case 't':
        buffer += '\t';
        break;
      case 'u':
        // interpret the following 4 characters as the hex of the unicode code point
        var codePoint = v.substring(index + 1, index + 5);
        if (!codePointPattern.test(codePoint)) {
          this.parseError('Illegal escape sequence: \\u' + codePoint);
        }
        buffer += String.fromCharCode(parseInt(codePoint, 16));
        index += 4;
        break;
      default:
        throw this.parseError('Illegal escape sequence: "\\' + c + '"');
    }
    ++index;
    var backslash = v.indexOf('\\', index);
    buffer += v.substring(index, backslash < 0 ? v.length : backslash);
    index = backslash;
  }

  return buffer;
};

TokenStream.prototype.isComment = function () {
  var c = this.expression.charAt(this.pos);
  if (c === '/' && this.expression.charAt(this.pos + 1) === '*') {
    this.pos = this.expression.indexOf('*/', this.pos) + 2;
    if (this.pos === 1) {
      this.pos = this.expression.length;
    }
    return true;
  }
  return false;
};

TokenStream.prototype.isRadixInteger = function () {
  var pos = this.pos;

  if (pos >= this.expression.length - 2 || this.expression.charAt(pos) !== '0') {
    return false;
  }
  ++pos;

  var radix;
  var validDigit;
  if (this.expression.charAt(pos) === 'x') {
    radix = 16;
    validDigit = /^[0-9a-f]$/i;
    ++pos;
  } else if (this.expression.charAt(pos) === 'b') {
    radix = 2;
    validDigit = /^[01]$/i;
    ++pos;
  } else {
    return false;
  }

  var valid = false;
  var startPos = pos;

  while (pos < this.expression.length) {
    var c = this.expression.charAt(pos);
    if (validDigit.test(c)) {
      pos++;
      valid = true;
    } else {
      break;
    }
  }

  if (valid) {
    this.current = this.newToken(TNUMBER, parseInt(this.expression.substring(startPos, pos), radix));
    this.pos = pos;
  }
  return valid;
};

TokenStream.prototype.isNumber = function () {
  var valid = false;
  var pos = this.pos;
  var startPos = pos;
  var resetPos = pos;
  var foundDot = false;
  var foundDigits = false;
  var c;

  while (pos < this.expression.length) {
    c = this.expression.charAt(pos);
    if ((c >= '0' && c <= '9') || (!foundDot && c === '.')) {
      if (c === '.') {
        foundDot = true;
      } else {
        foundDigits = true;
      }
      pos++;
      valid = foundDigits;
    } else {
      break;
    }
  }

  if (valid) {
    resetPos = pos;
  }

  if (c === 'e' || c === 'E') {
    pos++;
    var acceptSign = true;
    var validExponent = false;
    while (pos < this.expression.length) {
      c = this.expression.charAt(pos);
      if (acceptSign && (c === '+' || c === '-')) {
        acceptSign = false;
      } else if (c >= '0' && c <= '9') {
        validExponent = true;
        acceptSign = false;
      } else {
        break;
      }
      pos++;
    }

    if (!validExponent) {
      pos = resetPos;
    }
  }

  if (valid) {
    this.current = this.newToken(TNUMBER, parseFloat(this.expression.substring(startPos, pos)));
    this.pos = pos;
  } else {
    this.pos = resetPos;
  }
  return valid;
};

TokenStream.prototype.isOperator = function () {
  var startPos = this.pos;
  var c = this.expression.charAt(this.pos);

  if (c === '+' || c === '-' || c === '*' || c === '/' || c === '%' || c === '^' || c === '?' || c === ':' || c === '.') {
    this.current = this.newToken(TOP, c);
  } else if (c === '∙' || c === '•') {
    this.current = this.newToken(TOP, '*');
  } else if (c === '>') {
    if (this.expression.charAt(this.pos + 1) === '=') {
      this.current = this.newToken(TOP, '>=');
      this.pos++;
    } else {
      this.current = this.newToken(TOP, '>');
    }
  } else if (c === '<') {
    if (this.expression.charAt(this.pos + 1) === '=') {
      this.current = this.newToken(TOP, '<=');
      this.pos++;
    } else {
      this.current = this.newToken(TOP, '<');
    }
  } else if (c === '|') {
    if (this.expression.charAt(this.pos + 1) === '|') {
      this.current = this.newToken(TOP, '||');
      this.pos++;
    } else {
      return false;
    }
  } else if (c === '=') {
    if (this.expression.charAt(this.pos + 1) === '=') {
      this.current = this.newToken(TOP, '==');
      this.pos++;
    } else {
      return false;
    }
  } else if (c === '!') {
    if (this.expression.charAt(this.pos + 1) === '=') {
      this.current = this.newToken(TOP, '!=');
      this.pos++;
    } else {
      this.current = this.newToken(TOP, c);
    }
  } else {
    return false;
  }
  this.pos++;

  if (this.isOperatorEnabled(this.current.value)) {
    return true;
  } else {
    this.pos = startPos;
    return false;
  }
};

var optionNameMap = {
  '+': 'add',
  '-': 'subtract',
  '*': 'multiply',
  '/': 'divide',
  '%': 'remainder',
  '^': 'power',
  '!': 'factorial',
  '<': 'comparison',
  '>': 'comparison',
  '<=': 'comparison',
  '>=': 'comparison',
  '==': 'comparison',
  '!=': 'comparison',
  '||': 'concatenate',
  'and': 'logical',
  'or': 'logical',
  'not': 'logical',
  '?': 'conditional',
  ':': 'conditional'
};

function getOptionName(op) {
  return optionNameMap.hasOwnProperty(op) ? optionNameMap[op] : op;
}

TokenStream.prototype.isOperatorEnabled = function (op) {
  var optionName = getOptionName(op);
  var operators = this.options.operators || {};

  // in is a special case for now because it's disabled by default
  if (optionName === 'in') {
    return !!operators['in'];
  }

  return !(optionName in operators) || !!operators[optionName];
};

TokenStream.prototype.getCoordinates = function () {
  var line = 0;
  var column;
  var newline = -1;
  do {
    line++;
    column = this.pos - newline;
    newline = this.expression.indexOf('\n', newline + 1);
  } while (newline >= 0 && newline < this.pos);

  return {
    line: line,
    column: column
  };
};

TokenStream.prototype.parseError = function (msg) {
  var coords = this.getCoordinates();
  throw new Error('parse error [' + coords.line + ':' + coords.column + ']: ' + msg);
};

function ParserState(parser, tokenStream, options) {
  this.parser = parser;
  this.tokens = tokenStream;
  this.current = null;
  this.nextToken = null;
  this.next();
  this.savedCurrent = null;
  this.savedNextToken = null;
  this.allowMemberAccess = options.allowMemberAccess !== false;
}

ParserState.prototype.next = function () {
  this.current = this.nextToken;
  return (this.nextToken = this.tokens.next());
};

ParserState.prototype.tokenMatches = function (token, value) {
  if (typeof value === 'undefined') {
    return true;
  } else if (Array.isArray(value)) {
    return contains(value, token.value);
  } else if (typeof value === 'function') {
    return value(token);
  } else {
    return token.value === value;
  }
};

ParserState.prototype.save = function () {
  this.savedCurrent = this.current;
  this.savedNextToken = this.nextToken;
  this.tokens.save();
};

ParserState.prototype.restore = function () {
  this.tokens.restore();
  this.current = this.savedCurrent;
  this.nextToken = this.savedNextToken;
};

ParserState.prototype.accept = function (type, value) {
  if (this.nextToken.type === type && this.tokenMatches(this.nextToken, value)) {
    this.next();
    return true;
  }
  return false;
};

ParserState.prototype.expect = function (type, value) {
  if (!this.accept(type, value)) {
    var coords = this.tokens.getCoordinates();
    throw new Error('parse error [' + coords.line + ':' + coords.column + ']: Expected ' + (value || type));
  }
};

ParserState.prototype.parseAtom = function (instr) {
  if (this.accept(TNAME)) {
    instr.push(new Instruction(IVAR, this.current.value));
  } else if (this.accept(TNUMBER)) {
    instr.push(new Instruction(INUMBER, this.current.value));
  } else if (this.accept(TSTRING)) {
    instr.push(new Instruction(INUMBER, this.current.value));
  } else if (this.accept(TPAREN, '(')) {
    this.parseExpression(instr);
    this.expect(TPAREN, ')');
  } else {
    throw new Error('unexpected ' + this.nextToken);
  }
};

ParserState.prototype.parseExpression = function (instr) {
  this.parseConditionalExpression(instr);
};

ParserState.prototype.parseConditionalExpression = function (instr) {
  this.parseOrExpression(instr);
  while (this.accept(TOP, '?')) {
    var trueBranch = [];
    var falseBranch = [];
    this.parseConditionalExpression(trueBranch);
    this.expect(TOP, ':');
    this.parseConditionalExpression(falseBranch);
    instr.push(new Instruction(IEXPR, trueBranch));
    instr.push(new Instruction(IEXPR, falseBranch));
    instr.push(ternaryInstruction('?'));
  }
};

ParserState.prototype.parseOrExpression = function (instr) {
  this.parseAndExpression(instr);
  while (this.accept(TOP, 'or')) {
    this.parseAndExpression(instr);
    instr.push(binaryInstruction('or'));
  }
};

ParserState.prototype.parseAndExpression = function (instr) {
  this.parseComparison(instr);
  while (this.accept(TOP, 'and')) {
    this.parseComparison(instr);
    instr.push(binaryInstruction('and'));
  }
};

var COMPARISON_OPERATORS = ['==', '!=', '<', '<=', '>=', '>', 'in'];

ParserState.prototype.parseComparison = function (instr) {
  this.parseAddSub(instr);
  while (this.accept(TOP, COMPARISON_OPERATORS)) {
    var op = this.current;
    this.parseAddSub(instr);
    instr.push(binaryInstruction(op.value));
  }
};

var ADD_SUB_OPERATORS = ['+', '-', '||'];

ParserState.prototype.parseAddSub = function (instr) {
  this.parseTerm(instr);
  while (this.accept(TOP, ADD_SUB_OPERATORS)) {
    var op = this.current;
    this.parseTerm(instr);
    instr.push(binaryInstruction(op.value));
  }
};

var TERM_OPERATORS = ['*', '/', '%'];

ParserState.prototype.parseTerm = function (instr) {
  this.parseFactor(instr);
  while (this.accept(TOP, TERM_OPERATORS)) {
    var op = this.current;
    this.parseFactor(instr);
    instr.push(binaryInstruction(op.value));
  }
};

ParserState.prototype.parseFactor = function (instr) {
  var unaryOps = this.tokens.unaryOps;
  function isPrefixOperator(token) {
    return token.value in unaryOps;
  }

  this.save();
  if (this.accept(TOP, isPrefixOperator)) {
    if ((this.current.value !== '-' && this.current.value !== '+' && this.nextToken.type === TPAREN && this.nextToken.value === '(')) {
      this.restore();
      this.parseExponential(instr);
    } else {
      var op = this.current;
      this.parseFactor(instr);
      instr.push(unaryInstruction(op.value));
    }
  } else {
    this.parseExponential(instr);
  }
};

ParserState.prototype.parseExponential = function (instr) {
  this.parsePostfixExpression(instr);
  while (this.accept(TOP, '^')) {
    this.parseFactor(instr);
    instr.push(binaryInstruction('^'));
  }
};

ParserState.prototype.parsePostfixExpression = function (instr) {
  this.parseFunctionCall(instr);
  while (this.accept(TOP, '!')) {
    instr.push(unaryInstruction('!'));
  }
};

ParserState.prototype.parseFunctionCall = function (instr) {
  var unaryOps = this.tokens.unaryOps;
  function isPrefixOperator(token) {
    return token.value in unaryOps;
  }

  if (this.accept(TOP, isPrefixOperator)) {
    var op = this.current;
    this.parseAtom(instr);
    instr.push(unaryInstruction(op.value));
  } else {
    this.parseMemberExpression(instr);
    while (this.accept(TPAREN, '(')) {
      if (this.accept(TPAREN, ')')) {
        instr.push(new Instruction(IFUNCALL, 0));
      } else {
        var argCount = this.parseArgumentList(instr);
        instr.push(new Instruction(IFUNCALL, argCount));
      }
    }
  }
};

ParserState.prototype.parseArgumentList = function (instr) {
  var argCount = 0;

  while (!this.accept(TPAREN, ')')) {
    this.parseExpression(instr);
    ++argCount;
    while (this.accept(TCOMMA)) {
      this.parseExpression(instr);
      ++argCount;
    }
  }

  return argCount;
};

ParserState.prototype.parseMemberExpression = function (instr) {
  this.parseAtom(instr);
  while (this.accept(TOP, '.')) {
    if (!this.allowMemberAccess) {
      throw new Error('unexpected ".", member access is not permitted');
    }

    this.expect(TNAME);
    instr.push(new Instruction(IMEMBER, this.current.value));
  }
};

function add(a, b) {
  return Number(a) + Number(b);
}

function sub(a, b) {
  return a - b;
}

function mul(a, b) {
  return a * b;
}

function div(a, b) {
  return a / b;
}

function mod(a, b) {
  return a % b;
}

function concat(a, b) {
  return '' + a + b;
}

function equal(a, b) {
  return a === b;
}

function notEqual(a, b) {
  return a !== b;
}

function greaterThan(a, b) {
  return a > b;
}

function lessThan(a, b) {
  return a < b;
}

function greaterThanEqual(a, b) {
  return a >= b;
}

function lessThanEqual(a, b) {
  return a <= b;
}

function andOperator(a, b) {
  return Boolean(a && b);
}

function orOperator(a, b) {
  return Boolean(a || b);
}

function inOperator(a, b) {
  return contains(b, a);
}

function sinh(a) {
  return ((Math.exp(a) - Math.exp(-a)) / 2);
}

function cosh(a) {
  return ((Math.exp(a) + Math.exp(-a)) / 2);
}

function tanh(a) {
  if (a === Infinity) return 1;
  if (a === -Infinity) return -1;
  return (Math.exp(a) - Math.exp(-a)) / (Math.exp(a) + Math.exp(-a));
}

function asinh(a) {
  if (a === -Infinity) return a;
  return Math.log(a + Math.sqrt((a * a) + 1));
}

function acosh(a) {
  return Math.log(a + Math.sqrt((a * a) - 1));
}

function atanh(a) {
  return (Math.log((1 + a) / (1 - a)) / 2);
}

function log10(a) {
  return Math.log(a) * Math.LOG10E;
}

function neg(a) {
  return -a;
}

function not(a) {
  return !a;
}

function trunc(a) {
  return a < 0 ? Math.ceil(a) : Math.floor(a);
}

function random(a) {
  return Math.random() * (a || 1);
}

function factorial(a) { // a!
  return gamma(a + 1);
}

function isInteger(value) {
  return isFinite(value) && (value === Math.round(value));
}

var GAMMA_G = 4.7421875;
var GAMMA_P = [
  0.99999999999999709182,
  57.156235665862923517, -59.597960355475491248,
  14.136097974741747174, -0.49191381609762019978,
  0.33994649984811888699e-4,
  0.46523628927048575665e-4, -0.98374475304879564677e-4,
  0.15808870322491248884e-3, -0.21026444172410488319e-3,
  0.21743961811521264320e-3, -0.16431810653676389022e-3,
  0.84418223983852743293e-4, -0.26190838401581408670e-4,
  0.36899182659531622704e-5
];

// Gamma function from math.js
function gamma(n) {
  var t, x;

  if (isInteger(n)) {
    if (n <= 0) {
      return isFinite(n) ? Infinity : NaN;
    }

    if (n > 171) {
      return Infinity; // Will overflow
    }

    var value = n - 2;
    var res = n - 1;
    while (value > 1) {
      res *= value;
      value--;
    }

    if (res === 0) {
      res = 1; // 0! is per definition 1
    }

    return res;
  }

  if (n < 0.5) {
    return Math.PI / (Math.sin(Math.PI * n) * gamma(1 - n));
  }

  if (n >= 171.35) {
    return Infinity; // will overflow
  }

  if (n > 85.0) { // Extended Stirling Approx
    var twoN = n * n;
    var threeN = twoN * n;
    var fourN = threeN * n;
    var fiveN = fourN * n;
    return Math.sqrt(2 * Math.PI / n) * Math.pow((n / Math.E), n) *
      (1 + (1 / (12 * n)) + (1 / (288 * twoN)) - (139 / (51840 * threeN)) -
      (571 / (2488320 * fourN)) + (163879 / (209018880 * fiveN)) +
      (5246819 / (75246796800 * fiveN * n)));
  }

  --n;
  x = GAMMA_P[0];
  for (var i = 1; i < GAMMA_P.length; ++i) {
    x += GAMMA_P[i] / (n + i);
  }

  t = n + GAMMA_G + 0.5;
  return Math.sqrt(2 * Math.PI) * Math.pow(t, n + 0.5) * Math.exp(-t) * x;
}

function stringLength(s) {
  return String(s).length;
}

function hypot() {
  var sum = 0;
  var larg = 0;
  for (var i = 0; i < arguments.length; i++) {
    var arg = Math.abs(arguments[i]);
    var div;
    if (larg < arg) {
      div = larg / arg;
      sum = (sum * div * div) + 1;
      larg = arg;
    } else if (arg > 0) {
      div = arg / larg;
      sum += div * div;
    } else {
      sum += arg;
    }
  }
  return larg === Infinity ? Infinity : larg * Math.sqrt(sum);
}

function condition(cond, yep, nope) {
  return cond ? yep : nope;
}

/**
* Decimal adjustment of a number.
* From @escopecz.
*
* @param {Number} value The number.
* @param {Integer} exp  The exponent (the 10 logarithm of the adjustment base).
* @return {Number} The adjusted value.
*/
function roundTo(value, exp) {
  // If the exp is undefined or zero...
  if (typeof exp === 'undefined' || +exp === 0) {
    return Math.round(value);
  }
  value = +value;
  exp = -(+exp);
  // If the value is not a number or the exp is not an integer...
  if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
    return NaN;
  }
  // Shift
  value = value.toString().split('e');
  value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
  // Shift back
  value = value.toString().split('e');
  return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
}

function Parser(options) {
  this.options = options || {};
  this.unaryOps = {
    sin: Math.sin,
    cos: Math.cos,
    tan: Math.tan,
    asin: Math.asin,
    acos: Math.acos,
    atan: Math.atan,
    sinh: Math.sinh || sinh,
    cosh: Math.cosh || cosh,
    tanh: Math.tanh || tanh,
    asinh: Math.asinh || asinh,
    acosh: Math.acosh || acosh,
    atanh: Math.atanh || atanh,
    sqrt: Math.sqrt,
    log: Math.log,
    ln: Math.log,
    lg: Math.log10 || log10,
    log10: Math.log10 || log10,
    abs: Math.abs,
    ceil: Math.ceil,
    floor: Math.floor,
    round: Math.round,
    trunc: Math.trunc || trunc,
    '-': neg,
    '+': Number,
    exp: Math.exp,
    not: not,
    length: stringLength,
    '!': factorial
  };

  this.binaryOps = {
    '+': add,
    '-': sub,
    '*': mul,
    '/': div,
    '%': mod,
    '^': Math.pow,
    '||': concat,
    '==': equal,
    '!=': notEqual,
    '>': greaterThan,
    '<': lessThan,
    '>=': greaterThanEqual,
    '<=': lessThanEqual,
    and: andOperator,
    or: orOperator,
    'in': inOperator
  };

  this.ternaryOps = {
    '?': condition
  };

  this.functions = {
    random: random,
    fac: factorial,
    min: Math.min,
    max: Math.max,
    hypot: Math.hypot || hypot,
    pyt: Math.hypot || hypot, // backward compat
    pow: Math.pow,
    atan2: Math.atan2,
    'if': condition,
    gamma: gamma,
    roundTo: roundTo
  };

  this.consts = {
    E: Math.E,
    PI: Math.PI,
    'true': true,
    'false': false
  };
}

Parser.prototype.parse = function (expr) {
  var instr = [];
  var parserState = new ParserState(
    this,
    new TokenStream(this, expr),
    { allowMemberAccess: this.options.allowMemberAccess }
  );

  parserState.parseExpression(instr);
  parserState.expect(TEOF, 'EOF');

  return new Expression(instr, this);
};

Parser.prototype.evaluate = function (expr, variables) {
  return this.parse(expr).evaluate(variables);
};

var sharedParser = new Parser();

Parser.parse = function (expr) {
  return sharedParser.parse(expr);
};

Parser.evaluate = function (expr, variables) {
  return sharedParser.parse(expr).evaluate(variables);
};

/*!
 Based on ndef.parser, by Raphael Graf(r@undefined.ch)
 http://www.undefined.ch/mparser/index.html

 Ported to JavaScript and modified by Matthew Crumley (email@matthewcrumley.com, http://silentmatt.com/)

 You are free to use and modify this code in anyway you find useful. Please leave this comment in the code
 to acknowledge its original source. If you feel like it, I enjoy hearing about projects that use my code,
 but don't feel like you have to let me know or ask permission.
*/

var index = {
  Parser: Parser,
  Expression: Expression
};

return index;

})));


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var conditions = __webpack_require__(2).default;

exports.default = {
  evaluate: function evaluate(bundle, constraints) {
    var failures = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

    if (!constraints.length) {
      return true;
    }

    // Constraints must be reorganized into sets grouped by Concept. This is
    // to ensure that evaluation of constraints within the same Concept are
    // applied to a single record in the PHR, not mix/matching.
    var groupedConstraints = groupConstraints(constraints);

    // 1. For every constraint group (by concept)...
    return Object.keys(groupedConstraints).every(function (conceptId) {
      var constraintGroup = groupedConstraints[conceptId];
      var records = bundle.getRecords(conceptId) || [];

      // 2. At least one record...
      return records.some(function (record) {
        return (
          // 3. Must satisfy every constraint.
          constraintGroup.every(function (constraint) {
            var value = record.getValue(constraint.attributeId);
            var conditionFailures = [];
            var success = conditions.evaluateSet(value, constraint.value, conditionFailures);

            if (!success) {
              failures.push({
                conceptId: conceptId,
                attributeId: constraint.attributeId,
                conditions: conditionFailures,
                expected: constraint.value
              });
            }

            return success;
          })
        );
      });
    });
  }
};


function groupConstraints(constraints) {
  var bundle = {};

  constraints.forEach(function (constraint) {
    var conceptId = constraint.conceptId;
    var constraintGroup = bundle[conceptId] = bundle[conceptId] || [];

    constraintGroup.push(constraint);
  });

  return bundle;
}

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var flow = __webpack_require__(15).default;

var _require = __webpack_require__(5),
    Filters = _require.Filters;

var inquiry = __webpack_require__(6).default;

exports.default = {
  flow: flow,
  Filters: Filters,
  inquiry: inquiry
};

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var hls = __webpack_require__(17);
var fhir = __webpack_require__(16);

module.exports = {
  HL7: hls,
  fhir: fhir
};

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var patientData = __webpack_require__(21).default;
var rules = __webpack_require__(22).default;

exports.default = function (pedigree, proband, concepts) {
  var datasheet = patientData(pedigree, proband, concepts);
  var recommendations = rules.filter(function (rule) {
    return rule.condition(datasheet);
  }).map(function (rule) {
    return {
      id: rule.id,
      recommendation: rule.recommendation,
      organization: rule.organization,
      text: rule.text
    };
  });

  return recommendations;
};

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var amsterdamCriteria = __webpack_require__(24).default;

exports.default = {
  amsterdamCriteria: amsterdamCriteria
};

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var conditions = __webpack_require__(2).default;
var types = __webpack_require__(3).default;

var records = __webpack_require__(0).default;

// eslint-disable-next-line import/prefer-default-export
var rules = {
  validate: function validate(value) {
    var responseType = value.responseType;
    var valueAssigned = value.value !== undefined && value.value !== null;

    if (records.RESPONSES.indexOf(responseType) < 0) {
      return new Error('Malformed value: Missing or invalid responseType \'' + responseType + '\'');
    }

    if (valueAssigned && responseType !== records.RESPONSE_VALUE) {
      // For non-value responses no value must be set
      return new Error('Malformed value: Value must not be set for responseType \'' + responseType + '\'');
    }

    if (!valueAssigned && responseType === records.RESPONSE_VALUE) {
      // For value responses a value must be set
      return new Error('Malformed value: Value must be set for responseType \'' + responseType + '\'');
    }

    if (valueAssigned && !types.getSimpleType(value.value)) {
      // Values must be a recognized type
      return new Error('Malformed value: Value \'' + value.value + '\' is not of supported type');
    }

    return undefined;
  },

  evaluate: function evaluate(value, ruleset) {
    var failures = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

    var err = rules.validate(value);

    if (err) {
      throw err;
    }

    var responseType = value.responseType;
    var valueType = types.getSimpleType(value.value);

    if (ruleset.response && ruleset.response.indexOf(responseType) < 0) {
      failures.push({ cause: 'response', expected: ruleset.response, received: responseType });
      return false;
    }

    // Type and value checks only apply to value responses
    if (responseType !== records.RESPONSE_VALUE) {
      return true;
    }

    // Value type must match the ruleset type
    if (ruleset.type && ruleset.type !== valueType) {
      failures.push({ cause: 'type', expected: ruleset.type, received: valueType });
      return false;
    }

    var conditionFailures = [];

    // Value itself must meet the value conditions
    if (ruleset.value && !conditions.evaluateSet(value.value, ruleset.value, conditionFailures)) {
      failures.push({
        cause: 'value',
        conditions: conditionFailures,
        expected: ruleset.value,
        received: value.value });
      return false;
    }

    return true;
  }
};

exports.default = rules;

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _require = __webpack_require__(5),
    Filters = _require.Filters;

var Inquiry = __webpack_require__(6).default;
var Parser = __webpack_require__(8).Parser;

var DialogueFlow = function () {
  function DialogueFlow(script) {
    var _this = this;

    var extensions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, DialogueFlow);

    this.filters = Object.assign({}, Filters, extensions.filters);

    this.topics = [];
    Object.keys(script.topics).forEach(function (topicId) {
      return _this.addTopic(topicId, script.topics[topicId]);
    });
  }

  _createClass(DialogueFlow, [{
    key: 'addTopic',
    value: function addTopic(topicId, props) {
      var topic = Object.assign({ filters: [] }, props, { id: topicId });

      if (topic.content) {
        topic.content = topic.content.trim();
      }
      if (topic.question) {
        topic.question = topic.question.trim();
      }

      this.topics.push(topic);
    }
  }, {
    key: 'findTopic',
    value: function findTopic(topicId) {
      return this.topics.find(function (t) {
        return t.id === topicId;
      });
    }
  }, {
    key: 'findTopicByState',
    value: function findTopicByState(state) {
      var _this2 = this;

      // Search for the first topic that satisfies the given state,
      // ignoring topics without conditions.
      return this.topics.find(function (topic) {
        if (topic.conditions) {
          return Parser.evaluate(topic.conditions, Object.assign({}, state, _this2.filters));
        }

        return false;
      });
    }
  }, {
    key: 'startInquiry',
    value: function startInquiry(topicId, state) {
      var topic = this.findTopic(topicId);

      if (!topic) {
        throw new Error('Unknown topic \'' + topicId + '\'!');
      }

      return new Inquiry(topic, state, this.filters);
    }
  }, {
    key: 'nextInquiry',
    value: function nextInquiry(state) {
      var topic = this.findTopicByState(state);

      if (topic) {
        return this.startInquiry(topic.id, state);
      }

      return null;
    }
  }]);

  return DialogueFlow;
}();

exports.default = DialogueFlow;

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var helpers = __webpack_require__(7);
var records = __webpack_require__(0).default;

module.exports = function (pedigree, probandId, invite) {
  var familyHistory = {
    resourceType: 'List',
    id: 'genetic',
    status: 'current',
    mode: 'working',
    contained: []
  };
  var inviterType = invite.patientId ? 'your relative' : 'your doctor';
  var inviter = invite.invitedBy || inviterType;
  var formatConditions = function formatConditions(patientConditions) {
    if (!patientConditions.length) return null;

    return patientConditions.map(function (patientCondition) {
      var condition = {
        note: 'NASK',
        code: {
          coding: [{
            code: patientCondition.code,
            system: 'ItRuns',
            display: patientCondition.code
          }]
        },
        onsetAge: 'ASKU'
      };

      if (patientCondition.onsetAge) {
        condition.onsetAge = {
          value: patientCondition.onsetAge,
          unit: 'years',
          system: 'http://unitsofmeasure.org'
        };
      }
      if (patientCondition.status && patientCondition.status === records.RESPONSE_VALUE) {
        condition.outcome = 'not resolved';
      } else if (!patientCondition.status || patientCondition.status === records.RESPONSE_NONE) {
        condition.outcome = 'resolved';
      } else {
        condition.outcome = 'ASKU';
      }

      return condition;
    });
  };

  Object.keys(pedigree).forEach(function (patientId) {
    if (!pedigree[patientId].PHR) {
      return;
    }
    var patient = pedigree[patientId];
    var patientInfo = helpers.personInfo(patient);

    familyHistory.contained.push({
      resourceType: 'FamilyMemberHistory',
      identifier: patient.patientId,
      definition: 'Questionnaire',
      status: helpers.personProgress(pedigree, patient, probandId),
      notDone: false,
      notDoneReason: 'NI',
      patient: 'NI',
      date: patientInfo.date,
      name: patientInfo.name || 'ASKU',
      relationship: helpers.relationInfo(pedigree, probandId, patient),
      gender: patientInfo.gender,
      bornDate: patientInfo.birthDate,
      ageAge: helpers.ageInfo(patient),
      estimatedAge: false,
      deceasedBoolean: patientInfo.deceased,
      reasonCode: 'Invited to complete family health history by ' + inviter,
      reasonReference: 'Questionnaire Response',
      note: 'NASK',
      condition: formatConditions(patientInfo.conditions) || 'UNK'
    });
  });
  return familyHistory;
};

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var XmlWriter = __webpack_require__(26);
var helpers = __webpack_require__(7);

var hlsCodeMap = {
  male: 'M',
  female: 'F',
  parent: {
    male: 'NFTH',
    female: 'NMTH'
  },
  'parent.child': {
    male: 'NBRO',
    female: 'NSIS'
  },
  'parent.parent': {
    male: 'GRFTH',
    female: 'GRMTH'
  },
  'parent.parent.parent': {
    male: 'GGRFTH',
    female: 'GGRMTH'
  },
  'parent.child.child': {
    male: 'NEPHEW',
    female: 'NIECE'
  },
  'parent.child.child.child': {
    male: 'NEPHEW',
    female: 'NIECE'
  },
  'parent.parent.child': {
    male: 'UNCLE',
    female: 'AUNT'
  },
  'parent.parent.child.child': {
    male: 'COUSN',
    female: 'COUSN'
  },
  'parent.parent.parent.child': {
    male: 'UNCLE',
    female: 'AUNT'
  },
  child: {
    male: 'SON',
    female: 'DAU'
  },
  'child.child': {
    male: 'GRNDSON',
    female: 'GRNDDAU'
  },
  'child.child.child': {
    male: 'GRNDSON',
    female: 'GRNDDAU'
  },
  spousal: {
    male: 'HUSB',
    female: 'WIFE'
  }
};

function addConditions(relative, patientInfo) {
  var conditions = patientInfo.conditions;

  if (conditions && conditions.length) {
    conditions.forEach(function (condition) {
      var subjectConditions = relative.startElement('subjectOf2').writeAttribute('typeCode', 'SBJ');
      var clinicalObservation = subjectConditions.startElement('clinicalObservation').writeAttribute('classCode', 'OBS').writeAttribute('moodCode', 'EVN').startElement('code').writeAttribute('code', condition.code).writeAttribute('codeSystemName', 'ItRuns').writeAttribute('displayName', condition.code).endElement();

      if (condition.onsetAge) {
        clinicalObservation.startElement('subject').writeAttribute('typeCode', 'SUBJ').startElement('dataEstimatedAge').writeAttribute('classCode', 'OBS').writeAttribute('moodCode', 'EVN').startElement('code').writeAttribute('code', '21611-9').writeAttribute('codeSystemName', 'LOINC').writeAttribute('displayName', 'age at onset in years').endElement().startElement('value').writeAttribute('value', condition.onsetAge).endElement().endElement().endElement();
      }
      clinicalObservation.endElement();
      subjectConditions.endElement();
    });
  }
}
function addEstimatedAge(relative, patientInfo) {
  if (patientInfo.deceased && patientInfo.age) {
    relative.startElement('subjectOf1').writeAttribute('typeCode', 'SBJ').startElement('deceasedEstimatedAge').writeAttribute('classCode', 'OBS').writeAttribute('moodCode', 'EVN').startElement('code').writeAttribute('displayName', 'age in years').endElement().startElement('value').writeAttribute('value', patientInfo.age).endElement().endElement().endElement();
  }
  if (patientInfo.age) {
    relative.startElement('subjectOf1').writeAttribute('typeCode', 'SBJ').startElement('livingEstimatedAge').writeAttribute('classCode', 'OBS').writeAttribute('moodCode', 'EVN').startElement('code').writeAttribute('code', '21611-9').writeAttribute('codeSystemName', 'LOINC').writeAttribute('displayName', 'ESTIMATED AGE').endElement().startElement('value').writeAttribute('value', patientInfo.age).endElement().endElement().endElement();
  }
}
function createRelative(element, pedigree, patientId, type) {
  var patient = pedigree[patientId];
  var patientInfo = helpers.personInfo(patient);
  var relative = element.startElement('relative').writeAttribute('classCode', 'PRS');
  var relations = patient.relations;

  relative.startElement('code').writeAttribute('code', hlsCodeMap[type][patientInfo.gender]).endElement();
  var relationshipHolder = relative.startElement('relationshipHolder').writeAttribute('classCode', 'PSN').writeAttribute('determinerCode', 'INSTANCE').startElement('id').writeAttribute('extension', patient.patientId).endElement().writeElement('name', patientInfo.name).startElement('administrativeGenderCode').writeAttribute('code', hlsCodeMap[patientInfo.gender]).endElement().startElement('birthTime').writeAttribute('value', patientInfo.birthTime).endElement().startElement('deceasedInd').writeAttribute('value', '' + patientInfo.deceased).endElement();

  if (relations && relations.parent) {
    relations.parent.forEach(function (parentId) {
      createRelative(relationshipHolder, pedigree, parentId, 'parent');
    });
  }
  relationshipHolder.endElement();
  addEstimatedAge(relative, patientInfo);
  addConditions(relative, patientInfo);

  relative.endElement();
  return relative;
}

module.exports = function (pedigree, probandId) {
  var builder = new XmlWriter();

  builder.startDocument();
  var proband = pedigree[probandId];
  var probandInfo = helpers.personInfo(proband);
  var relations = proband.relations;
  var FamilyHistory = builder.startElement('FamilyHistory').writeAttribute('classCode', 'OBS').writeAttribute('moodCode', 'EVN');

  FamilyHistory.startElement('code').writeAttribute('code', '10157-6').writeAttribute('codeSystemName', 'LOINC').endElement();

  var subject = FamilyHistory.startElement('subject').writeAttribute('typeCode', 'SBJ');
  var patient = subject.startElement('patient').writeAttribute('classCode', 'PAT');
  var patientPerson = patient.startElement('patientPerson').writeAttribute('classCode', 'PSN').writeAttribute('determinerCode', 'INSTANCE');

  patientPerson.startElement('id').writeAttribute('extension', proband.patientId).endElement();
  patientPerson.writeElement('name', probandInfo.name);
  patientPerson.startElement('administrativeGenderCode').writeAttribute('code', hlsCodeMap[probandInfo.gender]).endElement();
  patientPerson.startElement('birthTime').writeAttribute('value', probandInfo.birthTime).endElement();
  patientPerson.startElement('deceasedInd').writeAttribute('value', '' + probandInfo.deceased).endElement();

  if (relations) {
    Object.keys(relations).forEach(function (relationType) {
      var patients = relations[relationType];

      patients.forEach(function (patientId) {
        createRelative(patientPerson, pedigree, patientId, relationType);
      });
    });
  }
  patientPerson.endElement();
  addEstimatedAge(patient, probandInfo);
  addConditions(patient, probandInfo);
  builder.endDocument();
  return builder.toString();
};

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var relationCodes = {
  parent: {
    male: {
      code: {
        paternal: 'NFTH',
        maternal: 'NFTH'
      },
      display: 'father'
    },
    female: {
      code: {
        paternal: 'NMTH',
        maternal: 'NMTH'
      },
      display: 'mother'
    }
  },
  'parent.child': {
    male: {
      code: {
        paternal: 'NBRO',
        maternal: 'NBRO'
      },
      display: 'brother'
    },
    female: {
      code: {
        paternal: 'NSIS',
        maternal: 'NSIS'
      },
      display: 'sister'
    }
  },
  'parent.parent': {
    male: {
      code: {
        paternal: 'PGRFTH',
        maternal: 'MGRFTH'
      },
      display: 'grandfather'
    },
    female: {
      code: {
        paternal: 'PGRMTH',
        maternal: 'MGRMTH'
      },
      display: 'grandmother'
    }
  },
  'parent.parent.parent': {
    male: {
      code: {
        paternal: 'PGGRFTH',
        maternal: 'MGGRFTH'
      },
      display: 'great grandfather'
    },
    female: {
      code: {
        paternal: 'PGGRMTH',
        maternal: 'MGGRMTH'
      },
      display: 'great grandmother'
    }
  },
  'parent.child.child': {
    male: {
      code: {
        paternal: 'NEPHEW',
        maternal: 'NEPHEW'
      },
      display: 'nephew'
    },
    female: {
      code: {
        paternal: 'NIECE',
        maternal: 'NIECE'
      },
      display: 'niece'
    }
  },
  'parent.child.child.child': {
    male: {
      code: {
        paternal: 'EXT',
        maternal: 'EXT'
      },
      display: 'grand nephew'
    },
    female: {
      code: {
        paternal: 'EXT',
        maternal: 'EXT'
      },
      display: 'grand niece'
    }
  },
  'parent.parent.child': {
    male: {
      code: {
        paternal: 'PUNCLE',
        maternal: 'MUNCLE'
      },
      display: 'uncle'
    },
    female: {
      code: {
        paternal: 'PAUNT',
        maternal: 'MAUNT'
      },
      display: 'aunt'
    }
  },
  'parent.parent.child.child': {
    male: {
      code: {
        paternal: 'COUSN',
        maternal: 'COUSN'
      },
      display: 'cousin'
    },
    female: {
      code: {
        paternal: 'COUSN',
        maternal: 'COUSN'
      },
      display: 'cousin'
    }
  },
  'parent.parent.parent.child': {
    male: {
      code: {
        paternal: 'EXT',
        maternal: 'EXT'
      },
      display: 'great uncle'
    },
    female: {
      code: {
        paternal: 'EXT',
        maternal: 'EXT'
      },
      display: 'great aunt'
    }
  },
  child: {
    male: {
      code: {
        paternal: 'SON',
        maternal: 'SON'
      },
      display: 'son'
    },
    female: {
      code: {
        paternal: 'DAU',
        maternal: 'DAU'
      },
      display: 'daughter'
    }
  },
  'child.child': {
    male: {
      code: {
        paternal: 'GRNDSON',
        maternal: 'GRNDSON'
      },
      display: 'grandson'
    },
    female: {
      code: {
        paternal: 'GRNDDAU',
        maternal: 'GRNDDAU'
      },
      display: 'granddaughter'
    }
  },
  'child.child.child': {
    male: {
      code: {
        paternal: 'EXT',
        maternal: 'EXT'
      },
      display: 'great grandson'
    },
    female: {
      code: {
        paternal: 'EXT',
        maternal: 'EXT'
      },
      display: 'great granddaughter'
    }
  },
  spousal: {
    male: {
      code: {
        paternal: 'HUSB',
        maternal: 'HUSB'
      },
      display: 'husband'
    },
    female: {
      code: {
        paternal: 'WIFE',
        maternal: 'WIFE'
      },
      display: 'wife'
    }
  },
  proband: {
    male: {
      code: {
        paternal: 'ONESELF',
        maternal: 'ONESELF'
      },
      display: 'You'
    },
    female: {
      code: {
        paternal: 'ONESELF',
        maternal: 'ONESELF'
      },
      display: 'You'
    }
  }
};

module.exports = {
  relationCodes: relationCodes
};

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = amsterdamCriteria;
var constants = __webpack_require__(1);

var MIN_CANCER_PATIENTS = 3;
var MAX_ONSET_VALUE = 50;
var FIRST_DEG_LINKS = 2;
var relativeGeneration = function relativeGeneration(relation) {
  return relation.split('.').reduce(function (v, cv) {
    return v + (cv === 'parent' ? 1 : -1);
  }, 0);
};

function amsterdamCriteria(datasheet) {
  var cancers = ['C81', 'C25', 'C29', 'C30'];
  var relativeAffected = Object.keys(datasheet.byRelatives).filter(function (patientId) {
    return datasheet.byRelatives[patientId].conditions.filter(function (c) {
      return cancers.includes(c.conceptId);
    }).length;
  });
  var hasSuccessiveGen = relativeAffected.find(function (patientId) {
    var relations = datasheet.byRelatives[patientId].relations;
    var successiveGen = {
      maternal: [],
      paternal: []
    };

    Object.keys(relations).forEach(function (relation) {
      relativeAffected.forEach(function (r) {
        var familySide = datasheet.byRelatives[r].familySide;

        if (familySide !== 'none' && r.patientId !== parseInt(patientId, 10) && relations[relation].includes(parseInt(r, 10))) {
          successiveGen[familySide].push(relation);
        }
      });
    });
    var maternal = successiveGen.maternal.find(function (r) {
      return successiveGen.maternal.find(function (i) {
        return Math.abs(relativeGeneration(r) - relativeGeneration(i)) === 1;
      });
    });
    var paternal = successiveGen.paternal.find(function (r) {
      return successiveGen.paternal.find(function (i) {
        return Math.abs(relativeGeneration(r) - relativeGeneration(i)) === 1;
      });
    });

    return maternal || paternal;
  });
  var diagnosedAge = relativeAffected.filter(function (patientId) {
    return datasheet.byRelatives[patientId].conditions.filter(function (c) {
      return cancers.includes(c.conceptId) && c.ageOnset < MAX_ONSET_VALUE;
    }).length;
  });
  var firstDegreeCheck = relativeAffected.find(function (patientId) {
    var relations = relativeAffected.filter(function (rId) {
      return rId !== patientId;
    });

    return relations.filter(function (rId) {
      return datasheet.byRelatives[rId].relativesByDegree[constants.FDR].find(function (r) {
        return r.patientId === parseInt(patientId, 10);
      });
    }).length >= FIRST_DEG_LINKS;
  });

  return !!(relativeAffected.length > MIN_CANCER_PATIENTS && hasSuccessiveGen && diagnosedAge.length && firstDegreeCheck);
}

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var records = __webpack_require__(0).default;
var degreeByRelation = __webpack_require__(1).default.degreeByRelation;

var findGender = function findGender(parent) {
  var life = new records.RecordBundle(parent.PHR).getRecords('Life')[0];

  return life.getValue('gender');
};

var memberRelation = function memberRelation(relations, relativeId) {
  var relationPath = Object.keys(relations).find(function (relation) {
    return relations[relation].includes(relativeId);
  }) || 'proband';

  return degreeByRelation[relationPath];
};

var familySides = function familySides(pedigree, probandId) {
  var relations = pedigree[probandId].relations;
  var family = {
    maternal: [],
    paternal: []
  };

  relations.parent.forEach(function (parent) {
    var data = pedigree[parent];
    var display = findGender(data);

    if (display === 'male') {
      family.paternal.push(parent);
      Object.keys(data.relations).forEach(function (relation) {
        if (relation.startsWith('parent')) {
          var _family$paternal;

          (_family$paternal = family.paternal).push.apply(_family$paternal, _toConsumableArray(data.relations[relation]));
        }
      });
      return;
    }

    family.maternal.push(parent);
    Object.keys(data.relations).forEach(function (relation) {
      if (relation.startsWith('parent')) {
        var _family$maternal;

        (_family$maternal = family.maternal).push.apply(_family$maternal, _toConsumableArray(data.relations[relation]));
      }
    });
  });

  return family;
};

var memberSide = function memberSide(pedigree, proband, relativeId) {
  var family = familySides(pedigree, proband);

  if (family.maternal.includes(relativeId)) {
    return 'maternal';
  }

  if (family.paternal.includes(relativeId)) {
    return 'paternal';
  }

  return 'none';
};

var patientConditions = function patientConditions(currentPatient, concepts) {
  var patientRecords = currentPatient.getAllRecords();
  var included = [];

  Object.keys(patientRecords).forEach(function (conceptId) {
    if (concepts[conceptId].path.startsWith('Patient.Condition.')) {
      patientRecords[conceptId].forEach(function (recordInstance) {
        included.push({
          conceptId: conceptId,
          ageOnset: recordInstance.getValue('age-start')
        });
      });
    }
  });

  return included;
};

var memberAge = function memberAge(life) {
  var dYear = life.getValue('death-year');
  var dMonth = life.getValue('death-month');
  var dDay = life.getValue('death-day');
  var bYear = life.getValue('birth-year');
  var bMonth = life.getValue('birth-month');
  var bDay = life.getValue('birth-day');
  var today = new Date();
  var age = null;
  var monthDiff = void 0;
  var dayCheck = void 0;

  if (bYear) {
    age = dYear ? dYear - bYear : today.getFullYear() - bYear;
    if (bMonth && bDay) {
      if (dMonth && dDay) {
        monthDiff = dMonth - bMonth;
        dayCheck = dDay < bDay;
      } else {
        monthDiff = today.getMonth() + 1 - bMonth;
        dayCheck = today.getDate() < bDay;
      }

      if (monthDiff <= 0 && dayCheck) {
        age--;
      }
    }
  }
  return age;
};

var relativesWithCondition = function relativesWithCondition(pedigree, proband, cancerId) {
  var relatives = [];

  Object.keys(pedigree).forEach(function (patientId) {
    if (patientId === 'stats') return;

    var relativePhr = new records.RecordBundle(pedigree[patientId].PHR);
    var relativeLife = relativePhr.getRecords('Life')[0];
    var relations = pedigree[proband].relations;
    var relativeRecord = relativePhr.getRecords(cancerId);
    var adopted = relativeLife.getResponse('adopted');

    relativeRecord.forEach(function (record) {
      relatives.push({
        memberId: patientId,
        ageOnset: record.getValue('age-start'),
        age: memberAge(relativeLife),
        gender: relativeLife.getValue('gender'),
        degree: memberRelation(relations, parseInt(patientId, 10)),
        familySide: memberSide(pedigree, proband, parseInt(patientId, 10)),
        isAdopted: !!(adopted && adopted.responseType !== records.RESPONSE_NONE),
        relations: pedigree[patientId].relations
      });
    });
  });

  return relatives;
};

var relativesWithDegree = function relativesWithDegree(pedigree, proband, concepts) {
  var relatives = {};

  Object.keys(pedigree).forEach(function (patientId) {
    if (patientId === 'stats') return;

    var relations = pedigree[proband].relations;
    var degree = memberRelation(relations, parseInt(patientId, 10));

    if (degree === undefined) return;

    if (!relatives[degree]) {
      relatives[degree] = [];
    }

    var currentPatient = new records.RecordBundle(pedigree[patientId].PHR);
    var included = patientConditions(currentPatient, concepts);

    var relativeLife = currentPatient.getRecords('Life')[0];
    var adopted = relativeLife.getResponse('adopted');

    relatives[degree].push({
      memberId: patientId,
      age: memberAge(relativeLife),
      gender: relativeLife.getValue('gender'),
      familySide: memberSide(pedigree, proband, parseInt(patientId, 10)),
      isAdopted: !!(adopted && adopted.responseType !== records.RESPONSE_NONE),
      conditions: included,
      relations: pedigree[patientId].relations
    });
  });

  return relatives;
};

var patientWithDiseases = function patientWithDiseases(pedigree, proband, concepts) {
  var result = {};

  Object.keys(pedigree).forEach(function (patientId) {
    if (patientId === 'stats') return;

    var probandRelations = pedigree[proband].relations;
    var relativeRelations = pedigree[patientId].relations;
    var currentPatient = new records.RecordBundle(pedigree[patientId].PHR);
    var life = currentPatient.getRecords('Life')[0];
    var patientAdopted = life.getResponse('adopted');
    var relativesByDegree = {};
    var relatives = {};

    Object.keys(relativeRelations).forEach(function (relation) {
      relativeRelations[relation].forEach(function (relativeId) {
        var relativePhr = new records.RecordBundle(pedigree[relativeId].PHR);
        var relativeLife = relativePhr.getRecords('Life')[0];
        var adopted = relativeLife.getResponse('adopted');

        relatives[relativeId] = {
          age: memberAge(relativeLife),
          gender: relativeLife.getValue('gender'),
          degree: memberRelation(relativeRelations, parseInt(relativeId, 10)),
          familySide: memberSide(pedigree, proband, parseInt(patientId, 10)),
          isAdopted: !!(adopted && adopted.responseType !== records.RESPONSE_NONE),
          conditions: patientConditions(relativePhr, concepts),
          patientId: relativeId,
          relations: pedigree[relativeId].relations
        };
      });
    });

    Object.keys(relatives).forEach(function (relativeId) {
      var degree = relatives[relativeId].degree;

      if (!relativesByDegree[degree]) {
        relativesByDegree[degree] = [];
      }
      relativesByDegree[degree].push(relatives[relativeId]);
    });

    result[patientId] = {
      age: memberAge(life),
      gender: life.getValue('gender'),
      degree: memberRelation(probandRelations, parseInt(patientId, 10)),
      isAdopted: !!(patientAdopted && patientAdopted.responseType !== records.RESPONSE_NONE),
      conditions: patientConditions(currentPatient, concepts),
      relatives: relatives,
      relativesByDegree: relativesByDegree,
      familySide: memberSide(pedigree, parseInt(proband, 10), parseInt(patientId, 10)),
      relations: pedigree[patientId].relations,
      patientId: patientId
    };
  });
  return result;
};

module.exports = {
  relativesWithCondition: relativesWithCondition,
  relativesWithDegree: relativesWithDegree,
  patientWithDiseases: patientWithDiseases
};

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = patientData;
var helpers = __webpack_require__(20);
var maxDegree = __webpack_require__(1).default.MAX_DEGREE;

function patientData(pedigree, proband, concepts) {
  var datasheet = {
    byCondition: {},
    byDegree: {},
    byRelatives: helpers.patientWithDiseases(pedigree, proband, concepts)
  };

  Object.keys(concepts).forEach(function (concept) {
    if (concepts[concept].path.indexOf('Patient.Condition.') === -1) return;

    datasheet.byCondition[concept] = helpers.relativesWithCondition(pedigree, proband, concept);
    datasheet.byCondition[concept].byDegree = {};

    var _loop = function _loop(degree) {
      datasheet.byCondition[concept].byDegree[degree] = datasheet.byCondition[concept].filter(function (patient) {
        return patient.degree === degree;
      });
    };

    for (var degree = 0; degree <= maxDegree; degree++) {
      _loop(degree);
    }
  });

  datasheet.byDegree = helpers.relativesWithDegree(pedigree, proband, concepts);

  return datasheet;
}

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/* eslint-disable no-magic-numbers */
var constants = __webpack_require__(1);
var amsterdamCriteria = __webpack_require__(19).default;

var isCloseRelative = function isCloseRelative(r) {
  return r.degree === constants.FDR || r.degree === constants.SDR;
};
var isMale = function isMale(r) {
  return r.gender === 'male';
};
var isFemale = function isFemale(r) {
  return r.gender === 'female';
};

var guidelines = [{
  // R1001 First degree relative with Breast cancer dx at age ≤50
  id: 'R1001',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'First degree relative with Breast cancer dx at age ≤50',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C9.byDegree[constants.FDR].find(function (r) {
      return r.ageOnset <= 50;
    });
  }
}, {
  // R1002 First degree relative with ≥2 primary breast cancers
  id: 'R1002',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'First degree relative with ≥2 primary breast cancers',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C9.byDegree[constants.FDR].length >= 2;
  }
}, {
  // R1003 ≥3 cases of breast, ovarian, pancreatic,
  // and/or aggressive prostate cancer in close relatives
  id: 'R1003',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: '≥3 cases of breast / ovarian / pancreatic / aggressive prostate cancer in close relatives',
  condition: function condition(datasheet) {
    var breastCancer = datasheet.byCondition.C9.filter(isCloseRelative).length;
    var ovarian = datasheet.byCondition.C19.filter(isCloseRelative).length;
    var pancreatic = datasheet.byCondition.C20.filter(isCloseRelative).length;
    var prostate = datasheet.byCondition.C22.filter(isCloseRelative).length;

    return breastCancer + ovarian + pancreatic + prostate >= 3;
  }
}, {
  // R1004 First degree relative with ≥3 cases of breast, ovarian, pancreatic,
  // and/or aggressive prostate cancer in close relatives
  id: 'R1004',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'First degree relative with ≥3 cases of breast / ovarian / pancreatic / aggressive prostate cancer',
  condition: function condition(datasheet) {
    var breastCancer = datasheet.byCondition.C9.byDegree[constants.FDR].length;
    var ovarian = datasheet.byCondition.C19.byDegree[constants.FDR].length;
    var pancreatic = datasheet.byCondition.C20.byDegree[constants.FDR].length;
    var prostate = datasheet.byCondition.C22.byDegree[constants.FDR].length;

    return breastCancer + ovarian + pancreatic + prostate >= 3;
  }
}, {
  // R1005 Breast cancer in two relatives, one dx at age ≤45
  id: 'R1005',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'Breast cancer in two relatives, one dx at age ≤45',
  condition: function condition(datasheet) {
    var breastCancer = datasheet.byCondition.C9;

    return breastCancer.length >= 2 && breastCancer.find(function (r) {
      return r.ageOnset <= 45;
    });
  }
}, {
  // R1006 First degree relative with male breast cancer
  id: 'R1006',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'First degree relative with male breast cancer',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C9.byDegree[constants.FDR].find(isMale);
  }
}, {
  // R1007 [First degree relative] with [Colorectal cancer] [dx at age <50]
  id: 'R1007',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'First degree relative with Colorectal cancer dx at age <50',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C81.byDegree[constants.FDR].find(function (r) {
      return r.ageOnset < 50;
    });
  }
}, {
  // R1008 [Colorectal cancer] dx at [age ≥50] and a [FDR] with [colorectal or
  // endometrial cancer] at [any age]
  id: 'R1008',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'Colorectal cancer dx at age ≥50 and a FDR with colorectal or endometrial cancer at any age',
  condition: function condition(datasheet) {
    var patientWithCancerAge = datasheet.byCondition.C81.find(function (r) {
      return r.ageOnset >= 50;
    });
    var fdrWithCancer = datasheet.byDegree[constants.FDR].find(function (r) {
      return r.conditions.find(function (c) {
        return c.conceptId === 'C81' || c.conceptId === 'C30';
      });
    });

    return patientWithCancerAge && fdrWithCancer;
  }
}, {
  // R1009 First degree relative with Colorectal cancer dx at age ≥50 and
  // has a FDR with colorectal or endometrial cancer at any age
  id: 'R1009',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'First degree relative with Colorectal cancer dx at age \u226550 and\n      has a FDR with colorectal or endometrial cancer at any age',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C81.byDegree[constants.FDR].find(function (r) {
      return r.ageOnset >= 50 && datasheet.byRelatives[r.memberId].relativesByDegree[constants.FDR].find(function (m) {
        return m.conditions.find(function (c) {
          return c.conceptId === 'C81' || c.conceptId === 'C30';
        });
      });
    });
  }
}, {
  // R1010 ≥2 cases of gastric cancer, one dx at age <50 in close relatives
  id: 'R1010',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: '≥2 cases of gastric cancer, one dx at age <50 in close relatives',
  condition: function condition(datasheet) {
    var gastricCancer = datasheet.byCondition.C26;
    var closeRelativesCancer = gastricCancer.find(function (r) {
      return r.ageOnset < 50 && isCloseRelative(r);
    });

    return gastricCancer.length >= 2 && closeRelativesCancer;
  }
}, {
  // R1011 First degree relative with ≥2 cases of gastric cancer,
  // one dx at age <50 in close relatives
  id: 'R1011',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'First degree relative with ≥2 cases of gastric cancer, one dx at age <50 in close relatives',
  condition: function condition(datasheet) {
    var gastricCancer = datasheet.byCondition.C26.byDegree[constants.FDR];
    var closeRelativesCancer = datasheet.byCondition.C26.find(function (r) {
      return r.ageOnset < 50 && isCloseRelative(r);
    });

    return gastricCancer.length >= 2 && closeRelativesCancer;
  }
}, {
  // R1012 ≥3 cases of gastric cancer in close relatives
  id: 'R1012',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: '≥3 cases of gastric cancer in close relatives',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C26.filter(isCloseRelative).length >= 3;
  }
}, {
  // R1013 First degree relative with ≥3 cases of gastric cancer in close relatives
  id: 'R1013',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'First degree relative with ≥3 cases of gastric cancer in close relatives',
  condition: function condition(datasheet) {
    return datasheet.byDegree[constants.FDR].find(function (r) {
      var relatives = datasheet.byRelatives[r.memberId].relatives;

      return Object.keys(relatives).filter(function (rid) {
        return isCloseRelative(relatives[rid]) && relatives[rid].conditions.find(function (c) {
          return c.conceptId === 'C26';
        });
      }).length >= 3;
    });
  }
}, {
  // R1014 First degree relative with Diffuse gastric cancer dx at age <40
  id: 'R1014',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'First degree relative with Diffuse gastric cancer dx at age <40',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C26.byDegree[constants.FDR].find(function (r) {
      return r.ageOnset < 40;
    });
  }
}, {
  // R1015 First degree relative with Diffuse gastric cancer and lobular breast cancer
  id: 'R1015',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'First degree relative with Diffuse gastric cancer and lobular breast cancer',
  condition: function condition(datasheet) {
    return datasheet.byDegree[constants.FDR].find(function (r) {
      var conditions = r.conditions.map(function (c) {
        return c.conceptId;
      });

      return conditions.includes('C26') && conditions.includes('C9');
    });
  }
}, {
  // R1016 Diffuse gastric cancer in one relative and lobular breast cancer in
  // another, one dx at age <50
  id: 'R1016',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'Diffuse gastric cancer in one relative and lobular breast cancer in another, one dx at age <50',
  condition: function condition(datasheet) {
    var gastricCancerPatient = datasheet.byCondition.C26.find(function (r) {
      return r;
    });
    var breastCancerPatient = datasheet.byCondition.C9.find(function (r) {
      return gastricCancerPatient && r.memberId !== gastricCancerPatient.memberId;
    });

    return gastricCancerPatient && breastCancerPatient && (gastricCancerPatient.ageOnset < 50 || breastCancerPatient.ageOnset < 50);
  }
}, {
  // R1213 patient has {breast cancer} && a FDR || SDR with {breast cancer} <50
  id: 'R1213',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'You have breast cancer and a FDR or SDR has breast cancer <50',
  condition: function condition(datasheet) {
    var probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length;
    var closeRelativesCancer = datasheet.byCondition.C9.find(function (c) {
      return isCloseRelative(c) && c.ageOnset < 50;
    });

    return probandHasCancer && closeRelativesCancer;
  }
}, {
  // R1214 patient has {breast cancer} &&  FDR || SDR with {ovarian cancer}
  id: 'R1214',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'Patient has breast cancer and a FDR or SDR has ovarian cancer',
  condition: function condition(datasheet) {
    var probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length;
    var closeRelativesCancer = datasheet.byCondition.C19.find(isCloseRelative);

    return probandHasCancer && closeRelativesCancer;
  }
}, {
  // R1215 patient has {breast cancer} && 2+ FDR || SDR with {breast cancer}
  id: 'R1215',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'Patient has breast cancer and 2+ FDR or SDR has breast cancer',
  condition: function condition(datasheet) {
    var probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length;
    var closeRelativesCancer = datasheet.byCondition.C9.filter(isCloseRelative).length;

    return probandHasCancer && closeRelativesCancer >= 2;
  }
}, {
  // R1216 patient has {breast cancer} && 2+ FDR||SDR with {breast cancer}||{pancreatic cancer}
  id: 'R1216',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'Patient has breast cancer and 2+ FDR or SDR has breast cancer or pancreatic cancer',
  condition: function condition(datasheet) {
    var probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length;
    var closeRelativesBreastCancer = datasheet.byCondition.C9.filter(isCloseRelative).length;
    var closeRelativesPancreaticCancer = datasheet.byCondition.C20.filter(isCloseRelative).length;

    return probandHasCancer && closeRelativesBreastCancer + closeRelativesPancreaticCancer >= 2;
  }
}, {
  // R1217 patient has {breast cancer} && {breast cancer gene} in family
  id: 'R1217',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'Patient has breast cancer and breast cancer gene in family',
  condition: function condition(datasheet) {
    var probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length;
    var familyBreastCancer = datasheet.byCondition.C9.find(function (c) {
      return c.degree !== constants.PROBAND;
    });

    return probandHasCancer && familyBreastCancer;
  }
}, {
  // R1218 FDR || SDR with {breast cancer} <45yo
  id: 'R1218',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'FDR or SDR with breast cancer at age <45',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C9.find(function (r) {
      return isCloseRelative(r) && r.ageOnset < 45;
    });
  }
}, {
  // R1232 FDR || SDR male relative with {breast cancer}
  id: 'R1232',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'FDR or SDR male relative with breast cancer',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C9.find(function (r) {
      return isCloseRelative(r) && isMale(r);
    });
  }
}, {
  // R1233 FDR with {breast cancer} <50yo && another {any cancer} in same person
  id: 'R1233',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'FDR with breast cancer at age <50 and another any cancer in same person',
  condition: function condition(datasheet) {
    return datasheet.byDegree[constants.FDR].find(function (r) {
      return r.conditions.find(function (c) {
        return c.conceptId === 'C9' && c.ageOnset < 50;
      }) && r.conditions.find(function (c) {
        return c.conceptId !== 'C9';
      });
    });
  }
}, {
  // R1234 FDR with {breast cancer} <50yo && another relative with {breast cancer} at any age
  id: 'R1234',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'FDR with breast cancer at age <50 and other relative with breast cancer at any age',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C9.filter(function (r) {
      return r.degree === constants.FDR && r.ageOnset < 50;
    }).find(function (r) {
      return datasheet.byCondition.C9.find(function (c) {
        return c.memberId !== r.memberId;
      });
    });
  }
}, {
  // R1235 FDR with {breast cancer} <50yo && is adopted
  id: 'R1235',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'FDR with breast cancer at age <50 and is adopted',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C9.byDegree[constants.FDR].find(function (r) {
      return r.ageOnset < 50 && r.isAdopted;
    });
  }
}, {
  // R1236 SDR with {breast cancer} <50yo && another {any cancer} in same person
  id: 'R1236',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'SDR with breast cancer at age <50 and another any cancer in same person',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C9.byDegree[constants.SDR].find(function (r) {
      return r.ageOnset < 50 && datasheet.byRelatives[r.memberId].conditions.find(function (c) {
        return c.conceptId !== 'C9';
      });
    });
  }
}, {
  // R1237 SDR with {breast cancer} <50yo && another relative with {breast cancer} at any age
  id: 'R1237',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'SDR with breast cancer at age <50 and another relative with breast cancer at any age',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C9.byDegree[constants.SDR].find(function (r) {
      return r.ageOnset < 50 && datasheet.byCondition.C9.find(function (m) {
        return m.memberId !== r.memberId;
      });
    });
  }
}, {
  // R1238 SDR with {breast cancer} <50yo && is adopted
  id: 'R1238',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'SDR with breast cancer at age <50 and is adopted',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C9.byDegree[constants.SDR].find(function (r) {
      return r.ageOnset < 50 && r.isAdopted;
    });
  }
}, {
  // R1239 {breast cancer gene} in family
  id: 'R1239',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'Breast cancer gene in family',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C9.filter(function (r) {
      return r.degree !== constants.PROBAND;
    }).length;
  }
}, {
  // R1240 2+ relatives on same side of family with {breast cancer}
  id: 'R1240',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: '2+ relatives on same side of family with breast cancer',
  condition: function condition(datasheet) {
    var relatives = datasheet.byRelatives;
    var familySides = {};

    Object.keys(relatives).map(function (rId) {
      return relatives[rId];
    }).forEach(function (relative) {
      var hasBreastCancer = relative.conditions.map(function (c) {
        return c.conceptId;
      }).includes('C9');

      if (relative.familySide !== 'none' && hasBreastCancer) {
        (familySides[relative.familySide] || (familySides[relative.familySide] = [])).push(relative);
      }
    });

    return Object.keys(familySides).find(function (side) {
      return familySides[side].length >= 2;
    });
  }
}, {
  // R1241 a relative with 2+ {breast cancers} in a single person
  id: 'R1241',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'A relative with 2+ breast cancers in a single person',
  condition: function condition(datasheet) {
    return Object.keys(datasheet.byRelatives).find(function (rId) {
      return datasheet.byRelatives[rId].conditions.filter(function (c) {
        return c.conceptId === 'C9';
      }).length >= 2;
    });
  }
}, {
  // R1242 patient w/{gastric cancer} && {breast cancer} in FDR||SDR w/a Dx <50
  id: 'R1242',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'Patient has gastric cancer and breast cancer in FDR or SDR under 50 yrs old',
  condition: function condition(datasheet) {
    var probandHasCancer = datasheet.byCondition.C26.byDegree[constants.PROBAND].length;
    var relativesHavingCancer = datasheet.byCondition.C9.find(function (r) {
      return isCloseRelative(r) && r.ageOnset < 50;
    });

    return probandHasCancer && relativesHavingCancer;
  }
}, {
  // R1243 relative w/{gastric cancer} <40yo
  id: 'R1243',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'A relative with gastric cancer under 40 yrs old',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C26.find(function (r) {
      return r.ageOnset < 40;
    });
  }
}, {
  // R1244 2+ FDR||SDR w/{gastric cancer} w/a dx <50
  id: 'R1244',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: '2+ FDR or SDR with gastric cancer under 50 yrs old',
  condition: function condition(datasheet) {
    var relatives = datasheet.byCondition.C26.filter(function (r) {
      return isCloseRelative(r);
    });
    var relativesWithOnset = relatives.find(function (r) {
      return r.ageOnset < 50;
    });

    return relatives.length >= 2 && relativesWithOnset;
  }
}, {
  // R1245 3+ FDR||SDR w/{gastric cancer}
  id: 'R1245',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: '3+ FDR or SDR with gastric cancer',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C26.filter(function (r) {
      return isCloseRelative(r);
    }).length >= 3;
  }
}, {
  // R1246 {gastric cancer gene} in family
  id: 'R1246',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'Gastric cancer gene in family',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C26.find(function (r) {
      return r.degree !== constants.PROBAND;
    });
  }
}, {
  // R1248 meets {amsterdam}
  id: 'R1248',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'Meets Amsterdam criteria',
  condition: function condition(datasheet) {
    return amsterdamCriteria(datasheet);
  }
}, {
  // R1020 rel w/3+ cases of {melanoma a/o pancreatic cancer}
  id: 'R1020',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'Relative with 3+ cases of melenoma / pancreatic cancer',
  condition: function condition(datasheet) {
    var melanoma = datasheet.byCondition.C41.filter(isCloseRelative);
    var pancreatic = datasheet.byCondition.C20.filter(isCloseRelative);

    return melanoma.length + pancreatic.length >= 3;
  }
}, {
  // R1022 FDR with 3+ melanomas
  id: 'R1022',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'FDR with 3+ melanoma cancer',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C41.filter(function (r) {
      return r.degree === constants.FDR;
    }).length >= 3;
  }
}, {
  // R1023 FDR with {melanoma}&&{pancreastic cancer}
  id: 'R1023',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'FDR with melanoma and pancreatic cancer',
  condition: function condition(datasheet) {
    return datasheet.byDegree[constants.FDR].filter(function (r) {
      var conditions = r.conditions.map(function (c) {
        return c.conceptId;
      });

      return conditions.includes('C41') && conditions.includes('C20');
    }).length;
  }
}, {
  // R1024 {melanoma}&&{astrocytoma} in single FDR
  id: 'R1024',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'FDR with melanoma and astrocytoma cancer',
  condition: function condition(datasheet) {
    return datasheet.byDegree[constants.FDR].filter(function (r) {
      var conditions = r.conditions.map(function (c) {
        return c.conceptId;
      });

      return conditions.includes('C41') && conditions.includes('C82');
    }).length;
  }
}, {
  // R1025 {melanoma}&&{astrocytoma} in 2 FDR
  id: 'R1025',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: '2 FDR with melanoma and astrocytoma cancer',
  condition: function condition(datasheet) {
    return datasheet.byDegree[constants.FDR].filter(function (r) {
      var conditions = r.conditions.map(function (c) {
        return c.conceptId;
      });

      return conditions.includes('C41') && conditions.includes('C82');
    }).length >= 2;
  }
}, {
  // R1026 pt dx w/ {pancreatic cancer} && 2+ {pancreatic cancer} in relative
  id: 'R1026',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'Patient with pancreatic cancer and 2+ pancreatic cancer in relatives',
  condition: function condition(datasheet) {
    var probandHasCancer = false;
    var relativesHavingCancer = datasheet.byCondition.C20.filter(function (r) {
      if (r.degree === constants.PROBAND) {
        probandHasCancer = true;
      }
      return isCloseRelative(r);
    });

    return probandHasCancer && relativesHavingCancer.length >= 2;
  }
}, {
  // R1028 pt dx w/{pancreatic cancer} && 2+ {breast, ovarian, prostate caner} in relative
  id: 'R1028',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'Patient with pancreatic cancer and 2+ pancreatic / breast / prostate cancer in relatives',
  condition: function condition(datasheet) {
    var probandHasCancer = datasheet.byCondition.C20.byDegree[constants.PROBAND].length;
    var relativesHavingCancer = Object.keys(datasheet.byRelatives).find(function (rId) {
      var relative = datasheet.byRelatives[rId];
      var breastCancer = relative.conditions.filter(function (c) {
        return c.conceptId === 'C9';
      }).length;
      var ovarianCancer = relative.conditions.filter(function (c) {
        return c.conceptId === 'C19';
      }).length;
      var prostateCancer = relative.conditions.filter(function (c) {
        return c.conceptId === 'C22';
      }).length;

      return isCloseRelative(relative) && (breastCancer >= 2 || ovarianCancer >= 2 || prostateCancer >= 2);
    });

    return probandHasCancer && relativesHavingCancer;
  }
}, {
  // R1033 3+ dx of {pancreatic cancer || melanoma} in single relative
  id: 'R1033',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: '3+ pancreatic or melanoma cancer in single relative',
  condition: function condition(datasheet) {
    var relatives = datasheet.byRelatives;

    return Object.keys(relatives).find(function (rId) {
      var melanoma = relatives[rId].conditions.filter(function (c) {
        return c.conceptId === 'C41';
      });
      var pancreatic = relatives[rId].conditions.filter(function (c) {
        return c.conceptId === 'C20';
      });

      return isCloseRelative(relatives[rId]) && (melanoma.length >= 3 || pancreatic.length >= 3);
    });
  }
}, {
  // R1035 FDR with {pancreatic cancer && melanoma}
  id: 'R1035',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'FDR with pancreatic and melanoma cancer',
  condition: function condition(datasheet) {
    return datasheet.byDegree[constants.FDR].find(function (r) {
      var conditions = r.conditions.map(function (c) {
        return c.conceptId;
      });

      return conditions.includes('C41') && conditions.includes('C20');
    });
  }
}, {
  // R1036 2+ relateives with {prostate cancer} dx <56
  id: 'R1036',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: '2+ relatives with prostate cancer under 56 yrs old',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C22.filter(function (r) {
      return r.ageOnset < 56;
    }).length >= 2;
  }
}, {
  // R1038 3+ FDR with prostate cancer
  id: 'R1038',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: '3+ FDR with prostate cancer',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C22.byDegree[constants.FDR].length >= 3;
  }
}, {
  // R1040 Pt dx w/ {prostate cancer} && 2+ {breast, ovarian, pancreatic cancer} in relative
  id: 'R1040',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'ACMG',
  text: 'Patient with prostate cancer and 2+ breast / ovarian / pancreatic cancer in relative',
  condition: function condition(datasheet) {
    var probandHasCancer = datasheet.byCondition.C22.byDegree[constants.PROBAND].length;
    var relativesHavingCancer = Object.keys(datasheet.byRelatives).find(function (rId) {
      var relative = datasheet.byRelatives[rId];
      var breastCancer = relative.conditions.filter(function (c) {
        return c.conceptId === 'C9';
      }).length;
      var ovarianCancer = relative.conditions.filter(function (c) {
        return c.conceptId === 'C19';
      }).length;
      var pancreaticCancer = relative.conditions.filter(function (c) {
        return c.conceptId === 'C20';
      }).length;

      return isCloseRelative(relative) && (breastCancer >= 2 || ovarianCancer >= 2 || pancreaticCancer >= 2);
    });

    return probandHasCancer && relativesHavingCancer;
  }
}, {
  // R1257 If you do not have [breast cancer] but do have a [first or second degree relative]
  // with [breast cancer] [prior to or at] [45] yo;
  id: 'R1257',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'FDR or SDR with breast cancer under 45 yrs old',
  condition: function condition(datasheet) {
    var probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length;
    var relativesHavingCancer = datasheet.byCondition.C9.filter(function (r) {
      return isCloseRelative(r) && r.ageOnset <= 45;
    });

    return !probandHasCancer && relativesHavingCancer.length;
  }
}, {
  // R1258 [Personal history] of [breast cancer] and [diagnosed] [at any age]
  // with  a [close male blood relative] with [breast cancer]
  id: 'R1258',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'Personal history of breast cancer and a close male relative with breast cancer',
  condition: function condition(datasheet) {
    var probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length;
    var relativesHavingCancer = datasheet.byCondition.C9.filter(function (r) {
      return isCloseRelative(r) && isMale(r);
    });

    return probandHasCancer && relativesHavingCancer.length;
  }
}, {
  // R1250 patient with {ovarian cancer} && relative with {colon cancer}||{breast cancer}
  id: 'R1250',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'Personal history of ovarian cancer and a relative with colon cancer or breast cancer',
  condition: function condition(datasheet) {
    var probandHasCancer = datasheet.byCondition.C19.byDegree[constants.PROBAND].length;
    var colonCancer = datasheet.byCondition.C47.length;
    var breastCancer = datasheet.byCondition.C9.length;

    return probandHasCancer && (colonCancer || breastCancer);
  }
}, {
  // R1251 2+ FDR w/{ovarian cancer}
  id: 'R1251',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: '2+ FDR with ovarian cancer',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C19.byDegree[constants.FDR].length >= 2;
  }
}, {
  // R1252 relative w/{lynch syndrome}
  id: 'R1252',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'Relative with lynch syndromes',
  condition: function condition(datasheet) {
    return datasheet.byCondition.S1.length;
  }
}, {
  // R1322 if anyone in family has {Peutz-Jeghers syndrome, Li-Fruameni,
  // Cowden or Lynch syndrome}
  id: 'R1322',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'Relative with Peutz-Jeghers, Li-Fruameni, Cowden or Lynch syndrome',
  condition: function condition(datasheet) {
    var peutSyndrome = datasheet.byCondition.S5.length;
    var liFruameni = datasheet.byCondition.S3.length;
    var lynchSyndrome = datasheet.byCondition.S1.length;
    var cowdenSyndrome = datasheet.byCondition.S17.length;

    return peutSyndrome || liFruameni || lynchSyndrome || cowdenSyndrome;
  }
}, {
  // R1325 pt dx w/{pancreatic cancer} && 2+ relatives with
  // {breast, ovarian, pancreatic, prostate cancer}
  id: 'R1325',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'Personal history of pancreatic cancer and 2+ relatives with breast, ovarian, pancreatic or prostate cancer',
  condition: function condition(datasheet) {
    var probandHasCancer = datasheet.byCondition.C20.byDegree[constants.PROBAND].length;
    var relativesHavingCancer = Object.keys(datasheet.byRelatives).filter(function (rId) {
      var relative = datasheet.byRelatives[rId];

      return relative.conditions.filter(function (c) {
        return c.conceptId === 'C9' || c.conceptId === 'C19' || c.conceptId === 'C20' || c.conceptId === 'C22';
      }).length;
    });

    return probandHasCancer && relativesHavingCancer.length >= 2;
  }
}, {
  // R1326 pt dx w/ {breast cancer} && 2+ relatives w/ {breast cancer}
  id: 'R1326',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'Personal history of breast cancer and 2+ relatives with breast cancer',
  condition: function condition(datasheet) {
    var probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length;
    var relativesHavingCancer = datasheet.byCondition.C9.filter(isCloseRelative);

    return probandHasCancer && relativesHavingCancer.length >= 2;
  }
}, {
  // R1327 pt dx w/ {breast cancer} && 1+ relatives w/ {ovarian cancer}
  id: 'R1327',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'Personal history of breast cancer and 1+ relatives with ovarian cancer',
  condition: function condition(datasheet) {
    var probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length;
    var relativesHavingCancer = datasheet.byCondition.C19.filter(isCloseRelative);

    return probandHasCancer && relativesHavingCancer.length >= 1;
  }
}, {
  // R1328 1+ relatives on same side with {ovarian cnacer}
  id: 'R1328',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: '1+ relatives on same side of family with ovarian cancer',
  condition: function condition(datasheet) {
    var probandMutation = datasheet.byDegree[constants.PROBAND].filter(function (r) {
      return r.conditions.filter(function (c) {
        return c.conceptId === 'G3' || c.conceptId === 'G6';
      }).length;
    }).length;
    var relatives = {
      maternal: [],
      paternal: []
    };

    datasheet.byCondition.C19.forEach(function (r) {
      if (r.familySide !== 'none') {
        relatives[r.familySide].push(r);
      }
    });

    return !probandMutation && (relatives.maternal.length >= 1 || relatives.paternal.length >= 1);
  }
}, {
  // R1249 2+ relatives w/{melanoma}
  id: 'R1249',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: '2+ relatives with melenoma cancer',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C41.length >= 2;
  }
}, {
  // patient with {uterine cancer} <50yo &&
  // relative with {endometrial cancer}||{colorectal cancer}
  id: 'R1253',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'NCCN v1.2018',
  text: 'Personal history of utherine cancer under 50 yrs old and\n      a relative with endometrial cancer or colorectal cancer',
  condition: function condition(datasheet) {
    var probandHasCancer = datasheet.byCondition.C30.byDegree[constants.PROBAND].filter(function (r) {
      return r.ageOnset < 50;
    }).length;
    var endometrial = datasheet.byCondition.C30.find(function (r) {
      return r.degree !== constants.PROBAND;
    });
    var colorectal = datasheet.byCondition.C81.find(function (r) {
      return r.degree !== constants.PROBAND;
    });

    return probandHasCancer && (endometrial || colorectal);
  }
}, {
  // R1523 2+ FDR||SDR with {ovarian cancer} || {breast cancer & ovarian cancer}
  id: 'R1523',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'USPSTF',
  text: '2+ FDR or SDR with ovarian cancer or (breast cancer and ovarian cancer)',
  condition: function condition(datasheet) {
    var probandIsFemale = datasheet.byDegree[constants.PROBAND].find(isFemale);
    var relativesHavingCancer = datasheet.byCondition.C19.filter(isCloseRelative);

    return probandIsFemale && relativesHavingCancer.length >= 2;
  }
}, {
  // R1524 1 FDR w {breast cancer} || {ovarian cancer}
  id: 'R1524',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'USPSTF',
  text: '1 FDR with breast cancer or ovarian cancer',
  condition: function condition(datasheet) {
    var breastCancer = datasheet.byCondition.C9.find(function (r) {
      return r.degree === constants.FDR;
    });
    var ovarianCancer = datasheet.byCondition.C19.find(function (r) {
      return r.degree === constants.FDR;
    });

    return breastCancer || ovarianCancer;
  }
}, {
  // R1525 2 SDR same side of family w {breast cancer} || {ovarian cancer}
  id: 'R1525',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'USPSTF',
  text: '2 SDR on same side of family with breast cancer or ovarian cancer',
  condition: function condition(datasheet) {
    var relatives = datasheet.byRelatives;
    var familySides = {};

    Object.keys(relatives).map(function (rId) {
      return relatives[rId];
    }).forEach(function (relative) {
      var conditions = relative.conditions.map(function (c) {
        return c.conceptId;
      });
      var hasCancer = conditions.includes('C9') || conditions.includes('C19');

      if (relative.familySide !== 'none' && hasCancer && relative.degree === constants.SDR) {
        (familySides[relative.familySide] || (familySides[relative.familySide] = [])).push(relative);
      }
    });

    return Object.keys(familySides).find(function (side) {
      return familySides[side].length === 2;
    });
  }
}, {
  // R1513 If you have a first-degree relative who has had colorectal adenomas
  id: 'R1513',
  recommendation: constants.EARLIER_SCREENING,
  organization: 'USPSTF',
  text: 'FDR with colorectal adenomas',
  condition: function condition(datasheet) {
    return datasheet.byCondition.S4.byDegree[constants.FDR].length;
  }
}, {
  // R1514 If you have a family history of skin cancer,
  id: 'R1514',
  recommendation: constants.EARLIER_SCREENING,
  organization: 'USPSTF',
  text: 'Family history of skin cancer',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C24.length;
  }
}, {
  // R1515 If you have a family history of prostate cancer
  id: 'R1515',
  recommendation: constants.EARLIER_SCREENING,
  organization: 'USPSTF',
  text: 'Family history of prostate cancer',
  condition: function condition(datasheet) {
    return datasheet.byCondition.C22.length;
  }
}, {
  // R1519 If [women] test negative for [BRCA 1 & 2] mutation but
  // have [relatives] with known [BRCA mutations]
  id: 'R1519',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'USPSTF',
  text: 'Personal history of test negative BRCA 1 and BRCA 2 mutation and a relative with known BRCA mutations',
  condition: function condition(datasheet) {
    var proband = datasheet.byDegree[constants.PROBAND].find(function (r) {
      var conditions = r.conditions.map(function (c) {
        return c.conceptId;
      });

      return !conditions.includes('G6') && !conditions.includes('G3') && isFemale(r);
    });
    var relativesWithMutation = datasheet.byCondition.G6.length || datasheet.byCondition.G3.length;

    return proband && relativesWithMutation;
  }
}, {
  // R1520 If [women] who have been diagnosed with [breast cancer] in the past
  // and who did not receive [BRCA testing] as part of their cancer care
  // but have a [family history] of [breast or ovarian cancer]
  id: 'R1520',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'USPSTF',
  text: 'Personal history of breast cancer in the past and did not receive\n      BRCA testing and family history of breast or ovarian cancer',
  condition: function condition(datasheet) {
    var proband = datasheet.byDegree[constants.PROBAND].find(function (r) {
      var conditions = r.conditions.map(function (c) {
        return c.conceptId;
      });
      var brcaTesting = conditions.includes('G3') || conditions.includes('G6');

      return conditions.includes('C9') && !brcaTesting;
    });
    var breastCancer = datasheet.byCondition.C9.find(function (r) {
      return r.degree !== constants.PROBAND;
    });
    var ovarianCancer = datasheet.byCondition.C19.find(function (r) {
      return r.degree !== constants.PROBAND;
    });

    return proband && (breastCancer || ovarianCancer);
  }
}, {
  // R1522 If you have a family history of Lynch syndrome
  id: 'R1522',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'USPSTF',
  text: 'Family history of Lynch syndrome',
  condition: function condition(datasheet) {
    return datasheet.byCondition.S1.length;
  }
}, {
  // R1521 If [women] who have [1] [or more] [family members]
  // with a [known potentially harmful mutation] in the [BRCA1 or BRCA2 genes]
  id: 'R1521',
  recommendation: constants.GENETIC_COUNSELING,
  organization: 'USPSTF',
  text: '1+ relatives with known potentially harmful mutation in the BRCA 1 or BRCA 2 genes',
  condition: function condition(datasheet) {
    var proband = datasheet.byDegree[constants.PROBAND].find(isFemale);
    var brcaOne = datasheet.byCondition.G6.find(function (r) {
      return r.degree !== constants.PROBAND;
    });
    var brcaTwo = datasheet.byCondition.G3.find(function (r) {
      return r.degree !== constants.PROBAND;
    });

    return proband && (brcaOne || brcaTwo);
  }
}];

exports.default = guidelines;

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var types = __webpack_require__(3).default;
var conditions = __webpack_require__(2).default;
var records = __webpack_require__(0).default;
var constraints = __webpack_require__(9).default;
var rules = __webpack_require__(14).default;
var progress = __webpack_require__(4).default;
var dialogue = __webpack_require__(10).default;
var Export = __webpack_require__(11);
var recommendation = __webpack_require__(13).default;
var guidelines = __webpack_require__(12).default;

module.exports = {
  types: types,
  conditions: conditions,
  records: records,
  constraints: constraints,
  rules: rules,
  progress: progress,
  dialogue: dialogue,
  export: Export,
  recommendation: recommendation,
  guidelines: guidelines
};

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.default = amsterdamCriteria;
var generations = __webpack_require__(25).default.generations;
var records = __webpack_require__(0).default;

var cancerCodes = ['C81', 'C25', 'C29', 'C30'];
var MIN_CANCER_PATIENTS = 3;
var MAX_ONSET_VALUE = 50;
var FIRST_DEG_LINKS = 2;

function amsterdamCriteria(pedigree, currentPatientId) {
  var cancerPatients = [];
  var ageStartCheck = false;

  Object.keys(pedigree).forEach(function (patientId) {
    if (patientId === currentPatientId) return;

    var phr = new records.RecordBundle(pedigree[patientId].PHR);

    // eslint-disable-next-line no-underscore-dangle
    Object.keys(phr.__records).forEach(function (conceptId) {
      if (cancerCodes.indexOf(conceptId) === -1) return;

      if (cancerPatients.indexOf(pedigree[patientId]) === -1) {
        var record = phr.getRecords(conceptId)[0];

        cancerPatients.push(pedigree[patientId]);
        if (record.getValue('age-start') < MAX_ONSET_VALUE && !ageStartCheck) {
          ageStartCheck = true;
        }
      }
    });
  });

  if (cancerPatients.length < MIN_CANCER_PATIENTS || !ageStartCheck) {
    return { amsterdamCriteria: false };
  }

  var successiveGenerations = findSuccessiveGen(cancerPatients, pedigree, currentPatientId);

  if (!successiveGenerations) return { amsterdamCriteria: false };

  var firstDegreeCheck = checkFirstDegreeRel(cancerPatients);

  if (!firstDegreeCheck) return { amsterdamCriteria: false };

  return { amsterdamCriteria: true };
}

function findSuccessiveGen(patients, pedigree, currentPatientId) {
  var parentOneGen = [];
  var parentTwoGen = [];
  var probandsRel = pedigree[currentPatientId].relations;

  var _probandsRel$parent = _slicedToArray(probandsRel.parent, 2),
      parentOne = _probandsRel$parent[0],
      parentTwo = _probandsRel$parent[1];

  var parentOneRel = pedigree[parentOne].relations;

  patients.forEach(function (patient) {
    if (patient.patientId === parentOne) {
      parentOneGen.push('parent');
      return;
    }

    if (patient.patientId === parentTwo) {
      parentTwoGen.push('parent');
      return;
    }

    var relation = Object.keys(probandsRel).find(function (rel) {
      return probandsRel[rel].indexOf(patient.patientId) > -1;
    });

    var isRelatedToParentOne = Object.keys(parentOneRel).find(function (rel) {
      return parentOneRel[rel].indexOf(patient.patientId) > -1;
    });

    if (isRelatedToParentOne) {
      parentOneGen.push(relation);
      return;
    }

    parentTwoGen.push(relation);
  });
  return hasSuccessiveGen(parentOneGen, parentTwoGen);
}

function hasSuccessiveGen(parentOneGen, parentTwoGen) {
  var successiveGenCheck = false;

  Object.keys(generations).forEach(function (gen) {
    if (successiveGenCheck) return;
    successiveGenCheck = parentOneGen.indexOf(generations[gen][0]) > -1 && parentOneGen.indexOf(generations[gen][1]) > -1;

    if (!successiveGenCheck) {
      successiveGenCheck = parentTwoGen.indexOf(generations[gen][0]) > -1 && parentTwoGen.indexOf(generations[gen][1]) > -1;
    }
  });

  return successiveGenCheck;
}

function checkFirstDegreeRel(patients) {
  var relationsFound = patients.find(function (patient) {
    var relFound = [patient.patientId];
    var parents = patient.relations.parent;
    var children = patient.relations.child;
    var siblings = patient.relations['parent.child'];
    var firstDegreeRel = [].concat(parents, children, siblings);
    var list = patients.filter(function (pat) {
      return pat !== patient;
    });

    list.forEach(function (relative) {
      if (firstDegreeRel.indexOf(relative.patientId) > -1) {
        relFound.push(relative.patientId);
      }
    });
    return relFound.length > FIRST_DEG_LINKS;
  });

  if (relationsFound) return true;

  return false;
}

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/* eslint no-param-reassign: ["error", { "props": false }] */
var generations = {
  gen1: ['parent.parent.parent', 'parent.parent.parent.child'],
  gen2: ['parent.parent', 'parent.parent.child'],
  gen3: ['parent.parent.child', 'parent.parent.child.child'],
  gen4: ['parent', 'parent.child'],
  gen5: ['parent.child', 'parent.child.child'],
  gen6: ['parent.child.child', 'parent.child.child.child'],
  gen7: ['child', 'child.child'],
  gen8: ['child.child', 'child.child.child'],
  gen9: ['parent', 'parent.parent']
};

exports.default = {
  generations: generations
};

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(27);


/***/ }),
/* 27 */
/***/ (function(module, exports) {


function isFalse(s) {
  return typeof s !== 'number' && !s;
}

function strval(s) {
  if (typeof s == 'string') {
    return s;
  }
  else if (typeof s == 'number') {
    return s+'';
  }
  else if (typeof s == 'function') {
    return s();
  }
  else if (s instanceof XMLWriter) {
    return s.toString();
  }
  else throw Error('Bad Parameter');
}

function XMLWriter(indent, callback) {

    if (!(this instanceof XMLWriter)) {
        return new XMLWriter();
    }

    this.name_regex = /[_:A-Za-z][-._:A-Za-z0-9]*/;
    this.indent = indent ? true : false;
    this.indentString = this.indent && typeof indent === 'string' ? indent : '    ';
    this.output = '';
    this.stack = [];
    this.tags = 0;
    this.attributes = 0;
    this.attribute = 0;
    this.texts = 0;
    this.comment = 0;
    this.dtd = 0;
    this.root = '';
    this.pi = 0;
    this.cdata = 0;
    this.started_write = false;
    this.writer;
    this.writer_encoding = 'UTF-8';

    if (typeof callback == 'function') {
        this.writer = callback;
    } else {
        this.writer = function (s, e) {
            this.output += s;
        }
    }
}

XMLWriter.prototype = {
    toString : function () {
        this.flush();
        return this.output;
    },

    indenter : function () {
      if (this.indent) {
        this.write('\n');
        for (var i = 1; i < this.tags; i++) {
          this.write(this.indentString);
        }
      }
    },

    write : function () {
        for (var i = 0; i < arguments.length; i++) {
            this.writer(arguments[i], this.writer_encoding);
        }
    },


    flush : function () {
        for (var i = this.tags; i > 0; i--) {
            this.endElement();
        }
        this.tags = 0;
    },

    startDocument : function (version, encoding, standalone) {
        if (this.tags || this.attributes) return this;

        this.startPI('xml');
        this.startAttribute('version');
        this.text(typeof version == "string" ? version : "1.0");
        this.endAttribute();
        if (typeof encoding == "string") {
            this.startAttribute('encoding');
            this.text(encoding);
            this.endAttribute();
            this.writer_encoding = encoding;
        }
        if (standalone) {
            this.startAttribute('standalone');
            this.text("yes");
            this.endAttribute();
        }
        this.endPI();
        if (!this.indent) {
          this.write('\n');
        }
        return this;
    },

    endDocument : function () {
        if (this.attributes) this.endAttributes();
        return this;
    },

    writeElement : function (name, content) {
        return this.startElement(name).text(content).endElement();
    },

    writeElementNS : function (prefix, name, uri, content) {
        if (!content) {
            content = uri;
        }
        return this.startElementNS(prefix, name, uri).text(content).endElement();
    },

    startElement : function (name) {
        name = strval(name);
        if (!name.match(this.name_regex)) throw Error('Invalid Parameter');
        if (this.tags === 0 && this.root && this.root !== name) throw Error('Invalid Parameter');
        if (this.attributes) this.endAttributes();
        ++this.tags;
        this.texts = 0;
        if (this.stack.length > 0)
          this.stack[this.stack.length-1].containsTag = true;

        this.stack.push({
            name: name,
            tags: this.tags
        });
        if (this.started_write) this.indenter();
        this.write('<', name);
        this.startAttributes();
        this.started_write = true;
        return this;
    },
    startElementNS : function (prefix, name, uri) {
        prefix = strval(prefix);
        name = strval(name);

        if (!prefix.match(this.name_regex)) throw Error('Invalid Parameter');
        if (!name.match(this.name_regex)) throw Error('Invalid Parameter');
        if (this.attributes) this.endAttributes();
        ++this.tags;
        this.texts = 0;
        if (this.stack.length > 0)
          this.stack[this.stack.length-1].containsTag = true;

        this.stack.push({
            name: prefix + ':' + name,
            tags: this.tags
        });
        if (this.started_write) this.indenter();
        this.write('<', prefix + ':' + name);
        this.startAttributes();
        this.started_write = true;
        return this;
    },

    endElement : function () {
        if (!this.tags) return this;
        var t = this.stack.pop();
        if (this.attributes > 0) {
            if (this.attribute) {
                if (this.texts) this.endAttribute();
                this.endAttribute();
            }
            this.write('/');
            this.endAttributes();
        } else {
            if (t.containsTag) this.indenter();
            this.write('</', t.name, '>');
        }
        --this.tags;
        this.texts = 0;
        return this;
    },

    writeAttribute : function (name, content) {
        if (typeof content == 'function') {
          content = content();
        }
        if (isFalse(content)) {
           return this;
        }
        return this.startAttribute(name).text(content).endAttribute();
    },
    writeAttributeNS : function (prefix, name, uri, content) {
        if (!content) {
            content = uri;
        }
        if (typeof content == 'function') {
          content = content();
        }
        if (isFalse(content)) {
          return this;
        }
        return this.startAttributeNS(prefix, name, uri).text(content).endAttribute();
    },

    startAttributes : function () {
        this.attributes = 1;
        return this;
    },

    endAttributes : function () {
        if (!this.attributes) return this;
        if (this.attribute) this.endAttribute();
        this.attributes = 0;
        this.attribute = 0;
        this.texts = 0;
        this.write('>');
        return this;
    },

    startAttribute : function (name) {
        name = strval(name);
        if (!name.match(this.name_regex)) throw Error('Invalid Parameter');
        if (!this.attributes && !this.pi) return this;
        if (this.attribute) return this;
        this.attribute = 1;
        this.write(' ', name, '="');
        return this;
    },
    startAttributeNS : function (prefix, name, uri) {
        prefix = strval(prefix);
        name = strval(name);

        if (!prefix.match(this.name_regex)) throw Error('Invalid Parameter');
        if (!name.match(this.name_regex)) throw Error('Invalid Parameter');
        if (!this.attributes && !this.pi) return this;
        if (this.attribute) return this;
        this.attribute = 1;
        this.write(' ', prefix + ':' + name, '="');
        return this;
    },
    endAttribute : function () {
        if (!this.attribute) return this;
        this.attribute = 0;
        this.texts = 0;
        this.write('"');
        return this;
    },

    text : function (content) {
        content = strval(content);
        if (!this.tags && !this.comment && !this.pi && !this.cdata) return this;
        if (this.attributes && this.attribute) {
            ++this.texts;
            this.write(content
                       .replace(/&/g, '&amp;')
                       .replace(/</g, '&lt;')
                       .replace(/"/g, '&quot;')
                       .replace(/\t/g, '&#x9;')
                       .replace(/\n/g, '&#xA;')
                       .replace(/\r/g, '&#xD;')
                      );
            return this;
        } else if (this.attributes && !this.attribute) {
            this.endAttributes();
        }
        if (this.comment || this.cdata) {
            this.write(content);
        }
        else {
          this.write(content.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;'));
        }
        ++this.texts;
        this.started_write = true;
        return this;
    },

    writeComment : function (content) {
        return this.startComment().text(content).endComment();
    },

    startComment : function () {
        if (this.comment) return this;
        if (this.attributes) this.endAttributes();
        this.indenter();
        this.write('<!--');
        this.comment = 1;
        this.started_write = true;
        return this;
    },

    endComment : function () {
        if (!this.comment) return this;
        this.write('-->');
        this.comment = 0;
        return this;
    },

    writeDocType : function (name, pubid, sysid, subset) {
        return this.startDocType(name, pubid, sysid, subset).endDocType()
    },

    startDocType : function (name, pubid, sysid, subset) {
        if (this.dtd || this.tags) return this;

        name = strval(name);
        pubid = pubid ? strval(pubid) : pubid;
        sysid = sysid ? strval(sysid) : sysid;
        subset = subset ? strval(subset) : subset;

        if (!name.match(this.name_regex)) throw Error('Invalid Parameter');
        if (pubid && !pubid.match(/^[\w\-][\w\s\-\/\+\:\.]*/)) throw Error('Invalid Parameter');
        if (sysid && !sysid.match(/^[\w\.][\w\-\/\\\:\.]*/)) throw Error('Invalid Parameter');
        if (subset && !subset.match(/[\w\s\<\>\+\.\!\#\-\?\*\,\(\)\|]*/)) throw Error('Invalid Parameter');

        pubid = pubid ? ' PUBLIC "' + pubid + '"' : (sysid) ? ' SYSTEM' : '';
        sysid = sysid ? ' "' + sysid + '"' : '';
        subset = subset ? ' [' + subset + ']': '';

        if (this.started_write) this.indenter();
        this.write('<!DOCTYPE ', name, pubid, sysid, subset);
        this.root = name;
        this.dtd = 1;
        this.started_write = true;
        return this;
    },

    endDocType : function () {
        if (!this.dtd) return this;
        this.write('>');
        return this;
    },

    writePI : function (name, content) {
        return this.startPI(name).text(content).endPI()
    },

    startPI : function (name) {
        name = strval(name);
        if (!name.match(this.name_regex)) throw Error('Invalid Parameter');
        if (this.pi) return this;
        if (this.attributes) this.endAttributes();
        if (this.started_write) this.indenter();
        this.write('<?', name);
        this.pi = 1;
        this.started_write = true;
        return this;
    },

    endPI : function () {
        if (!this.pi) return this;
        this.write('?>');
        this.pi = 0;
        return this;
    },

    writeCData : function (content) {
        return this.startCData().text(content).endCData();
    },

    startCData : function () {
        if (this.cdata) return this;
        if (this.attributes) this.endAttributes();
        this.indenter();
        this.write('<![CDATA[');
        this.cdata = 1;
        this.started_write = true;
        return this;
    },

    endCData : function () {
        if (!this.cdata) return this;
        this.write(']]>');
        this.cdata = 0;
        return this;
    },

    writeRaw : function(content) {
        content = strval(content);
        if (!this.tags && !this.comment && !this.pi && !this.cdata) return this;
        if (this.attributes && this.attribute) {
            ++this.texts;
            this.write(content.replace('&', '&amp;').replace('"', '&quot;'));
            return this;
        } else if (this.attributes && !this.attribute) {
            this.endAttributes();
        }
        ++this.texts;
        this.write(content);
        this.started_write = true;
        return this;
    }

}

module.exports = XMLWriter;


/***/ })
/******/ ]);
});