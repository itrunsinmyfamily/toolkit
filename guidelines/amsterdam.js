const constants = require('./constants')

const MIN_CANCER_PATIENTS = 3
const MAX_ONSET_VALUE = 50
const FIRST_DEG_LINKS = 2
const relativeGeneration = relation => relation.split('.')
  .reduce((v, cv) => v + (cv === 'parent' ? 1 : -1), 0)

export default function amsterdamCriteria(datasheet) {
  const cancers = ['C81', 'C25', 'C29', 'C30']
  const relativeAffected = Object.keys(datasheet.byRelatives).filter(patientId =>
    datasheet.byRelatives[patientId].conditions
      .filter(c => cancers.includes(c.conceptId)).length
  )
  const hasSuccessiveGen = relativeAffected.find(patientId => {
    const relations = datasheet.byRelatives[patientId].relations
    const successiveGen = {
      maternal: [],
      paternal: [],
    }

    Object.keys(relations).forEach(relation => {
      relativeAffected.forEach(r => {
        const familySide = datasheet.byRelatives[r].familySide

        if (familySide !== 'none' && r.patientId !== parseInt(patientId, 10) &&
        relations[relation].includes(parseInt(r, 10))) {
          successiveGen[familySide].push(relation)
        }
      })
    })
    const maternal = successiveGen.maternal.find(r => successiveGen.maternal.find(i =>
      Math.abs(relativeGeneration(r) - relativeGeneration(i)) === 1
    ))
    const paternal = successiveGen.paternal.find(r => successiveGen.paternal.find(i =>
      Math.abs(relativeGeneration(r) - relativeGeneration(i)) === 1
    ))

    return maternal || paternal
  })
  const diagnosedAge = relativeAffected.filter(patientId =>
    datasheet.byRelatives[patientId].conditions
      .filter(c => cancers.includes(c.conceptId) && c.ageOnset < MAX_ONSET_VALUE).length
  )
  const firstDegreeCheck = relativeAffected.find(patientId => {
    const relations = relativeAffected.filter(rId => rId !== patientId)

    return relations.filter(rId =>
      datasheet.byRelatives[rId].relativesByDegree[constants.FDR]
        .find(r => r.patientId === parseInt(patientId, 10))
    ).length >= FIRST_DEG_LINKS
  })

  return !!(relativeAffected.length > MIN_CANCER_PATIENTS && hasSuccessiveGen &&
    diagnosedAge.length && firstDegreeCheck)
}
