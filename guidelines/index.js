const patientData = require('./patientData').default
const rules = require('./rules').default

export default (pedigree, proband, concepts) => {
  const datasheet = patientData(pedigree, proband, concepts)
  const recommendations = rules.filter(rule => rule.condition(datasheet))
    .map(rule => ({
      id: rule.id,
      recommendation: rule.recommendation,
      organization: rule.organization,
      text: rule.text,
    }))

  return recommendations
}
