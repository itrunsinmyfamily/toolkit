/* eslint-disable no-magic-numbers */
const constants = require('./constants')
const amsterdamCriteria = require('./amsterdam').default

const isCloseRelative = r => r.degree === constants.FDR || r.degree === constants.SDR
const isMale = r => r.gender === 'male'
const isFemale = r => r.gender === 'female'

const guidelines = [
  {
    // R1001 First degree relative with Breast cancer dx at age ≤50
    id: 'R1001',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'First degree relative with Breast cancer dx at age ≤50',
    condition: datasheet => datasheet.byCondition.C9.byDegree[constants.FDR]
      .find(r => r.ageOnset <= 50),
  },
  {
    // R1002 First degree relative with ≥2 primary breast cancers
    id: 'R1002',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'First degree relative with ≥2 primary breast cancers',
    condition: datasheet => datasheet.byCondition.C9.byDegree[constants.FDR].length >= 2,
  },
  {
    // R1003 ≥3 cases of breast, ovarian, pancreatic,
    // and/or aggressive prostate cancer in close relatives
    id: 'R1003',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: '≥3 cases of breast / ovarian / pancreatic / aggressive prostate cancer in close relatives',
    condition: datasheet => {
      const breastCancer = datasheet.byCondition.C9
        .filter(isCloseRelative).length
      const ovarian = datasheet.byCondition.C19
        .filter(isCloseRelative).length
      const pancreatic = datasheet.byCondition.C20
        .filter(isCloseRelative).length
      const prostate = datasheet.byCondition.C22
        .filter(isCloseRelative).length

      return (breastCancer + ovarian + pancreatic + prostate) >= 3
    },
  },
  {
    // R1004 First degree relative with ≥3 cases of breast, ovarian, pancreatic,
    // and/or aggressive prostate cancer in close relatives
    id: 'R1004',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'First degree relative with ≥3 cases of breast / ovarian / pancreatic / aggressive prostate cancer',
    condition: datasheet => {
      const breastCancer = datasheet.byCondition.C9.byDegree[constants.FDR].length
      const ovarian = datasheet.byCondition.C19.byDegree[constants.FDR].length
      const pancreatic = datasheet.byCondition.C20.byDegree[constants.FDR].length
      const prostate = datasheet.byCondition.C22.byDegree[constants.FDR].length

      return (breastCancer + ovarian + pancreatic + prostate) >= 3
    },
  },
  {
    // R1005 Breast cancer in two relatives, one dx at age ≤45
    id: 'R1005',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'Breast cancer in two relatives, one dx at age ≤45',
    condition: datasheet => {
      const breastCancer = datasheet.byCondition.C9

      return breastCancer.length >= 2 && breastCancer
        .find(r => r.ageOnset <= 45)
    },
  },
  {
    // R1006 First degree relative with male breast cancer
    id: 'R1006',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'First degree relative with male breast cancer',
    condition: datasheet => datasheet.byCondition.C9.byDegree[constants.FDR]
        .find(isMale),
  },
  {
    // R1007 [First degree relative] with [Colorectal cancer] [dx at age <50]
    id: 'R1007',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'First degree relative with Colorectal cancer dx at age <50',
    condition: datasheet => datasheet.byCondition.C81.byDegree[constants.FDR]
        .find(r => r.ageOnset < 50),
  },
  {
    // R1008 [Colorectal cancer] dx at [age ≥50] and a [FDR] with [colorectal or
    // endometrial cancer] at [any age]
    id: 'R1008',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'Colorectal cancer dx at age ≥50 and a FDR with colorectal or endometrial cancer at any age',
    condition: datasheet => {
      const patientWithCancerAge = datasheet.byCondition.C81.find(r => r.ageOnset >= 50)
      const fdrWithCancer = datasheet.byDegree[constants.FDR]
      .find(r => r.conditions
        .find(c => c.conceptId === 'C81' || c.conceptId === 'C30'))

      return patientWithCancerAge && fdrWithCancer
    },
  },
  {
    // R1009 First degree relative with Colorectal cancer dx at age ≥50 and
    // has a FDR with colorectal or endometrial cancer at any age
    id: 'R1009',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: `First degree relative with Colorectal cancer dx at age ≥50 and
      has a FDR with colorectal or endometrial cancer at any age`,
    condition: datasheet => datasheet.byCondition.C81.byDegree[constants.FDR]
        .find(r => r.ageOnset >= 50 && datasheet.byRelatives[r.memberId]
          .relativesByDegree[constants.FDR].find(m => m.conditions
            .find(c => c.conceptId === 'C81' || c.conceptId === 'C30'))
          ),
  },
  {
    // R1010 ≥2 cases of gastric cancer, one dx at age <50 in close relatives
    id: 'R1010',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: '≥2 cases of gastric cancer, one dx at age <50 in close relatives',
    condition: datasheet => {
      const gastricCancer = datasheet.byCondition.C26
      const closeRelativesCancer = gastricCancer
        .find(r => r.ageOnset < 50 && isCloseRelative(r))

      return gastricCancer.length >= 2 && closeRelativesCancer
    },
  },
  {
    // R1011 First degree relative with ≥2 cases of gastric cancer,
    // one dx at age <50 in close relatives
    id: 'R1011',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'First degree relative with ≥2 cases of gastric cancer, one dx at age <50 in close relatives',
    condition: datasheet => {
      const gastricCancer = datasheet.byCondition.C26.byDegree[constants.FDR]
      const closeRelativesCancer = datasheet.byCondition.C26
        .find(r => r.ageOnset < 50 && isCloseRelative(r))

      return gastricCancer.length >= 2 && closeRelativesCancer
    },
  },
  {
    // R1012 ≥3 cases of gastric cancer in close relatives
    id: 'R1012',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: '≥3 cases of gastric cancer in close relatives',
    condition: datasheet => datasheet.byCondition.C26
      .filter(isCloseRelative).length >= 3,
  },
  {
    // R1013 First degree relative with ≥3 cases of gastric cancer in close relatives
    id: 'R1013',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'First degree relative with ≥3 cases of gastric cancer in close relatives',
    condition: datasheet => datasheet.byDegree[constants.FDR]
      .find(r => {
        const relatives = datasheet.byRelatives[r.memberId].relatives

        return Object.keys(relatives)
          .filter(rid => isCloseRelative(relatives[rid]) && relatives[rid].conditions
            .find(c => c.conceptId === 'C26')).length >= 3
      }),
  },
  {
    // R1014 First degree relative with Diffuse gastric cancer dx at age <40
    id: 'R1014',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'First degree relative with Diffuse gastric cancer dx at age <40',
    condition: datasheet => datasheet.byCondition.C26.byDegree[constants.FDR]
        .find(r => r.ageOnset < 40),
  },
  {
    // R1015 First degree relative with Diffuse gastric cancer and lobular breast cancer
    id: 'R1015',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'First degree relative with Diffuse gastric cancer and lobular breast cancer',
    condition: datasheet => datasheet.byDegree[constants.FDR]
        .find(r => {
          const conditions = r.conditions.map(c => c.conceptId)

          return conditions.includes('C26') && conditions.includes('C9')
        }),
  },
  {
    // R1016 Diffuse gastric cancer in one relative and lobular breast cancer in
    // another, one dx at age <50
    id: 'R1016',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'Diffuse gastric cancer in one relative and lobular breast cancer in another, one dx at age <50',
    condition: datasheet => {
      const gastricCancerPatient = datasheet.byCondition.C26.find(r => r)
      const breastCancerPatient = datasheet.byCondition.C9
        .find(r => gastricCancerPatient && r.memberId !== gastricCancerPatient.memberId)

      return (gastricCancerPatient && breastCancerPatient &&
        (gastricCancerPatient.ageOnset < 50 || breastCancerPatient.ageOnset < 50))
    },
  },
  {
    // R1213 patient has {breast cancer} && a FDR || SDR with {breast cancer} <50
    id: 'R1213',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'You have breast cancer and a FDR or SDR has breast cancer <50',
    condition: datasheet => {
      const probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length
      const closeRelativesCancer = datasheet.byCondition.C9
        .find(c => isCloseRelative(c) && c.ageOnset < 50)

      return probandHasCancer && closeRelativesCancer
    },
  },
  {
    // R1214 patient has {breast cancer} &&  FDR || SDR with {ovarian cancer}
    id: 'R1214',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'Patient has breast cancer and a FDR or SDR has ovarian cancer',
    condition: datasheet => {
      const probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length
      const closeRelativesCancer = datasheet.byCondition.C19.find(isCloseRelative)

      return probandHasCancer && closeRelativesCancer
    },
  },
  {
    // R1215 patient has {breast cancer} && 2+ FDR || SDR with {breast cancer}
    id: 'R1215',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'Patient has breast cancer and 2+ FDR or SDR has breast cancer',
    condition: datasheet => {
      const probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length
      const closeRelativesCancer = datasheet.byCondition.C9.filter(isCloseRelative).length

      return probandHasCancer && closeRelativesCancer >= 2
    },
  },
  {
    // R1216 patient has {breast cancer} && 2+ FDR||SDR with {breast cancer}||{pancreatic cancer}
    id: 'R1216',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'Patient has breast cancer and 2+ FDR or SDR has breast cancer or pancreatic cancer',
    condition: datasheet => {
      const probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length
      const closeRelativesBreastCancer = datasheet.byCondition.C9
        .filter(isCloseRelative).length
      const closeRelativesPancreaticCancer = datasheet.byCondition.C20
        .filter(isCloseRelative).length

      return probandHasCancer && (closeRelativesBreastCancer + closeRelativesPancreaticCancer) >= 2
    },
  },
  {
    // R1217 patient has {breast cancer} && {breast cancer gene} in family
    id: 'R1217',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'Patient has breast cancer and breast cancer gene in family',
    condition: datasheet => {
      const probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length
      const familyBreastCancer = datasheet.byCondition.C9
        .find(c => c.degree !== constants.PROBAND)

      return probandHasCancer && familyBreastCancer
    },
  },
  {
    // R1218 FDR || SDR with {breast cancer} <45yo
    id: 'R1218',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'FDR or SDR with breast cancer at age <45',
    condition: datasheet => datasheet.byCondition.C9
      .find(r => isCloseRelative(r) && r.ageOnset < 45),
  },
  {
    // R1232 FDR || SDR male relative with {breast cancer}
    id: 'R1232',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'FDR or SDR male relative with breast cancer',
    condition: datasheet => datasheet.byCondition.C9
      .find(r => isCloseRelative(r) && isMale(r)),
  },
  {
    // R1233 FDR with {breast cancer} <50yo && another {any cancer} in same person
    id: 'R1233',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'FDR with breast cancer at age <50 and another any cancer in same person',
    condition: datasheet => datasheet.byDegree[constants.FDR].find(r =>
        r.conditions.find(c => c.conceptId === 'C9' && c.ageOnset < 50)
        && r.conditions.find(c => c.conceptId !== 'C9')),
  },
  {
    // R1234 FDR with {breast cancer} <50yo && another relative with {breast cancer} at any age
    id: 'R1234',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'FDR with breast cancer at age <50 and other relative with breast cancer at any age',
    condition: datasheet =>
      datasheet.byCondition.C9.filter(r => r.degree === constants.FDR && r.ageOnset < 50)
      .find(r => datasheet.byCondition.C9
        .find(c => c.memberId !== r.memberId)),
  },
  {
    // R1235 FDR with {breast cancer} <50yo && is adopted
    id: 'R1235',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'FDR with breast cancer at age <50 and is adopted',
    condition: datasheet => datasheet.byCondition.C9.byDegree[constants.FDR]
      .find(r => r.ageOnset < 50 && r.isAdopted),
  },
  {
    // R1236 SDR with {breast cancer} <50yo && another {any cancer} in same person
    id: 'R1236',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'SDR with breast cancer at age <50 and another any cancer in same person',
    condition: datasheet => datasheet.byCondition.C9.byDegree[constants.SDR]
      .find(r => r.ageOnset < 50 && datasheet.byRelatives[r.memberId].conditions
        .find(c => c.conceptId !== 'C9')),
  },
  {
    // R1237 SDR with {breast cancer} <50yo && another relative with {breast cancer} at any age
    id: 'R1237',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'SDR with breast cancer at age <50 and another relative with breast cancer at any age',
    condition: datasheet => datasheet.byCondition.C9.byDegree[constants.SDR]
        .find(r => r.ageOnset < 50 && datasheet.byCondition.C9
          .find(m => m.memberId !== r.memberId)),
  },
  {
    // R1238 SDR with {breast cancer} <50yo && is adopted
    id: 'R1238',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'SDR with breast cancer at age <50 and is adopted',
    condition: datasheet => datasheet.byCondition.C9.byDegree[constants.SDR]
      .find(r => r.ageOnset < 50 && r.isAdopted),
  },
  {
    // R1239 {breast cancer gene} in family
    id: 'R1239',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'Breast cancer gene in family',
    condition: datasheet => datasheet.byCondition.C9
      .filter(r => r.degree !== constants.PROBAND).length,
  },
  {
    // R1240 2+ relatives on same side of family with {breast cancer}
    id: 'R1240',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: '2+ relatives on same side of family with breast cancer',
    condition: datasheet => {
      const relatives = datasheet.byRelatives
      const familySides = {}

      Object.keys(relatives).map(rId => relatives[rId]).forEach(relative => {
        const hasBreastCancer = relative.conditions.map(c => c.conceptId).includes('C9')

        if (relative.familySide !== 'none' && hasBreastCancer) {
          (familySides[relative.familySide] || (familySides[relative.familySide] = []))
          .push(relative)
        }
      })

      return Object.keys(familySides).find(side => familySides[side].length >= 2)
    },
  },
  {
    // R1241 a relative with 2+ {breast cancers} in a single person
    id: 'R1241',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'A relative with 2+ breast cancers in a single person',
    condition: datasheet =>
      Object.keys(datasheet.byRelatives).find(rId =>
        datasheet.byRelatives[rId].conditions.filter(c => c.conceptId === 'C9').length >= 2
      ),
  },
  {
    // R1242 patient w/{gastric cancer} && {breast cancer} in FDR||SDR w/a Dx <50
    id: 'R1242',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'Patient has gastric cancer and breast cancer in FDR or SDR under 50 yrs old',
    condition: datasheet => {
      const probandHasCancer = datasheet.byCondition.C26.byDegree[constants.PROBAND].length
      const relativesHavingCancer = datasheet.byCondition.C9
        .find(r => isCloseRelative(r) && r.ageOnset < 50)

      return probandHasCancer && relativesHavingCancer
    },
  },
  {
    // R1243 relative w/{gastric cancer} <40yo
    id: 'R1243',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'A relative with gastric cancer under 40 yrs old',
    condition: datasheet => datasheet.byCondition.C26.find(r => r.ageOnset < 40),
  },
  {
    // R1244 2+ FDR||SDR w/{gastric cancer} w/a dx <50
    id: 'R1244',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: '2+ FDR or SDR with gastric cancer under 50 yrs old',
    condition: datasheet => {
      const relatives = datasheet.byCondition.C26.filter(r => isCloseRelative(r))
      const relativesWithOnset = relatives.find(r => r.ageOnset < 50)

      return relatives.length >= 2 && relativesWithOnset
    },
  },
  {
    // R1245 3+ FDR||SDR w/{gastric cancer}
    id: 'R1245',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: '3+ FDR or SDR with gastric cancer',
    condition: datasheet => datasheet.byCondition.C26
      .filter(r => isCloseRelative(r)).length >= 3,
  },
  {
    // R1246 {gastric cancer gene} in family
    id: 'R1246',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'Gastric cancer gene in family',
    condition: datasheet => datasheet.byCondition.C26
      .find(r => r.degree !== constants.PROBAND),
  },
  {
    // R1248 meets {amsterdam}
    id: 'R1248',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'Meets Amsterdam criteria',
    condition: datasheet => amsterdamCriteria(datasheet),
  },
  {
    // R1020 rel w/3+ cases of {melanoma a/o pancreatic cancer}
    id: 'R1020',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'Relative with 3+ cases of melenoma / pancreatic cancer',
    condition: datasheet => {
      const melanoma = datasheet.byCondition.C41.filter(isCloseRelative)
      const pancreatic = datasheet.byCondition.C20.filter(isCloseRelative)

      return (melanoma.length + pancreatic.length) >= 3
    },
  },
  {
    // R1022 FDR with 3+ melanomas
    id: 'R1022',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'FDR with 3+ melanoma cancer',
    condition: datasheet => datasheet.byCondition.C41
      .filter(r => r.degree === constants.FDR).length >= 3,
  },
  {
    // R1023 FDR with {melanoma}&&{pancreastic cancer}
    id: 'R1023',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'FDR with melanoma and pancreatic cancer',
    condition: datasheet => datasheet.byDegree[constants.FDR]
      .filter(r => {
        const conditions = r.conditions.map(c => c.conceptId)

        return conditions.includes('C41') && conditions.includes('C20')
      }).length,
  },
  {
    // R1024 {melanoma}&&{astrocytoma} in single FDR
    id: 'R1024',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'FDR with melanoma and astrocytoma cancer',
    condition: datasheet => datasheet.byDegree[constants.FDR]
      .filter(r => {
        const conditions = r.conditions.map(c => c.conceptId)

        return conditions.includes('C41') && conditions.includes('C82')
      }).length,
  },
  {
    // R1025 {melanoma}&&{astrocytoma} in 2 FDR
    id: 'R1025',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: '2 FDR with melanoma and astrocytoma cancer',
    condition: datasheet => datasheet.byDegree[constants.FDR]
      .filter(r => {
        const conditions = r.conditions.map(c => c.conceptId)

        return conditions.includes('C41') && conditions.includes('C82')
      }).length >= 2,
  },
  {
    // R1026 pt dx w/ {pancreatic cancer} && 2+ {pancreatic cancer} in relative
    id: 'R1026',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'Patient with pancreatic cancer and 2+ pancreatic cancer in relatives',
    condition: datasheet => {
      let probandHasCancer = false
      const relativesHavingCancer = datasheet.byCondition.C20.filter(r => {
        if (r.degree === constants.PROBAND) {
          probandHasCancer = true
        }
        return isCloseRelative(r)
      })

      return probandHasCancer && relativesHavingCancer.length >= 2
    },
  },
  {
    // R1028 pt dx w/{pancreatic cancer} && 2+ {breast, ovarian, prostate caner} in relative
    id: 'R1028',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'Patient with pancreatic cancer and 2+ pancreatic / breast / prostate cancer in relatives',
    condition: datasheet => {
      const probandHasCancer = datasheet.byCondition.C20.byDegree[constants.PROBAND].length
      const relativesHavingCancer = Object.keys(datasheet.byRelatives).find(rId => {
        const relative = datasheet.byRelatives[rId]
        const breastCancer = relative.conditions.filter(c => c.conceptId === 'C9').length
        const ovarianCancer = relative.conditions.filter(c => c.conceptId === 'C19').length
        const prostateCancer = relative.conditions.filter(c => c.conceptId === 'C22').length

        return isCloseRelative(relative) &&
          (breastCancer >= 2 || ovarianCancer >= 2 || prostateCancer >= 2)
      })

      return probandHasCancer && relativesHavingCancer
    },
  },
  {
    // R1033 3+ dx of {pancreatic cancer || melanoma} in single relative
    id: 'R1033',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: '3+ pancreatic or melanoma cancer in single relative',
    condition: datasheet => {
      const relatives = datasheet.byRelatives

      return Object.keys(relatives).find(rId => {
        const melanoma = relatives[rId].conditions.filter(c => c.conceptId === 'C41')
        const pancreatic = relatives[rId].conditions.filter(c => c.conceptId === 'C20')

        return isCloseRelative(relatives[rId]) && (melanoma.length >= 3 || pancreatic.length >= 3)
      })
    },
  },
  {
    // R1035 FDR with {pancreatic cancer && melanoma}
    id: 'R1035',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'FDR with pancreatic and melanoma cancer',
    condition: datasheet =>
      datasheet.byDegree[constants.FDR].find(r => {
        const conditions = r.conditions.map(c => c.conceptId)

        return conditions.includes('C41') && conditions.includes('C20')
      }),
  },
  {
    // R1036 2+ relateives with {prostate cancer} dx <56
    id: 'R1036',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: '2+ relatives with prostate cancer under 56 yrs old',
    condition: datasheet =>
      datasheet.byCondition.C22.filter(r => r.ageOnset < 56).length >= 2,
  },
  {
    // R1038 3+ FDR with prostate cancer
    id: 'R1038',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: '3+ FDR with prostate cancer',
    condition: datasheet =>
      datasheet.byCondition.C22.byDegree[constants.FDR].length >= 3,
  },
  {
    // R1040 Pt dx w/ {prostate cancer} && 2+ {breast, ovarian, pancreatic cancer} in relative
    id: 'R1040',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'ACMG',
    text: 'Patient with prostate cancer and 2+ breast / ovarian / pancreatic cancer in relative',
    condition: datasheet => {
      const probandHasCancer = datasheet.byCondition.C22.byDegree[constants.PROBAND].length
      const relativesHavingCancer = Object.keys(datasheet.byRelatives).find(rId => {
        const relative = datasheet.byRelatives[rId]
        const breastCancer = relative.conditions.filter(c => c.conceptId === 'C9').length
        const ovarianCancer = relative.conditions.filter(c => c.conceptId === 'C19').length
        const pancreaticCancer = relative.conditions.filter(c => c.conceptId === 'C20').length

        return isCloseRelative(relative) &&
          (breastCancer >= 2 || ovarianCancer >= 2 || pancreaticCancer >= 2)
      })

      return probandHasCancer && relativesHavingCancer
    },
  },
  {
    // R1257 If you do not have [breast cancer] but do have a [first or second degree relative]
    // with [breast cancer] [prior to or at] [45] yo;
    id: 'R1257',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'FDR or SDR with breast cancer under 45 yrs old',
    condition: datasheet => {
      const probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length
      const relativesHavingCancer = datasheet.byCondition.C9
        .filter(r => isCloseRelative(r) && r.ageOnset <= 45)

      return !probandHasCancer && relativesHavingCancer.length
    },
  },
  {
    // R1258 [Personal history] of [breast cancer] and [diagnosed] [at any age]
    // with  a [close male blood relative] with [breast cancer]
    id: 'R1258',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'Personal history of breast cancer and a close male relative with breast cancer',
    condition: datasheet => {
      const probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length
      const relativesHavingCancer = datasheet.byCondition.C9
        .filter(r => isCloseRelative(r) && isMale(r))

      return probandHasCancer && relativesHavingCancer.length
    },
  },
  {
    // R1250 patient with {ovarian cancer} && relative with {colon cancer}||{breast cancer}
    id: 'R1250',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'Personal history of ovarian cancer and a relative with colon cancer or breast cancer',
    condition: datasheet => {
      const probandHasCancer = datasheet.byCondition.C19.byDegree[constants.PROBAND].length
      const colonCancer = datasheet.byCondition.C47.length
      const breastCancer = datasheet.byCondition.C9.length

      return probandHasCancer && (colonCancer || breastCancer)
    },
  },
  {
    // R1251 2+ FDR w/{ovarian cancer}
    id: 'R1251',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: '2+ FDR with ovarian cancer',
    condition: datasheet => datasheet.byCondition.C19.byDegree[constants.FDR].length >= 2,
  },
  {
    // R1252 relative w/{lynch syndrome}
    id: 'R1252',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'Relative with lynch syndromes',
    condition: datasheet => datasheet.byCondition.S1.length,
  },
  {
    // R1322 if anyone in family has {Peutz-Jeghers syndrome, Li-Fruameni,
    // Cowden or Lynch syndrome}
    id: 'R1322',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'Relative with Peutz-Jeghers, Li-Fruameni, Cowden or Lynch syndrome',
    condition: datasheet => {
      const peutSyndrome = datasheet.byCondition.S5.length
      const liFruameni = datasheet.byCondition.S3.length
      const lynchSyndrome = datasheet.byCondition.S1.length
      const cowdenSyndrome = datasheet.byCondition.S17.length

      return peutSyndrome || liFruameni || lynchSyndrome || cowdenSyndrome
    },
  },
  {
    // R1325 pt dx w/{pancreatic cancer} && 2+ relatives with
    // {breast, ovarian, pancreatic, prostate cancer}
    id: 'R1325',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'Personal history of pancreatic cancer and 2+ relatives with breast, ovarian, pancreatic or prostate cancer',
    condition: datasheet => {
      const probandHasCancer = datasheet.byCondition.C20.byDegree[constants.PROBAND].length
      const relativesHavingCancer = Object.keys(datasheet.byRelatives).filter(rId => {
        const relative = datasheet.byRelatives[rId]

        return relative.conditions.filter(c =>
          c.conceptId === 'C9' || c.conceptId === 'C19' ||
          c.conceptId === 'C20' || c.conceptId === 'C22').length
      })

      return probandHasCancer && relativesHavingCancer.length >= 2
    },
  },
  {
    // R1326 pt dx w/ {breast cancer} && 2+ relatives w/ {breast cancer}
    id: 'R1326',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'Personal history of breast cancer and 2+ relatives with breast cancer',
    condition: datasheet => {
      const probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length
      const relativesHavingCancer = datasheet.byCondition.C9.filter(isCloseRelative)

      return probandHasCancer && relativesHavingCancer.length >= 2
    },
  },
  {
    // R1327 pt dx w/ {breast cancer} && 1+ relatives w/ {ovarian cancer}
    id: 'R1327',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: 'Personal history of breast cancer and 1+ relatives with ovarian cancer',
    condition: datasheet => {
      const probandHasCancer = datasheet.byCondition.C9.byDegree[constants.PROBAND].length
      const relativesHavingCancer = datasheet.byCondition.C19.filter(isCloseRelative)

      return probandHasCancer && relativesHavingCancer.length >= 1
    },
  },
  {
    // R1328 1+ relatives on same side with {ovarian cnacer}
    id: 'R1328',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: '1+ relatives on same side of family with ovarian cancer',
    condition: datasheet => {
      const probandMutation = datasheet.byDegree[constants.PROBAND]
        .filter(r => r.conditions
          .filter(c => c.conceptId === 'G3' || c.conceptId === 'G6').length).length
      const relatives = {
        maternal: [],
        paternal: [],
      }

      datasheet.byCondition.C19.forEach(r => {
        if (r.familySide !== 'none') {
          relatives[r.familySide].push(r)
        }
      })

      return !probandMutation && (relatives.maternal.length >= 1 || relatives.paternal.length >= 1)
    },
  },
  {
    // R1249 2+ relatives w/{melanoma}
    id: 'R1249',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: '2+ relatives with melenoma cancer',
    condition: datasheet => datasheet.byCondition.C41.length >= 2,
  },
  {
    // patient with {uterine cancer} <50yo &&
    // relative with {endometrial cancer}||{colorectal cancer}
    id: 'R1253',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'NCCN v1.2018',
    text: `Personal history of utherine cancer under 50 yrs old and
      a relative with endometrial cancer or colorectal cancer`,
    condition: datasheet => {
      const probandHasCancer = datasheet.byCondition.C30
        .byDegree[constants.PROBAND].filter(r => r.ageOnset < 50).length
      const endometrial = datasheet.byCondition.C30.find(r => r.degree !== constants.PROBAND)
      const colorectal = datasheet.byCondition.C81.find(r => r.degree !== constants.PROBAND)

      return probandHasCancer && (endometrial || colorectal)
    },
  },
  {
    // R1523 2+ FDR||SDR with {ovarian cancer} || {breast cancer & ovarian cancer}
    id: 'R1523',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'USPSTF',
    text: '2+ FDR or SDR with ovarian cancer or (breast cancer and ovarian cancer)',
    condition: datasheet => {
      const probandIsFemale = datasheet.byDegree[constants.PROBAND].find(isFemale)
      const relativesHavingCancer = datasheet.byCondition.C19.filter(isCloseRelative)

      return probandIsFemale && relativesHavingCancer.length >= 2
    },
  },
  {
    // R1524 1 FDR w {breast cancer} || {ovarian cancer}
    id: 'R1524',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'USPSTF',
    text: '1 FDR with breast cancer or ovarian cancer',
    condition: datasheet => {
      const breastCancer = datasheet.byCondition.C9.find(r => r.degree === constants.FDR)
      const ovarianCancer = datasheet.byCondition.C19.find(r => r.degree === constants.FDR)

      return breastCancer || ovarianCancer
    },
  },
  {
    // R1525 2 SDR same side of family w {breast cancer} || {ovarian cancer}
    id: 'R1525',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'USPSTF',
    text: '2 SDR on same side of family with breast cancer or ovarian cancer',
    condition: datasheet => {
      const relatives = datasheet.byRelatives
      const familySides = {}

      Object.keys(relatives).map(rId => relatives[rId]).forEach(relative => {
        const conditions = relative.conditions.map(c => c.conceptId)
        const hasCancer = conditions.includes('C9') || conditions.includes('C19')

        if (relative.familySide !== 'none' && hasCancer && relative.degree === constants.SDR) {
          (familySides[relative.familySide] || (familySides[relative.familySide] = []))
          .push(relative)
        }
      })

      return Object.keys(familySides).find(side => familySides[side].length === 2)
    },
  },
  {
    // R1513 If you have a first-degree relative who has had colorectal adenomas
    id: 'R1513',
    recommendation: constants.EARLIER_SCREENING,
    organization: 'USPSTF',
    text: 'FDR with colorectal adenomas',
    condition: datasheet => datasheet.byCondition.S4.byDegree[constants.FDR].length,
  },
  {
    // R1514 If you have a family history of skin cancer,
    id: 'R1514',
    recommendation: constants.EARLIER_SCREENING,
    organization: 'USPSTF',
    text: 'Family history of skin cancer',
    condition: datasheet => datasheet.byCondition.C24.length,
  },
  {
    // R1515 If you have a family history of prostate cancer
    id: 'R1515',
    recommendation: constants.EARLIER_SCREENING,
    organization: 'USPSTF',
    text: 'Family history of prostate cancer',
    condition: datasheet => datasheet.byCondition.C22.length,
  },
  {
    // R1519 If [women] test negative for [BRCA 1 & 2] mutation but
    // have [relatives] with known [BRCA mutations]
    id: 'R1519',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'USPSTF',
    text: 'Personal history of test negative BRCA 1 and BRCA 2 mutation and a relative with known BRCA mutations',
    condition: datasheet => {
      const proband = datasheet.byDegree[constants.PROBAND].find(r => {
        const conditions = r.conditions.map(c => c.conceptId)

        return !conditions.includes('G6') && !conditions.includes('G3') && isFemale(r)
      })
      const relativesWithMutation =
        datasheet.byCondition.G6.length || datasheet.byCondition.G3.length

      return proband && relativesWithMutation
    },
  },
  {
    // R1520 If [women] who have been diagnosed with [breast cancer] in the past
    // and who did not receive [BRCA testing] as part of their cancer care
    // but have a [family history] of [breast or ovarian cancer]
    id: 'R1520',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'USPSTF',
    text: `Personal history of breast cancer in the past and did not receive
      BRCA testing and family history of breast or ovarian cancer`,
    condition: datasheet => {
      const proband = datasheet.byDegree[constants.PROBAND].find(r => {
        const conditions = r.conditions.map(c => c.conceptId)
        const brcaTesting = conditions.includes('G3') || conditions.includes('G6')

        return conditions.includes('C9') && !brcaTesting
      })
      const breastCancer = datasheet.byCondition.C9.find(r => r.degree !== constants.PROBAND)
      const ovarianCancer = datasheet.byCondition.C19.find(r => r.degree !== constants.PROBAND)

      return proband && (breastCancer || ovarianCancer)
    },
  },
  {
    // R1522 If you have a family history of Lynch syndrome
    id: 'R1522',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'USPSTF',
    text: 'Family history of Lynch syndrome',
    condition: datasheet => datasheet.byCondition.S1.length,
  },
  {
    // R1521 If [women] who have [1] [or more] [family members]
    // with a [known potentially harmful mutation] in the [BRCA1 or BRCA2 genes]
    id: 'R1521',
    recommendation: constants.GENETIC_COUNSELING,
    organization: 'USPSTF',
    text: '1+ relatives with known potentially harmful mutation in the BRCA 1 or BRCA 2 genes',
    condition: datasheet => {
      const proband = datasheet.byDegree[constants.PROBAND].find(isFemale)
      const brcaOne = datasheet.byCondition.G6.find(r => r.degree !== constants.PROBAND)
      const brcaTwo = datasheet.byCondition.G3.find(r => r.degree !== constants.PROBAND)

      return proband && (brcaOne || brcaTwo)
    },
  },
]

export default guidelines
