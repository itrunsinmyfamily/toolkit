export const degreeByRelation = {
  proband: 0,
  parent: 1,
  child: 1,
  'parent.child': 1,
  'parent.parent': 2,
  'child.child': 2,
  'parent.child.child': 2,
  'parent.parent.child': 2,
  'parent.parent.parent': 3,
  'child.child.child': 3,
  'parent.parent.child.child': 3,
  'parent.child.child.child': 3,
  'parent.parent.parent.child': 3,
}
export const MAX_DEGREE = 3
export const GENETIC_COUNSELING = 'genetic-counseling'
export const EARLIER_SCREENING = 'earlier-screening'
export const PROBAND = 0
export const FDR = 1
export const SDR = 2

export default {
  degreeByRelation,
  MAX_DEGREE,
}
