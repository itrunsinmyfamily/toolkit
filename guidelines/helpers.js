const records = require('../records').default
const degreeByRelation = require('./constants').default.degreeByRelation

const findGender = parent => {
  const life = new records.RecordBundle(parent.PHR).getRecords('Life')[0]

  return life.getValue('gender')
}

const memberRelation = (relations, relativeId) => {
  const relationPath = Object.keys(relations).find(relation =>
    relations[relation].includes(relativeId)) || 'proband'

  return degreeByRelation[relationPath]
}

const familySides = (pedigree, probandId) => {
  const relations = pedigree[probandId].relations
  const family = {
    maternal: [],
    paternal: [],
  }

  relations.parent.forEach(parent => {
    const data = pedigree[parent]
    const display = findGender(data)

    if (display === 'male') {
      family.paternal.push(parent)
      Object.keys(data.relations).forEach(relation => {
        if (relation.startsWith('parent')) {
          family.paternal.push(...data.relations[relation])
        }
      })
      return
    }

    family.maternal.push(parent)
    Object.keys(data.relations).forEach(relation => {
      if (relation.startsWith('parent')) {
        family.maternal.push(...data.relations[relation])
      }
    })
  })

  return family
}

const memberSide = (pedigree, proband, relativeId) => {
  const family = familySides(pedigree, proband)

  if (family.maternal.includes(relativeId)) {
    return 'maternal'
  }

  if (family.paternal.includes(relativeId)) {
    return 'paternal'
  }

  return 'none'
}

const patientConditions = (currentPatient, concepts) => {
  const patientRecords = currentPatient.getAllRecords()
  const included = []

  Object.keys(patientRecords).forEach(conceptId => {
    if (concepts[conceptId].path.startsWith('Patient.Condition.')) {
      patientRecords[conceptId].forEach(recordInstance => {
        included.push({
          conceptId,
          ageOnset: recordInstance.getValue('age-start'),
        })
      })
    }
  })

  return included
}

const memberAge = life => {
  const dYear = life.getValue('death-year')
  const dMonth = life.getValue('death-month')
  const dDay = life.getValue('death-day')
  const bYear = life.getValue('birth-year')
  const bMonth = life.getValue('birth-month')
  const bDay = life.getValue('birth-day')
  const today = new Date()
  let age = null
  let monthDiff
  let dayCheck

  if (bYear) {
    age = dYear ? (dYear - bYear) : (today.getFullYear() - bYear)
    if (bMonth && bDay) {
      if (dMonth && dDay) {
        monthDiff = dMonth - bMonth
        dayCheck = dDay < bDay
      } else {
        monthDiff = (today.getMonth() + 1) - bMonth
        dayCheck = today.getDate() < bDay
      }

      if (monthDiff <= 0 && dayCheck) {
        age--
      }
    }
  }
  return age
}

const relativesWithCondition = (pedigree, proband, cancerId) => {
  const relatives = []

  Object.keys(pedigree).forEach(patientId => {
    if (patientId === 'stats') return

    const relativePhr = new records.RecordBundle(pedigree[patientId].PHR)
    const relativeLife = relativePhr.getRecords('Life')[0]
    const relations = pedigree[proband].relations
    const relativeRecord = relativePhr.getRecords(cancerId)
    const adopted = relativeLife.getResponse('adopted')

    relativeRecord.forEach(record => {
      relatives.push({
        memberId: patientId,
        ageOnset: record.getValue('age-start'),
        age: memberAge(relativeLife),
        gender: relativeLife.getValue('gender'),
        degree: memberRelation(relations, parseInt(patientId, 10)),
        familySide: memberSide(pedigree, proband, parseInt(patientId, 10)),
        isAdopted: !!(adopted && adopted.responseType !== records.RESPONSE_NONE),
        relations: pedigree[patientId].relations,
      })
    })
  })

  return relatives
}

const relativesWithDegree = (pedigree, proband, concepts) => {
  const relatives = {}

  Object.keys(pedigree).forEach(patientId => {
    if (patientId === 'stats') return

    const relations = pedigree[proband].relations
    const degree = memberRelation(relations, parseInt(patientId, 10))

    if (degree === undefined) return

    if (!relatives[degree]) {
      relatives[degree] = []
    }

    const currentPatient = new records.RecordBundle(pedigree[patientId].PHR)
    const included = patientConditions(currentPatient, concepts)

    const relativeLife = currentPatient.getRecords('Life')[0]
    const adopted = relativeLife.getResponse('adopted')

    relatives[degree].push({
      memberId: patientId,
      age: memberAge(relativeLife),
      gender: relativeLife.getValue('gender'),
      familySide: memberSide(pedigree, proband, parseInt(patientId, 10)),
      isAdopted: !!(adopted && adopted.responseType !== records.RESPONSE_NONE),
      conditions: included,
      relations: pedigree[patientId].relations,
    })
  })

  return relatives
}

const patientWithDiseases = (pedigree, proband, concepts) => {
  const result = {}

  Object.keys(pedigree).forEach(patientId => {
    if (patientId === 'stats') return

    const probandRelations = pedigree[proband].relations
    const relativeRelations = pedigree[patientId].relations
    const currentPatient = new records.RecordBundle(pedigree[patientId].PHR)
    const life = currentPatient.getRecords('Life')[0]
    const patientAdopted = life.getResponse('adopted')
    const relativesByDegree = {}
    const relatives = {}

    Object.keys(relativeRelations).forEach(relation => {
      relativeRelations[relation].forEach(relativeId => {
        const relativePhr = new records.RecordBundle(pedigree[relativeId].PHR)
        const relativeLife = relativePhr.getRecords('Life')[0]
        const adopted = relativeLife.getResponse('adopted')

        relatives[relativeId] = {
          age: memberAge(relativeLife),
          gender: relativeLife.getValue('gender'),
          degree: memberRelation(relativeRelations, parseInt(relativeId, 10)),
          familySide: memberSide(pedigree, proband, parseInt(patientId, 10)),
          isAdopted: !!(adopted && adopted.responseType !== records.RESPONSE_NONE),
          conditions: patientConditions(relativePhr, concepts),
          patientId: relativeId,
          relations: pedigree[relativeId].relations,
        }
      })
    })

    Object.keys(relatives).forEach(relativeId => {
      const degree = relatives[relativeId].degree

      if (!relativesByDegree[degree]) {
        relativesByDegree[degree] = []
      }
      relativesByDegree[degree].push(relatives[relativeId])
    })

    result[patientId] = {
      age: memberAge(life),
      gender: life.getValue('gender'),
      degree: memberRelation(probandRelations, parseInt(patientId, 10)),
      isAdopted: !!(patientAdopted && patientAdopted.responseType !== records.RESPONSE_NONE),
      conditions: patientConditions(currentPatient, concepts),
      relatives,
      relativesByDegree,
      familySide: memberSide(pedigree, parseInt(proband, 10), parseInt(patientId, 10)),
      relations: pedigree[patientId].relations,
      patientId,
    }
  })
  return result
}

module.exports = {
  relativesWithCondition,
  relativesWithDegree,
  patientWithDiseases,
}
