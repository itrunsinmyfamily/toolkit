const helpers = require('./helpers')
const maxDegree = require('./constants').default.MAX_DEGREE

export default function patientData(pedigree, proband, concepts) {
  const datasheet = {
    byCondition: {},
    byDegree: {},
    byRelatives: helpers.patientWithDiseases(pedigree, proband, concepts),
  }

  Object.keys(concepts).forEach(concept => {
    if (concepts[concept].path.indexOf('Patient.Condition.') === -1) return

    datasheet.byCondition[concept] = helpers.relativesWithCondition(pedigree, proband, concept)
    datasheet.byCondition[concept].byDegree = {}
    for (let degree = 0; degree <= maxDegree; degree++) {
      datasheet.byCondition[concept].byDegree[degree] =
        datasheet.byCondition[concept].filter(patient => patient.degree === degree)
    }
  })

  datasheet.byDegree = helpers.relativesWithDegree(pedigree, proband, concepts)

  return datasheet
}
