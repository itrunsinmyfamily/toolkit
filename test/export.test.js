/* eslint prefer-template: "off" */
const faker = require('faker')
const fhir = require('fhir-validator')
const appRoot = require('app-root-path')
const chai = require('chai')
const xmldoc = require('xmldoc')

const expect = chai.expect
const Export = appRoot.require('/export')
const records = require('../records').default

chai.use(require('dirty-chai'))

function setPhr(response) {
  const code = 'Life'
  const bundle = new records.RecordBundle()
  const record = bundle.addRecord(code)

  Object.keys(response).forEach(key => {
    record.setResponse(key, {
      responseType: response[key].responseType,
      value: response[key].value || undefined,
    })
  })
  return bundle
}
describe('Export HL7', function () {
  it('should create xml with minimal data', function (done) {
    const probandId = faker.random.number()
    const pedigree = {
      [probandId]: {
        patientId: probandId,
        PHR: setPhr({}),
      },
    }
    const hls = Export.HL7(pedigree, probandId)
    const xml = new xmldoc.XmlDocument(hls)

    expect(xml.name).to.be.eql('FamilyHistory')
    expect(xml.valueWithPath('subject.patient.patientPerson.id@extension')).to.be.equal(`${probandId}`)
    done()
  })

  it('should verify proband data correctly passed', function (done) {
    const probandId = faker.random.number()
    const firstName = faker.random.word()
    const birthYear = faker.random.number({ min: 1950, max: 2000 })
    const pedigree = {
      [probandId]: {
        patientId: probandId,
        PHR: setPhr({
          gender: {
            responseType: 'value',
            value: 'male',
          },
          'first-name': {
            responseType: 'value',
            value: firstName,
          },
          'birth-year': {
            responseType: 'value',
            value: birthYear,
          },
        }),
      },
    }
    const hls = Export.HL7(pedigree, probandId)
    const xml = new xmldoc.XmlDocument(hls)

    expect(xml.name).to.be.eql('FamilyHistory')
    expect(xml.valueWithPath('subject.patient.patientPerson.id@extension')).to.be.equal(`${probandId}`)
    expect(xml.valueWithPath('subject.patient.patientPerson.administrativeGenderCode@code'))
    .to.be.equal('M')
    expect(xml.valueWithPath('subject.patient.patientPerson.birthTime@value'))
      .to.be.equal(`${birthYear}`)
    expect(xml.valueWithPath('subject.patient.patientPerson.name')).to.be.equal(firstName)
    done()
  })
  it('should verify number of relatives', function (done) {
    const probandId = faker.random.number()
    const parentA = faker.random.number()
    const parentB = faker.random.number()
    const pedigree = {
      [probandId]: {
        patientId: probandId,
        PHR: setPhr({}),
        relations: {
          parent: [parentA, parentB],
        },
      },
      [parentA]: {
        patientId: parentA,
        PHR: setPhr({}),
      },
      [parentB]: {
        patientId: parentB,
        PHR: setPhr({}),
      },
    }
    const hls = Export.HL7(pedigree, probandId)
    const xml = new xmldoc.XmlDocument(hls)
    const patientPerson = xml.descendantWithPath('subject.patient.patientPerson')

    expect(xml.name).to.be.eql('FamilyHistory')

    expect(patientPerson.childrenNamed('relative').length).to.equal(2)
    done()
  })
  it('should verify relative data passed correctly', function (done) {
    const probandId = faker.random.number()
    const parentA = faker.random.number()
    const firstName = faker.random.word()
    const birthYear = faker.random.number({ min: 1950, max: 2000 })
    const pedigree = {
      [probandId]: {
        patientId: probandId,
        PHR: setPhr({}),
        relations: {
          parent: [parentA],
        },
      },
      [parentA]: {
        patientId: parentA,
        PHR: setPhr({
          gender: {
            responseType: 'value',
            value: 'male',
          },
          'first-name': {
            responseType: 'value',
            value: firstName,
          },
          'birth-year': {
            responseType: 'value',
            value: birthYear,
          },
        }),
      },
    }
    const hls = Export.HL7(pedigree, probandId)
    const xml = new xmldoc.XmlDocument(hls)
    const relative = xml.descendantWithPath('subject.patient.patientPerson.relative')

    expect(relative.valueWithPath('relationshipHolder.id@extension')).to.be.equal(`${parentA}`)
    expect(relative.valueWithPath('relationshipHolder.administrativeGenderCode@code'))
      .to.be.equal('M')
    expect(relative.valueWithPath('relationshipHolder.birthTime@value')).to.be.equal(`${birthYear}`)
    expect(relative.valueWithPath('relationshipHolder.name')).to.be.equal(firstName)
    expect(xml.name).to.be.eql('FamilyHistory')
    done()
  })
})
describe('export fhir json', function () {
  it('should contain all family members in fhir resource', function (done) {
    const probandId = faker.random.number()
    const patientIdOne = faker.random.number()
    const patientIdTwo = faker.random.number()
    const invite = {
      invitedBy: faker.random.word(),
    }
    const fakeResponses = [{}, {}, {}]

    const pedigree = {
      [probandId]: {
        patientId: probandId,
        PHR: setPhr(fakeResponses[0]),
      },
      [patientIdOne]: {
        patientId: patientIdOne,
        PHR: setPhr(fakeResponses[1]),
      },
      [patientIdTwo]: {
        patientId: patientIdTwo,
        PHR: setPhr(fakeResponses[2]),
      },
    }

    const fhirResource = Export.fhir(pedigree, probandId, invite)
    const validationResult = fhir.validate(fhirResource)

    expect(fhirResource.contained.length).to.equal(3)
    expect(validationResult.errors.length).to.equal(0)

    done()
  })

  it('should define all properties for each family member', function (done) {
    const probandId = faker.random.number()

    const pedigree = {
      [probandId]: {
        patientId: probandId,
        PHR: setPhr({}),
      },
    }

    const fhirResource = Export.fhir(pedigree, probandId, {})

    expect(Object.keys(fhirResource.contained[0]).length).to.equal(19)

    done()
  })

  it('should return correct data if invite empty', function (done) {
    const probandId = faker.random.number()

    const pedigree = {
      [probandId]: {
        patientId: probandId,
        PHR: setPhr({}),
      },
    }

    const fhirResource = Export.fhir(pedigree, probandId, {})
    const validationResult = fhir.validate(fhirResource)

    expect(fhirResource.contained[0].reasonCode)
    .to.equal('Invited to complete family health history by your doctor')
    expect(validationResult.errors.length).to.equal(0)

    done()
  })

  it('should use correct concept codes for unknown properties', function (done) {
    const probandId = faker.random.number()
    const fakeResponses = {}

    const pedigree = {
      [probandId]: {
        patientId: probandId,
        PHR: setPhr(fakeResponses),
      },
    }

    const fhirResource = Export.fhir(pedigree, probandId, {})
    const validationResult = fhir.validate(fhirResource)

    expect(fhirResource.contained[0].patient).to.equal('NI')
    expect(fhirResource.contained[0].note).to.equal('NASK')
    expect(fhirResource.contained[0].name).to.equal('ASKU')
    expect(fhirResource.contained[0].condition).to.equal('UNK')
    expect(validationResult.errors.length).to.equal(0)

    done()
  })

  it('should return correct relation code for family members', function (done) {
    const probandId = faker.random.number({ min: 100, max: 200 })
    const relativeId = faker.random.number({ min: 300, max: 400 })
    const fakeResponses = [{
      gender: {
        responseType: 'value',
        value: 'female',
      },
    }, {
      gender: {
        responseType: 'value',
        value: 'male',
      },
    }]

    const pedigree = {
      [probandId]: {
        patientId: probandId,
        PHR: setPhr(fakeResponses[0]),
        relations: {
          'parent.child.child': [relativeId],
        },
      },
      [relativeId]: {
        patientId: relativeId,
        PHR: setPhr(fakeResponses[1]),
      },
    }

    const fhirResource = Export.fhir(pedigree, probandId, {})
    const validationResult = fhir.validate(fhirResource)
    const probandCode = fhirResource.contained[0].relationship.coding[0].code
    const relationCode = fhirResource.contained[1].relationship.coding[0].code

    expect(probandCode).to.equal('ONESELF')
    expect(relationCode).to.equal('NEPHEW')
    expect(validationResult.errors.length).to.equal(0)

    done()
  })

  it('should return correct progress status for each member', function (done) {
    const probandId = faker.random.number({ min: 100, max: 200 })
    const relativeOneId = faker.random.number({ min: 300, max: 400 })
    const relativeTwoId = faker.random.number({ min: 500, max: 600 })
    const fakeResponses = [{
    }, {
      'first-name': {
        responseType: 'value',
        value: faker.random.word(),
      },
    }, {
      'first-name': {
        responseType: 'value',
        value: faker.random.word(),
      },
      'birth-year': {
        responseType: 'value',
        value: faker.random.number({ min: 1950, max: 2000 }),
      },
    }]

    const pedigree = {
      [probandId]: {
        patientId: probandId,
        PHR: setPhr(fakeResponses[0]),
      },
      [relativeOneId]: {
        patientId: relativeOneId,
        PHR: setPhr(fakeResponses[1]),
      },
      [relativeTwoId]: {
        patientId: relativeTwoId,
        PHR: setPhr(fakeResponses[2]),
      },
    }

    const fhirResource = Export.fhir(pedigree, probandId, {})
    const validationResult = fhir.validate(fhirResource)

    expect(fhirResource.contained[0].status).to.equal('health-unknown')
    expect(fhirResource.contained[1].status).to.equal('partial')
    expect(fhirResource.contained[2].status).to.equal('complete')
    expect(validationResult.errors.length).to.equal(0)

    done()
  })
})
