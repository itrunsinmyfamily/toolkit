const appRoot = require('app-root-path')
const faker = require('faker')
const chai = require('chai')

const expect = chai.expect

const recommendation = appRoot.require('./recommendation').default

const records = appRoot.require('./records').default

function createTestData(currentPatientId, memberOne, memberTwo) {
  const bundle = new records.RecordBundle()
  const bundleOne = new records.RecordBundle()
  const bundleTwo = new records.RecordBundle()

  bundle.addRecord('C6', {}, {})

  bundleOne.addRecord(memberOne.code, {}, {})

  bundleTwo.addRecord(memberTwo.code, {}, {})

  const pedigree = {}

  pedigree[currentPatientId] = {
    patientId: currentPatientId,
    PHR: bundle,
    relations: {
      parent: [memberOne.id, memberTwo.id],
    },
    relation: 'proband',
  }
  pedigree[memberOne.id] = {
    patientId: memberOne.id,
    PHR: bundleOne,
    relations: {
      spousal: [memberTwo.id],
      child: [currentPatientId],
    },
    relationship: 'Mother',
  }
  pedigree[memberTwo.id] = {
    patientId: memberTwo.id,
    PHR: bundleTwo,
    relations: {
      spousal: [memberOne.id],
      child: [currentPatientId],
    },
    relationship: 'Father',
  }

  return pedigree
}

describe('Amsterdam Criteria', function () {
  describe('Number of Cancer patients in relatives:', function () {
    it('number of cancer patients should be greater than two ', function (done) {
      const currentPatientId = faker.random.number()

      const memberOne = {
        id: faker.random.number(),
        code: 'C30',
      }
      const memberTwo = {
        id: faker.random.number(),
        code: 'C29',
      }

      const testPedigree = createTestData(currentPatientId, memberOne, memberTwo)

      const result = recommendation.amsterdamCriteria(testPedigree, currentPatientId)

      expect(result.amsterdamCriteria).to.equal(false)

      done()
    })

    it('should require three relatives with specified cancers ', function (done) {
      const currentPatientId = faker.random.number()
      const memberIdThree = faker.random.number()
      const bundle = new records.RecordBundle()

      const memberOne = {
        id: faker.random.number(),
        code: 'C30',
      }
      const memberTwo = {
        id: faker.random.number(),
        code: 'C29',
      }

      // add a cancer not specified in the criteria list
      bundle.addRecord('C90', {}, {
        'age-start': {
          responseType: records.RESPONSE_VALUE,
          value: faker.random.number({ min: 0, max: 49 }),
        },
      })

      const testPedigree = createTestData(currentPatientId, memberOne, memberTwo)

      testPedigree[currentPatientId].relations['parent.parent'] = [memberIdThree]
      testPedigree[memberOne.id].relations.parent = [memberIdThree]
      testPedigree.memberIdThree = {
        patientId: memberIdThree,
        PHR: bundle,
        relations: {
          child: [memberOne.id],
          'child.child': [currentPatientId],
        },
        relationship: 'Maternal Grandmother',
      }

      const result = recommendation.amsterdamCriteria(testPedigree, currentPatientId)

      expect(result.amsterdamCriteria).to.equal(false)

      done()
    })
  })

  describe('Age-start Check:', function () {
    it('should return false if age-start is not set', function (done) {
      const currentPatientId = faker.random.number()
      const memberIdThree = faker.random.number()
      const bundle = new records.RecordBundle()

      const memberOne = {
        id: faker.random.number(),
        code: 'C30',
      }
      const memberTwo = {
        id: faker.random.number(),
        code: 'C29',
      }

      bundle.addRecord('C29', {}, {})

      const testPedigree = createTestData(currentPatientId, memberOne, memberTwo)

      testPedigree[currentPatientId].relations['parent.parent'] = [memberIdThree]
      testPedigree[memberOne.id].relations.parent = [memberIdThree]
      testPedigree.memberIdThree = {
        patientId: memberIdThree,
        PHR: bundle,
        relations: {
          child: [memberOne.id],
          'child.child': [currentPatientId],
        },
        relationship: 'Maternal Grandmother',
      }

      const result = recommendation.amsterdamCriteria(testPedigree, currentPatientId)

      expect(result.amsterdamCriteria).to.equal(false)

      done()
    })

    it('should return false if age-start is greater than 50', function (done) {
      const currentPatientId = faker.random.number()
      const memberIdThree = faker.random.number()
      const bundle = new records.RecordBundle()

      const memberOne = {
        id: faker.random.number(),
        code: 'C30',
      }
      const memberTwo = {
        id: faker.random.number(),
        code: 'C29',
      }

      bundle.addRecord('C29', {}, {
        'age-start': {
          responseType: records.RESPONSE_VALUE,
          value: faker.random.number({ min: 50, max: 100 }),
        },
      })

      const testPedigree = createTestData(currentPatientId, memberOne, memberTwo)

      testPedigree[currentPatientId].relations['parent.parent'] = [memberIdThree]
      testPedigree[memberOne.id].relations.parent = [memberIdThree]
      testPedigree.memberIdThree = {
        patientId: memberIdThree,
        PHR: bundle,
        relations: {
          child: [memberOne.id],
          'child.child': [currentPatientId],
        },
        relationship: 'Maternal Grandmother',
      }

      const result = recommendation.amsterdamCriteria(testPedigree, currentPatientId)

      expect(result.amsterdamCriteria).to.equal(false)

      done()
    })
  })

  describe('Successive generations with cancer', function () {
    it('should return false incase of no successiveGenerations', function (done) {
      const currentPatientId = faker.random.number()
      const memberIdThree = faker.random.number()
      const bundle = new records.RecordBundle()

      const memberOne = {
        id: faker.random.number(),
        code: 'C30',
      }
      const memberTwo = {
        id: faker.random.number(),
        code: 'C29',
      }

      bundle.addRecord('C29', {}, {
        'age-start': {
          responseType: records.RESPONSE_VALUE,
          value: faker.random.number({ min: 0, max: 49 }),
        },
      })

      const testPedigree = createTestData(currentPatientId, memberOne, memberTwo)

      testPedigree[currentPatientId].relations.child = [memberIdThree]
      testPedigree[memberOne.id].relations['child.child'] = [memberIdThree]
      testPedigree[memberTwo.id].relations['child.child'] = [memberIdThree]
      testPedigree.memberIdThree = {
        patientId: memberIdThree,
        PHR: bundle,
        relations: {
          parent: [currentPatientId],
          'parent.parent': [memberOne.id, memberTwo.id],
        },
        relationship: 'Daughter',
      }

      const result = recommendation.amsterdamCriteria(testPedigree, currentPatientId)

      expect(result.amsterdamCriteria).to.equal(false)

      done()
    })

    it('successive generations should relate to a single parent', function (done) {
      const currentPatientId = faker.random.number()
      const memberIdThree = faker.random.number()
      const memberIdFour = faker.random.number()
      const bundle = new records.RecordBundle()
      const bundleTwo = new records.RecordBundle()

      const memberOne = {
        id: faker.random.number(),
        code: 'C30',
      }
      const memberTwo = {
        id: faker.random.number(),
        code: 'C5',
      }

      bundle.addRecord('C29', {}, {
        'age-start': {
          responseType: records.RESPONSE_VALUE,
          value: faker.random.number({ min: 0, max: 49 }),
        },
      })

      bundleTwo.addRecord('C30', {}, {})

      const testPedigree = createTestData(currentPatientId, memberOne, memberTwo)

      testPedigree[currentPatientId].relations.child = [memberIdThree]
      testPedigree[memberOne.id].relations['child.child'] = [memberIdThree]
      testPedigree[memberTwo.id].relations['child.child'] = [memberIdThree]

      testPedigree[currentPatientId].relations['parent.parent'] = [memberIdFour]
      testPedigree[memberTwo.id].relations.parent = [memberIdFour]

      testPedigree.memberIdThree = {
        patientId: memberIdThree,
        PHR: bundle,
        relations: {
          parent: [currentPatientId],
          'parent.parent': [memberOne.id, memberTwo.id],
          'parent.parent.parent': [memberIdFour],
        },
        relationship: 'Daughter',
      }

      testPedigree.memberIdFour = {
        patientId: memberIdFour,
        PHR: bundleTwo,
        relations: {
          child: [memberTwo.id],
          'child.child': [currentPatientId],
          'child.child.child': [memberIdThree],
        },
        relationship: 'Paternal Grandmother',
      }

      const result = recommendation.amsterdamCriteria(testPedigree, currentPatientId)

      expect(result.amsterdamCriteria).to.equal(false)

      done()
    })
  })

  describe('First Degree Check:', function () {
    it('cancer relatives should belong to same parent', function (done) {
      const currentPatientId = faker.random.number()
      const memberIdThree = faker.random.number()
      const memberIdFour = faker.random.number()
      const bundle = new records.RecordBundle()
      const bundleTwo = new records.RecordBundle()


      const memberOne = {
        id: faker.random.number(),
        code: 'C30',
      }
      const memberTwo = {
        id: faker.random.number(),
        code: 'C29',
      }

      bundle.addRecord('C29', {}, {
        'age-start': {
          responseType: records.RESPONSE_VALUE,
          value: faker.random.number({ min: 0, max: 49 }),
        },
      })

      bundleTwo.addRecord('C30', {}, {})

      const testPedigree = createTestData(currentPatientId, memberOne, memberTwo)

      testPedigree[currentPatientId].relations['parent.parent'] = [memberIdThree]
      testPedigree[memberOne.id].relations.parent = [memberIdThree]
      testPedigree[currentPatientId].relations['parent.parent'] = [memberIdFour]
      testPedigree[memberTwo.id].relations.parent = [memberIdFour]

      testPedigree.memberIdThree = {
        patientId: memberIdThree,
        PHR: bundle,
        relations: {
          child: [memberOne.id],
          'child.child': [currentPatientId],
        },
        relationship: 'Maternal Grandmother',
      }

      testPedigree.memberIdFour = {
        patientId: memberIdFour,
        PHR: bundleTwo,
        relations: {
          child: [memberTwo.id],
          'child.child': [currentPatientId],
          'child.child.child': [memberIdThree],
        },
        relationship: 'Paternal Grandmother',
      }

      const result = recommendation.amsterdamCriteria(testPedigree, currentPatientId)

      expect(result.amsterdamCriteria).to.equal(false)

      done()
    })

    it('first degree check should omit spousal relation', function (done) {
      const currentPatientId = faker.random.number()
      const memberIdThree = faker.random.number()
      const bundle = new records.RecordBundle()

      const memberOne = {
        id: faker.random.number(),
        code: 'C30',
      }
      const memberTwo = {
        id: faker.random.number(),
        code: 'C29',
      }

      bundle.addRecord('C29', {}, {
        'age-start': {
          responseType: records.RESPONSE_VALUE,
          value: faker.random.number({ min: 0, max: 49 }),
        },
      })

      const testPedigree = createTestData(currentPatientId, memberOne, memberTwo)

      testPedigree[currentPatientId].relations['parent.parent'] = [memberIdThree]
      testPedigree[memberOne.id].relations.parent = [memberIdThree]

      testPedigree.memberIdThree = {
        patientId: memberIdThree,
        PHR: bundle,
        relations: {
          'child.child': [currentPatientId],
          child: [memberOne.id],
        },
        relationship: 'Maternel Grandmother',
      }

      const result = recommendation.amsterdamCriteria(testPedigree, currentPatientId)

      expect(result.amsterdamCriteria).to.equal(false)

      done()
    })
  })

  describe('Amsterdam criteria success test ', function () {
    it('should return true if every criteria is met', function (done) {
      const currentPatientId = faker.random.number()
      const memberIdThree = faker.random.number()
      const memberIdFour = faker.random.number()
      const bundle = new records.RecordBundle()
      const bundleOne = new records.RecordBundle()

      const memberOne = {
        id: faker.random.number(),
        code: 'C30',
      }
      const memberTwo = {
        id: faker.random.number(),
        code: 'C29',
      }

      bundle.addRecord('C29', {}, {
        'age-start': {
          responseType: records.RESPONSE_VALUE,
          value: faker.random.number({ min: 0, max: 49 }),
        },
      })

      bundleOne.addRecord('C30', {}, {})


      const testPedigree = createTestData(currentPatientId, memberOne, memberTwo)

      testPedigree[currentPatientId].relations['parent.parent'] = [memberIdThree]
      testPedigree[memberOne.id].relations.parent = [memberIdThree]
      testPedigree[currentPatientId].relations['parent.parent.parent'] = [memberIdFour]
      testPedigree[memberOne.id].relations['parent.parent'] = [memberIdFour]

      testPedigree.memberIdThree = {
        patientId: memberIdThree,
        PHR: bundle,
        relations: {
          child: [memberOne.id],
          parent: [memberIdFour],
          'child.child': [currentPatientId],
        },
        relationship: 'Maternal Grandmother',
      }
      testPedigree.memberIdFour = {
        patientId: memberIdFour,
        PHR: bundleOne,
        relations: {
          child: [memberIdThree],
          'child.child': [memberOne.id],
          'child.child.child': [currentPatientId],
        },
        relationship: 'Great Grandmother',
      }

      const result = recommendation.amsterdamCriteria(testPedigree, currentPatientId)

      expect(result.amsterdamCriteria).to.equal(true)

      done()
    })
  })
})
