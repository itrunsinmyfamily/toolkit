/* eslint prefer-template: "off" */
const faker = require('faker')
const appRoot = require('app-root-path')
const chai = require('chai')

const expect = chai.expect
const conditions = appRoot.require('/conditions').default

chai.use(require('dirty-chai'))

describe('Conditions', function () {
  it('should support condition sets', function (done) {
    const subject = faker.random.number()

    expect(conditions.evaluateSet(subject, { eq: subject, neq: (subject + 1) })).to.be.true()
    expect(conditions.evaluateSet(subject, { eq: subject, inq: [subject] })).to.be.true()
    expect(conditions.evaluateSet(subject, { btw: [-subject, subject] })).to.be.true()

    expect(conditions.evaluateSet(subject, { eq: (subject + 5) })).to.be.false()

    // Conflicting conditions are acceptable
    expect(conditions.evaluateSet(subject, { eq: subject, neq: subject })).to.be.false()

    done()
  })

  it('should not support invalid operators', function (done) {
    const str = faker.random.word()

    expect(() => conditions.evaluate(str, 'XXX', str)).to.throw(/Invalid operator/i)
    expect(() => conditions.evaluate(str, 'EQ', str)).to.throw(/Invalid operator/i)

    expect(() => conditions.evaluateSet(str, { unknown: str })).to.throw(/Invalid operator/i)
    done()
  })

  it('should support condition validation', function (done) {
    const str = faker.random.word()

    expect(conditions.validate('eq', str)).to.be.undefined()
    expect(conditions.validate('neq', str)).to.be.undefined()
    expect(conditions.validate('eq', null)).to.be.an('error')
    expect(conditions.validate('XXX', str)).to.be.an('error')

    expect(conditions.validateSet({ eq: str })).to.be.an('array').with.lengthOf(0)
    expect(conditions.validateSet({ neq: str })).to.be.an('array').with.lengthOf(0)
    expect(conditions.validateSet({ unknown: str }))
      .to.be.an('array').with.deep.property('[0]').that.is.an('error')

    done()
  })

  it('should reject undefined subjects', function (done) {
    expect(conditions.evaluate(undefined, 'eq', 0)).to.be.false()
    done()
  })

  it('should report failing operators in condition sets', function (done) {
    const subject = faker.random.number()
    const failures = []

    conditions.evaluateSet(subject, { eq: subject }, failures)
    expect(failures).to.be.empty()

    failures.length = 0
    conditions.evaluateSet(subject, { eq: (subject + 1) }, failures)
    expect(failures).to.deep.equal(['eq'])

    failures.length = 0
    conditions.evaluateSet(subject, { eq: (subject + 1), neq: subject }, failures)
    expect(failures).to.deep.equal(['eq', 'neq'])

    done()
  })

  describe('equality operators (eq, neq)', function () {
    function expectations(subject, other) {
      expect(conditions.evaluate(subject, 'eq', subject)).to.be.true()
      expect(conditions.evaluate(subject, 'eq', other)).to.be.false()

      expect(conditions.evaluate(subject, 'neq', subject)).to.be.false()
      expect(conditions.evaluate(subject, 'neq', other)).to.be.true()
    }

    it('should support string comparisons', function (done) {
      const subject = faker.random.word()
      const other = subject + 'XXX'

      expectations(subject, other)
      done()
    })

    it('should support numeric comparisons', function (done) {
      const subject = faker.random.number()
      const other = subject + 5

      expectations(subject, other)
      done()
    })

    it('should support datetime equality', function (done) {
      const subject = faker.date.past()
      const other = faker.date.future()

      expectations(subject, other)
      done()
    })

    it('should not support mismatched types', function (done) {
      const num = faker.random.number()
      const str = '' + num
      const arr = [num]

      expect(() => conditions.evaluate(num, 'eq', str)).to.throw(/Typecheck/i)
      expect(() => conditions.evaluate(num, 'neq', str)).to.throw(/Typecheck/i)

      expect(() => conditions.evaluate(num, 'eq', arr)).to.throw(/Validation/i)
      expect(() => conditions.evaluate(num, 'neq', arr)).to.throw(/Validation/i)

      done()
    })
  })

  describe('relational operators (gt, lt, gte, lte)', function () {
    function expectations(upper, lower) {
      expect(conditions.evaluate(upper, 'gt', lower)).to.be.true()
      expect(conditions.evaluate(upper, 'gt', upper)).to.be.false()
      expect(conditions.evaluate(lower, 'gt', upper)).to.be.false()

      expect(conditions.evaluate(upper, 'lt', lower)).to.be.false()
      expect(conditions.evaluate(upper, 'lt', upper)).to.be.false()
      expect(conditions.evaluate(lower, 'lt', upper)).to.be.true()

      expect(conditions.evaluate(upper, 'gte', lower)).to.be.true()
      expect(conditions.evaluate(upper, 'gte', upper)).to.be.true()
      expect(conditions.evaluate(lower, 'gte', lower)).to.be.true()
      expect(conditions.evaluate(lower, 'gte', upper)).to.be.false()

      expect(conditions.evaluate(upper, 'lte', lower)).to.be.false()
      expect(conditions.evaluate(upper, 'lte', upper)).to.be.true()
      expect(conditions.evaluate(lower, 'lte', lower)).to.be.true()
      expect(conditions.evaluate(lower, 'lte', upper)).to.be.true()
    }

    it('should support string comparison', function (done) {
      const str = faker.random.word()
      const lower = str + ':AAA'
      const upper = str + ':ZZZ'

      expectations(upper, lower)
      done()
    })

    it('should support numeric comparison', function (done) {
      const lower = faker.random.number()
      const upper = lower + 1

      expectations(upper, lower)
      done()
    })

    it('should support datetime comparison', function (done) {
      const lower = faker.date.past()
      const upper = faker.date.future()

      expectations(upper, lower)
      done()
    })

    it('should not support mismatched types', function (done) {
      const num = faker.random.number()
      const str = '' + num

      expect(() => conditions.evaluate(num, 'gt', '' + str)).to.throw(/Typecheck/i)
      expect(() => conditions.evaluate(num, 'lt', '' + str)).to.throw(/Typecheck/i)
      expect(() => conditions.evaluate(num, 'gte', '' + str)).to.throw(/Typecheck/i)
      expect(() => conditions.evaluate(num, 'lte', '' + str)).to.throw(/Typecheck/i)

      expect(() => conditions.evaluate(num, 'gt', [num])).to.throw(/Validation/i)
      expect(() => conditions.evaluate(num, 'lt', [num])).to.throw(/Validation/i)
      expect(() => conditions.evaluate(num, 'gte', [num])).to.throw(/Validation/i)
      expect(() => conditions.evaluate(num, 'lte', [num])).to.throw(/Validation/i)
      done()
    })
  })

  describe('range operator (btw)', function () {
    function expectations(upper, middle, lower) {
      expect(conditions.evaluate(middle, 'btw', [lower, upper])).to.be.true()
      expect(conditions.evaluate(lower, 'btw', [middle, upper])).to.be.false()
      expect(conditions.evaluate(upper, 'btw', [lower, middle])).to.be.false()

      // Inclusivity
      expect(conditions.evaluate(lower, 'btw', [lower, upper])).to.be.true()
      expect(conditions.evaluate(lower, 'btw', [lower, upper])).to.be.true()
      expect(conditions.evaluate(upper, 'btw', [lower, upper])).to.be.true()
      expect(conditions.evaluate(upper, 'btw', [upper, upper])).to.be.true()
    }

    it('should support string comparison', function (done) {
      const str = faker.random.word()
      const lower = str + ':AAA'
      const middle = str + ':MMM'
      const upper = str + ':ZZZ'

      expectations(upper, middle, lower)
      done()
    })

    it('should support numeric comparison', function (done) {
      const lower = faker.random.number()
      const middle = lower + 5
      const upper = middle + 5

      expectations(upper, middle, lower)
      done()
    })

    it('should support datetime comparison', function (done) {
      const lower = faker.date.past()
      const upper = faker.date.future()
      const middle = faker.date.between(lower, upper)

      expectations(upper, middle, lower)
      done()
    })

    it('should not support incorrect types', function (done) {
      const num = faker.random.number()
      const str = '' + num

      expect(() => conditions.evaluate(num, 'btw', num)).to.throw(/Validation/i)
      expect(() => conditions.evaluate(num, 'btw', [num])).to.throw(/Validation/i)
      expect(() => conditions.evaluate(num, 'btw', [str, str])).to.throw(/Typecheck/i)
      expect(() => conditions.evaluate(num, 'btw', [num, num, num])).to.throw(/Validation/i)

      done()
    })
  })

  describe('inclusion operators (inq, nin)', function () {
    function expectations(valueset) {
      const notinset = valueset.pop()

      expect(conditions.evaluate(valueset[0], 'inq', valueset)).to.be.true()
      expect(conditions.evaluate(valueset[0], 'nin', valueset)).to.be.false()

      expect(conditions.evaluate(notinset, 'inq', valueset)).to.be.false()
      expect(conditions.evaluate(notinset, 'nin', valueset)).to.be.true()

      expect(conditions.evaluate(notinset, 'inq', [])).to.be.false()
      expect(conditions.evaluate(notinset, 'nin', [])).to.be.true()
    }

    it('should support string sets', function (done) {
      const valueset = [...new Set(Array(8).fill().map(faker.random.word))]

      expectations(valueset)
      done()
    })

    it('should support numeric sets', function (done) {
      const valueset = [...new Set(Array(8).fill().map(faker.random.number))]

      expectations(valueset)
      done()
    })

    it('should support datetime sets', function (done) {
      const valueset = [...new Set(Array(8).fill().map(faker.date.past))]

      expectations(valueset)
      done()
    })


    it('should not support incorrect types', function (done) {
      const num = faker.random.number()
      const str = '' + num

      expect(() => conditions.evaluate(num, 'inq', num)).to.throw(/Validation/i)
      expect(() => conditions.evaluate(num, 'inq', [str])).to.throw(/Typecheck/i)
      expect(() => conditions.evaluate([num], 'inq', [num])).to.throw(/Typecheck/i)

      expect(() => conditions.evaluate(num, 'nin', num)).to.throw(/Validation/i)
      expect(() => conditions.evaluate(num, 'nin', [str])).to.throw(/Typecheck/i)
      expect(() => conditions.evaluate([num], 'nin', [num])).to.throw(/Typecheck/i)

      done()
    })
  })

  describe('regexp operator (re)', function () {
    it('should support string subjects', function (done) {
      const subject = 'FRONT ' + faker.random.word() + ' MATCHME ' + faker.random.word() + ' END'

      expect(conditions.evaluate(subject, 're', '^FRONT')).to.be.true()
      expect(conditions.evaluate(subject, 're', 'MATCH[A-Z]{2}')).to.be.true()
      expect(conditions.evaluate(subject, 're', 'MATCHYOU')).to.be.false()
      expect(conditions.evaluate(subject, 're', '^NOTFRONT')).to.be.false()
      expect(conditions.evaluate(subject, 're', ['^front', 'i'])).to.be.true()

      done()
    })

    it('should not support incorrect types', function (done) {
      const num = faker.random.number()
      const str = '' + num

      expect(() => conditions.evaluate(num, 're', str)).to.throw(/Typecheck/i)
      expect(() => conditions.evaluate(num, 're', [str, str])).to.throw(/Typecheck/i)
      expect(() => conditions.evaluate(str, 're', [str])).to.throw(/Validation/i)
      expect(() => conditions.evaluate(str, 're', [str, num])).to.throw(/Validation/i)
      expect(() => conditions.evaluate(str, 're', [str, str, str])).to.throw(/Validation/i)

      done()
    })
  })
})
