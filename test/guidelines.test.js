const appRoot = require('app-root-path')
const chai = require('chai')
const fs = require('fs')

const expect = chai.expect

const guidelines = appRoot.require('./guidelines').default
const concepts = require('./fixtures/concepts.json')

const rules = fs.readdirSync(`${appRoot}/test/fixtures/guidelines`)

describe('Guidelines', function () {
  it('should recommend patient correctly', function (done) {
    rules.forEach(rule => {
      // eslint-disable-next-line global-require
      const file = require(`./fixtures/guidelines/${rule}`)
      const recommendations = guidelines(file.pedigree, file.proband, concepts)
      const ruleExists = !!recommendations.find(r =>
        (r.id === file.guideline.id && r.recommendation === file.guideline.recommendation))

      expect(ruleExists, `${rule} failed`).to.equal(true)
    })
    done()
  })
})
