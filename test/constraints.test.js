/* eslint prefer-template: "off", mocha/no-synchronous-tests: "off" */
const appRoot = require('app-root-path')
const chai = require('chai')

const expect = chai.expect
const constraints = appRoot.require('/constraints').default
const records = appRoot.require('/records').default
const RecordBundle = records.RecordBundle

chai.use(require('dirty-chai'))

describe('Constraints', function () {
  before(function setupBundle() {
    const bundle = this.bundle = new RecordBundle()

    bundle.addRecord('Life', {}, {
      age: { responseType: records.RESPONSE_VALUE, value: 42 },
      name: { responseType: records.RESPONSE_VALUE, value: 'Yohan' },
      birth: { responseType: records.RESPONSE_VALUE, value: new Date('2010-01-01') },
    })
    bundle.addRecord('ChronicCaffeination', {}, {
      'age-start': { responseType: records.RESPONSE_VALUE, value: 21 },
      'age-end': { responseType: records.RESPONSE_VALUE, value: 25 },
      'daily-dose': { responseType: records.RESPONSE_VALUE, value: 300 },
    })
    bundle.addRecord('ChronicCaffeination', {}, {
      'age-start': { responseType: records.RESPONSE_VALUE, value: 37 },
      'daily-dose': { responseType: records.RESPONSE_VALUE, value: 500 },
    })
  })

  it('should handle as PASS empty constraint sets', function () {
    expect(constraints.evaluate(new RecordBundle(), [])).to.be.true()
  })

  it('should handle as FAIL empty bundles with non-empty constraints', function () {
    const simple = [{ conceptId: 'Life', attributeId: 'age', value: { gt: 0 } }]

    expect(constraints.evaluate(new RecordBundle(), simple)).to.be.false()
  })

  it('should support single concept simple constraints', function () {
    const simplePass = [{ conceptId: 'Life', attributeId: 'age', value: { gt: 0 } }]
    const simpleFail = [{ conceptId: 'Life', attributeId: 'age', value: { lt: 40 } }]

    expect(constraints.evaluate(this.bundle, simplePass)).to.be.true()
    expect(constraints.evaluate(this.bundle, simpleFail)).to.be.false()
  })

  it('should support multi-concept simple constraints', function () {
    const multiPass = [{
      conceptId: 'ChronicCaffeination', attributeId: 'age-start', value: { gt: 0 },
    }]
    const multiFail = [{
      conceptId: 'ChronicCaffeination', attributeId: 'age-start', value: { lt: 10 },
    }]

    expect(constraints.evaluate(this.bundle, multiPass)).to.be.true()
    expect(constraints.evaluate(this.bundle, multiFail)).to.be.false()
  })

  it('should support single concept complex constraints', function () {
    const singleComplexPass = [
      // Both of these pass
      { conceptId: 'Life', attributeId: 'age', value: { gt: 0 } },
      { conceptId: 'Life', attributeId: 'name', value: { eq: 'Yohan' } },
    ]
    const singleComplexFail = [
      // Both of these fail
      { conceptId: 'Life', attributeId: 'age', value: { lt: 40 } },
      { conceptId: 'Life', attributeId: 'name', value: { eq: 'Charles' } },
    ]
    const singleComplexMixed = [
      // First Passes, Second Fails => Overall Fail
      { conceptId: 'Life', attributeId: 'age', value: { gt: 0 } },
      { conceptId: 'Life', attributeId: 'name', value: { eq: 'Charles' } },
    ]

    expect(constraints.evaluate(this.bundle, singleComplexPass)).to.be.true()
    expect(constraints.evaluate(this.bundle, singleComplexFail)).to.be.false()
    expect(constraints.evaluate(this.bundle, singleComplexMixed)).to.be.false()
  })

  it('should support multi-concept complex constraints', function () {
    const multiComplexPass = [
      // Both of these pass
      { conceptId: 'ChronicCaffeination', attributeId: 'age-start', value: { gt: 0 } },
      { conceptId: 'ChronicCaffeination', attributeId: 'age-end', value: { lt: 50 } },
    ]
    const multiComplexFail = [
      // Both of these fail
      { conceptId: 'ChronicCaffeination', attributeId: 'age-start', value: { gt: 100 } },
      { conceptId: 'ChronicCaffeination', attributeId: 'age-end', value: { lt: 5 } },
    ]
    const multiComplexMixed = [
      // First Passes, Second Fails => Overall Fail
      { conceptId: 'ChronicCaffeination', attributeId: 'age-start', value: { gt: 0 } },
      { conceptId: 'ChronicCaffeination', attributeId: 'age-end', value: { lt: 5 } },
    ]

    expect(constraints.evaluate(this.bundle, multiComplexPass)).to.be.true()
    expect(constraints.evaluate(this.bundle, multiComplexFail)).to.be.false()
    expect(constraints.evaluate(this.bundle, multiComplexMixed)).to.be.false()
  })

  it('should not allow constraint evaluation to span record instances', function () {
    const multiComplex = [
      // Both of these pass when separate, but they come from different instances of the
      // same Condition, and so the overall constraint must fail.
      { conceptId: 'ChronicCaffeination', attributeId: 'age-start', value: { lt: 30 } },
      { conceptId: 'ChronicCaffeination', attributeId: 'daily-dose', value: { gt: 400 } },
    ]

    expect(constraints.evaluate(this.bundle, [multiComplex[0]])).to.be.true()
    expect(constraints.evaluate(this.bundle, [multiComplex[1]])).to.be.true()
    expect(constraints.evaluate(this.bundle, multiComplex)).to.be.false()
  })

  it('should report failure causes', function () {
    const failures = []
    const simpleFail = [{ conceptId: 'Life', attributeId: 'age', value: { lt: 40 } }]

    constraints.evaluate(new RecordBundle(), [], failures)
    expect(failures).to.deep.equal([])

    failures.length = 0
    constraints.evaluate(this.bundle, simpleFail, failures)
    expect(failures)
      .to.deep.equal([{
        conceptId: 'Life',
        attributeId: 'age',
        conditions: ['lt'],
        expected: { lt: 40 },
      }])
  })
})
