/* eslint prefer-template: "off", mocha/no-synchronous-tests: "off" */
const faker = require('faker')
const appRoot = require('app-root-path')
const chai = require('chai')

const expect = chai.expect
const records = appRoot.require('/records').default
const RecordBundle = records.RecordBundle
const RecordBundleRecord = records.RecordBundleRecord

chai.use(require('dirty-chai'))

describe('Records', function () {
  it('should allow building up a bundle by piece', function () {
    const codeA = faker.random.word()
    const codeB = faker.random.word()
    const fakeValue = faker.random.word()

    const bundle = new RecordBundle()
    const record = bundle.addRecord(codeA)

    expect(record).to.be.an.instanceof(RecordBundleRecord)
    expect(bundle.getRecords(codeA)).to.be.an('array').with.lengthOf(1).and.to.contain(record)

    bundle.addRecord(codeA)
    expect(bundle.getRecords(codeA)).to.be.an('array').with.lengthOf(2)

    record.setValue(codeB, fakeValue)

    expect(record.getValue(codeB)).to.equal(fakeValue)
  })

  it('should allow building up with values provided', function () {
    const codeA = faker.random.word()
    const codeB = faker.random.word()
    const fakeValue = faker.random.word()

    const bundle = new RecordBundle()
    const record = bundle.addRecord(codeA, {}, {
      [codeB]: { responseType: records.RESPONSE_VALUE, value: fakeValue },
    })

    expect(record).to.be.an.instanceof(RecordBundleRecord)
    expect(bundle.getRecords(codeA)).to.be.an('array').with.lengthOf(1).and.to.contain(record)

    expect(record.getValue(codeB)).to.equal(fakeValue)
  })

  it('should allow extraneous record values', function () {
    const codeA = faker.random.word()

    const bundle = new RecordBundle()
    const record = bundle.addRecord(codeA, { code: codeA })

    expect(record).to.be.an.instanceof(RecordBundleRecord)
    expect(record).to.have.property('code', codeA)
  })

  it('should allow cloning of bundles and bundle records', function () {
    const codeA = faker.random.word()
    const codeB = faker.random.word()
    const fakeValue = faker.random.word()

    const bundle = new RecordBundle()

    bundle.addRecord(codeA, { code: codeA }, {
      [codeB]: { responseType: records.RESPONSE_VALUE, value: fakeValue },
    })

    const bundleClone = new RecordBundle(bundle)
    const bundleRecords = bundleClone.getRecords(codeA)

    expect(bundleRecords[0]).to.be.an.instanceof(RecordBundleRecord)
    expect(bundleRecords[0]).to.have.property('code', codeA)
    expect(bundleRecords[0].getValue(codeB)).to.equal(fakeValue)
  })

  it('should allow bulk value setting', function () {
    const codeA = faker.random.word()
    const fakeValues = {
      one: faker.random.word(),
      two: faker.random.word(),
      three: faker.random.word(),
    }

    const bundle = new RecordBundle()
    const record = bundle.addRecord(codeA)

    record.setValues(fakeValues)

    expect(record.getValue('one')).to.equal(fakeValues.one)
    expect(record.getValues()).to.deep.equal(fakeValues)
  })

  it('should allow recreating a serialized bundle', function () {
    const codeA = faker.random.word()
    const fakeResponses = {
      one: { responseType: records.RESPONSE_VALUE, value: faker.random.word() },
      two: { responseType: records.RESPONSE_VALUE, value: faker.random.word() },
      three: { responseType: records.RESPONSE_VALUE, value: faker.random.word() },
    }

    const bundle = new RecordBundle()
    const record = bundle.addRecord(codeA)

    record.setValues(fakeResponses)

    const jsonBundle = JSON.stringify(bundle)
    const newBundle = new RecordBundle(JSON.parse(jsonBundle))

    expect(newBundle.getRecords(codeA)[0]).to.be.an.instanceof(RecordBundleRecord)
  })

  it('should return an array even when no records were found', function () {
    const bundle = new RecordBundle()

    expect(bundle.getRecords(faker.random.word())).to.be.an('array').with.lengthOf(0)
  })
  describe('Change Tracking', function () {
    it('it should not flag new values as changed if defined explicitly', function () {
      const codeA = faker.random.word()
      const fakeResponses = {
        one: { responseType: records.RESPONSE_VALUE, value: faker.random.word() },
        two: { responseType: records.RESPONSE_VALUE, value: faker.random.word() },
      }

      const bundle = new RecordBundle()
      const record = bundle.addRecord(codeA)

      record.setResponses(fakeResponses, false)

      expect(record.getChangedResponses()).to.be.eql({})
    })

    it('it should flag new values as changed if defined by default', function () {
      const codeA = faker.random.word()
      const fakeResponses = {
        one: { responseType: records.RESPONSE_VALUE, value: faker.random.word() },
        two: { responseType: records.RESPONSE_VALUE, value: faker.random.word() },
      }

      const bundle = new RecordBundle()
      const record = bundle.addRecord(codeA)

      record.setResponses(fakeResponses)

      expect(record.getChangedResponses()).to.be.eql(fakeResponses)
    })

    it('it should be possible to get all changed values', function () {
      const codeA = faker.random.word()
      const changedResponses = {
        one: { responseType: records.RESPONSE_VALUE, value: faker.random.word() },
        two: { responseType: records.RESPONSE_VALUE, value: faker.random.word() },
      }
      const unChangedResponses = {
        three: { responseType: records.RESPONSE_VALUE, value: faker.random.word() },
      }

      const bundle = new RecordBundle()
      const record = bundle.addRecord(codeA)

      record.setResponses(changedResponses)
      record.setResponses(unChangedResponses, false)

      expect(record.getChangedResponses()).to.deep.equal(changedResponses)
    })

    it('it should be possible to clear all changed flags', function () {
      const codeA = faker.random.word()
      const changedResponses = {
        one: { responseType: records.RESPONSE_VALUE, value: faker.random.word() },
        two: { responseType: records.RESPONSE_VALUE, value: faker.random.word() },
      }

      const bundle = new RecordBundle()
      const record = bundle.addRecord(codeA)

      record.setResponses(changedResponses)

      expect(record.getChangedResponses()).to.be.eql(changedResponses)

      record.clearChanged()
      expect(record.getChangedResponses()).to.be.eql({})
    })

    it('should not set changed flag on cloning of records', function () {
      const codeA = faker.random.word()
      const codeB = faker.random.word()
      const fakeValue = faker.random.word()

      const bundle = new RecordBundle()

      bundle.addRecord(codeA, { code: codeA }, {
        [codeB]: { responseType: records.RESPONSE_VALUE, value: fakeValue },
      })

      const bundleClone = new RecordBundle(bundle)
      const bundleRecords = bundleClone.getRecords(codeA)

      expect(bundleRecords[0].getChangedResponses()).to.be.eql({})
    })

    it('should get all records in bundle', function () {
      const codeA = faker.random.word()
      const codeB = faker.random.word()
      const codeAValues = {
        one: faker.random.word(),
        two: faker.random.word(),
      }
      const codeBValues = {
        three: faker.random.word(),
      }
      const bundle = new RecordBundle()

      const codeARecord = bundle.addRecord(codeA)
      const codeBRecord = bundle.addRecord(codeB)

      codeARecord.setValues(codeAValues)
      codeBRecord.setValues(codeBValues)

      const bundleRecords = bundle.getAllRecords()

      expect(bundleRecords).to.be.an('object').and.to.have.property(codeA)
      expect(bundleRecords[codeA]).to.contain(codeARecord)
      expect(bundleRecords[codeB]).to.contain(codeBRecord)
    })

    it('should return only changed records and change status of records', function () {
      const codeA = faker.random.word()
      const codeB = faker.random.word()
      const codeC = faker.random.word()
      const codeAValues = {
        one: faker.random.word(),
        two: faker.random.word(),
      }
      const codeBValues = {
        three: faker.random.word(),
      }
      const codeCValues = {
        three: faker.random.word(),
      }
      const bundle = new RecordBundle()

      const codeARecord = bundle.addRecord(codeA)
      const codeBRecord = bundle.addRecord(codeB)
      const codeCRecord = bundle.addRecord(codeC)

      codeARecord.setValues(codeAValues, false)
      codeBRecord.setValues(codeBValues)
      codeCRecord.setValues(codeCValues)

      const expectedChanges = {}

      expectedChanges[codeB] = bundle.getRecords(codeB)
      expectedChanges[codeC] = bundle.getRecords(codeC)

      expect(bundle.getChanges()).to.be.eql(expectedChanges)
      expect(codeARecord.isChanged()).to.be.eql(false)
      expect(codeBRecord.isChanged()).to.be.eql(true)
    })
  })
})
