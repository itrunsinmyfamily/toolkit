const appRoot = require('app-root-path')
const faker = require('faker')
const chai = require('chai')

const expect = chai.expect

const { Filters } = appRoot.require('/dialogue/filters')
const DialogueFlow = appRoot.require('/dialogue/flow').default
const Inquiry = appRoot.require('/dialogue/inquiry').default

chai.use(require('dirty-chai'))

chai.Assertion.addMethod('DialogFlow', function () {
  const obj = this._obj

  expect(obj).to.be.an('object')
  expect(obj).to.be.an.instanceof(DialogueFlow)
  expect(obj).to.have.property('filters').to.be.a('object')
  expect(obj).to.have.property('topics').to.be.a('array')
})

chai.Assertion.addMethod('Topic', function () {
  const obj = this._obj

  expect(obj).to.be.an('object')
  expect(obj).to.have.property('filters').to.be.a('array')
  expect(obj).to.have.property('id').to.be.a('string')
})

chai.Assertion.addMethod('Inquiry', function () {
  const obj = this._obj

  expect(obj).to.be.an('object')
  expect(obj).to.be.an.instanceof(Inquiry)
  expect(obj).to.have.property('topic').to.be.an('object')
  expect(obj).to.have.property('state').to.be.an('object')
  expect(obj).to.have.property('filters').to.be.an('object')
  expect(obj).to.have.property('type').to.be.a('string')
})

describe('Dialogue', function () {
  describe('Filters', function () {
    it('are all functions', function (done) {
      Object.keys(Filters).forEach(name => {
        expect(typeof Filters[name]).to.equal('function')
      })

      done()
    })
  })

  describe('DialogueFlow', function () {
    it('accepts topics on construction', function (done) {
      const topics = { test: { type: 'choice', content: faker.random.word(), options: [] } }
      const flow = new DialogueFlow({ topics })

      const topic = flow.findTopic('test')

      expect(topic).to.be.a.Topic()
      expect(topic.id).to.equal('test')
      expect(topic.content).to.equal(topics.test.content)

      done()
    })

    it('finds topic by topicId', function (done) {
      const topics = { test: { type: 'choice', content: faker.random.word(), options: [] } }
      const flow = new DialogueFlow({ topics })

      expect(flow.findTopic('test')).to.be.a.Topic()
      expect(flow.findTopic('bogus')).to.not.be.ok()
      done()
    })

    it('locates topics by applying state conditions', function (done) {
      const numval = faker.random.number()
      const topics = { test: { type: 'choice', content: faker.random.word(), options: [] } }

      topics.test.conditions = `someStateKey == ${numval}`

      const flow = new DialogueFlow({ topics })
      const topic = flow.findTopicByState({ someStateKey: numval })

      expect(topic).to.be.a.Topic()
      expect(topic.id).to.equal('test')
      expect(topic.content).to.equal(topics.test.content)

      done()
    })

    it('fails to locate topics when no topics declare conditions', function (done) {
      const numval = faker.random.number()
      const topics = { test: { type: 'choice', content: faker.random.word(), options: [] } }

      const flow = new DialogueFlow({ topics })
      const topic = flow.findTopicByState({ someStateKey: numval })

      expect(topic).to.not.be.ok()

      done()
    })

    it('fails to locate topics when state conditions do not align', function (done) {
      const numval = faker.random.number()
      const topics = { test: { type: 'choice', content: faker.random.word(), options: [] } }

      topics.test.conditions = `someStateKey == ${numval + 1}`

      const flow = new DialogueFlow({ topics })
      const topic = flow.findTopicByState({ someStateKey: numval })

      expect(topic).to.not.be.ok()

      done()
    })

    it('creates an Inquiry by topicId', function (done) {
      const topics = { test: { type: 'choice', content: faker.random.word(), options: [] } }

      const flow = new DialogueFlow({ topics })
      const inquiry = flow.startInquiry('test')


      expect(inquiry).to.be.an.Inquiry()
      expect(inquiry.topic.id).to.equal('test')
      expect(inquiry.content).to.equal(topics.test.content)

      done()
    })

    it('throws on Inquiry by invalid topicId', function (done) {
      const topics = { test: { type: 'choice', content: faker.random.word(), options: [] } }
      const flow = new DialogueFlow({ topics })

      expect(() => flow.startInquiry('bogus')).to.throw()
      done()
    })

    it('creates an Inquiry by applying state conditions', function (done) {
      const numval = faker.random.number()
      const topics = { test: { type: 'choice', content: faker.random.word(), options: [] } }

      topics.test.conditions = `someStateKey == ${numval}`

      const flow = new DialogueFlow({ topics })
      const inquiry = flow.nextInquiry({ someStateKey: numval })

      expect(inquiry).to.be.an.Inquiry()
      expect(inquiry.topic.id).to.equal('test')
      expect(inquiry.content).to.equal(topics.test.content)

      done()
    })
  })

  describe('Inquiry', function () {
    beforeEach(function (done) {
      this.topics = {
        test: {
          type: 'choice',
          content: faker.random.word(),
          options: [
            { id: 'aaa', label: 'AAAA', extra: 'AAA' },
            { id: 'bbb', label: '{{ "BBB" }}', extra: 'BBB' },
          ],
        },
      }
      this.flow = new DialogueFlow({ topics: this.topics })

      done()
    })

    it('evaluates expressions', function (done) {
      const inquiry = this.flow.startInquiry('test')
      const numval = faker.random.number(20)

      expect(inquiry.eval('42 == 42')).to.equal(true)
      expect(inquiry.eval('43 > 41')).to.equal(true)
      expect(inquiry.eval('"test"')).to.equal('test')

      expect(inquiry.eval('someKey == "someVal"', { someKey: 'someVal' })).to.equal(true)
      expect(inquiry.eval(`someSq(${numval}) + 1`, { someSq: v => v * v }))
        .to.equal((numval * numval) + 1)

      done()
    })

    it('passes through extraneous option properties during interpolation', function (done) {
      const inquiry = this.flow.startInquiry('test')

      const options = inquiry.options

      expect(options.length).to.equal(this.topics.test.options.length)
      expect(options[0].extra).to.equal(this.topics.test.options[0].extra)
      expect(options[1].extra).to.equal(this.topics.test.options[1].extra)

      done()
    })

    it('interpolates strings', function (done) {
      const numval = faker.random.number(20)
      const wordval = faker.random.word()
      const inquiry = this.flow.startInquiry('test', {
        someKey: numval,
        someWord: wordval,
        nesting: { key: wordval },
      })

      expect(inquiry.interpolate(wordval)).to.equal(wordval)
      expect(inquiry.interpolate('{{someWord}}')).to.equal(wordval)
      expect(inquiry.interpolate('MY:{{nesting.key}}')).to.equal(`MY:${wordval}`)
      expect(inquiry.interpolate('{{toUpperCase(someWord)}}')).to.equal(wordval.toUpperCase())
      expect(inquiry.interpolate(`{{ ${numval} * ${numval} }}`)).to.equal(`${numval * numval}`)
      expect(inquiry.interpolate('SOMEKEY: {{ someKey }}')).to.equal(`SOMEKEY: ${numval}`)
      expect(inquiry.interpolate('{{ 111 }} {{ 222 }} {{ 333 }}')).to.equal('111 222 333')

      done()
    })

    it('interpolates multiline strings', function (done) {
      const inquiry = this.flow.startInquiry('test', {})

      expect(inquiry.interpolate('{{"line1\nline2"}}')).to.equal('line1\nline2')
      expect(inquiry.interpolate('{{"line1\nline2"}} {{"line2\nline3"}}'))
        .to.equal('line1\nline2 line2\nline3')
      expect(inquiry.interpolate('{{"line1\n\nline2"}}')).to.equal('line1\n\nline2')
      expect(inquiry.interpolate('{{ 3 \n + \n 4 + 5}}')).to.equal('12')

      done()
    })

    it('deeply interpolates strings in objects', function (done) {
      const numval = faker.random.number(20)
      const wordval = faker.random.word()
      const inquiry = this.flow.startInquiry('test', { someKey: numval, someWord: wordval })

      expect(inquiry.interpolateAll({ key: '{{"foo"}}' })).to.deep.equal({ key: 'foo' })
      expect(inquiry.interpolateAll({ key: { key2: 'X{{ someKey }}X' } }))
        .to.deep.equal({ key: { key2: `X${numval}X` } })
      expect(inquiry.interpolateAll({ key: { key2: 'Y{{someKey}}Y' }, str: 'WORD {{someWord}}' }))
        .to.deep.equal({ key: { key2: `Y${numval}Y` }, str: `WORD ${wordval}` })

      done()
    })

    it('interpolates topic properties', function (done) {
      const wordval = faker.random.word()
      const numval = faker.random.number(20)
      const topics = {
        test: {
          type: 'choice',
          content: 'Magic Number: {{ 21 * 2 }}',
          question: 'Magic Word: {{someWord}}',
          options: [{ id: 'ok', label: 'Magic Key: {{someKey}}' }],
        },
      }
      const flow = new DialogueFlow({ topics })
      const inquiry = flow.startInquiry('test', { someKey: numval, someWord: wordval })

      expect(inquiry.content).to.equal('Magic Number: 42')
      expect(inquiry.question).to.equal(`Magic Word: ${wordval}`)
      expect(inquiry.options[0].label).to.equal(`Magic Key: ${numval}`)

      done()
    })

    it('supports filtered responses', function (done) {
      this.flow.addTopic('filtered', { type: 'text', filters: ['toUpperCase(response)'] })

      const inquiry = this.flow.startInquiry('filtered')

      expect(inquiry.filterResponse('someval')).to.equal('SOMEVAL')
      done()
    })

    it('supports validating responses', function (done) {
      this.flow.addTopic('validated', { type: 'text', validate: 'length response > 4' })

      const inquiry = this.flow.startInquiry('validated')

      expect(inquiry.validateResponse('someval')).to.equal(null)
      expect(inquiry.validateResponse('moo')).to.equal('length response > 4')

      done()
    })

    it('processes responses', function (done) {
      const strval = faker.random.word()

      this.flow.addTopic('respond',
        { type: 'text', actions: [{ id: 'done', value: 'R:{{response}}' }] })

      const inquiry = this.flow.startInquiry('respond')
      const actions = inquiry.handleResponse(strval)

      expect(inquiry.response).to.equal(strval)
      expect(actions).to.deep.equal([{ id: 'done', value: `R:${strval}` }])

      done()
    })
    it('interpolates null value of an object', function (done) {
      const inquiry = this.flow.startInquiry('test', {})

      expect(inquiry.interpolateAll({ key: null })).to.deep.equal({ key: null })

      done()
    })
  })
})
