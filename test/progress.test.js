const appRoot = require('app-root-path')
const faker = require('faker')
const chai = require('chai')

const expect = chai.expect

const progress = appRoot.require('./progress').default
const records = appRoot.require('./records').default

function setPhr(response) {
  const code = 'Life'
  const bundle = new records.RecordBundle()
  const record = bundle.addRecord(code)

  Object.keys(response).forEach(key => {
    record.setResponse(key, {
      responseType: response[key].responseType,
      value: response[key].value || undefined,
    })
  })
  return bundle
}

describe('family tree progress', function () {
  it('should return same progress for all users', function (done) {
    const created = new Date()
    const result = progress.calculateFamilyTree(created)

    expect(result).to.deep.equal({
      updated: created,
      score: 7,
      total: 7,
    })

    done()
  })
})

describe('family info progress', function () {
  it('calculate correct progress for each patient and currentPatient', function (done) {
    const currentPatientId = faker.random.number()
    const patientIdOne = faker.random.number()
    const patientIdTwo = faker.random.number()
    const patientIdThree = faker.random.number()
    const fakeResponses = [
      {
        'first-name': {
          responseType: records.RESPONSE_VALUE,
          value: faker.random.word(),
        },
      },
      {
        'first-name': {
          responseType: records.RESPONSE_VALUE,
          value: faker.random.word(),
        },
        'birth-year': {
          responseType: records.RESPONSE_VALUE,
          value: faker.random.number({
            min: 2005, max: 2016,
          }),
        },
      },
      {
        'birth-year': {
          responseType: records.RESPONSE_VALUE,
          value: faker.random.number({
            min: 1950, max: 1990,
          }),
        },
        'death-year': {
          responseType: records.RESPONSE_UNKNOWN,
        },
      },
      {
        'last-name': {
          responseType: records.RESPONSE_VALUE,
          value: faker.random.word(),
        },
        'birth-year': {
          responseType: records.RESPONSE_VALUE,
          value: faker.random.number({
            min: 1950, max: 1990,
          }),
        },
        'death-year': {
          responseType: records.RESPONSE_VALUE,
          value: faker.random.number({
            min: 1990, max: 2016,
          }),
        },
      }]

    const pedigree = {
      [currentPatientId]: {
        patientId: currentPatientId,
        PHR: setPhr(fakeResponses[0]),
      },
      [patientIdOne]: {
        patientId: patientIdOne,
        PHR: setPhr(fakeResponses[1]),
      },
      [patientIdTwo]: {
        patientId: patientIdTwo,
        PHR: setPhr(fakeResponses[2]),
      },
      [patientIdThree]: {
        patientId: patientIdThree,
        PHR: setPhr(fakeResponses[3]),
      },
    }

    const result = progress.calculateFamilyInfo(pedigree, currentPatientId)

    expect(result.score).to.equal(5)
    expect(result.total).to.equal(8)
    expect(result.members[patientIdOne].score).to.equal(2)
    expect(result.members[patientIdOne].total).to.equal(2)
    expect(result.members[patientIdTwo].score).to.equal(0)
    expect(result.members[patientIdTwo].total).to.equal(2)
    expect(result.members[patientIdThree].score).to.equal(2)
    expect(result.members[patientIdThree].total).to.equal(2)

    done()
  })
})

describe('family cancer progress', function () {
  it('calculate correct progress if workflow answered', function (done) {
    const ui = {
      confirmedFamilyHealthHistory: false,
    }
    const created = new Date()
    const result = progress.calculateFamilyCancer(created, ui)

    expect(result.score).to.equal(0)
    expect(result.total).to.equal(0)
    expect(result.updated).to.equal(null)

    done()
  })

  it('calculate correct progress if workflow answered', function (done) {
    const ui = {
      confirmedFamilyHealthHistory: true,
    }
    const created = new Date()
    const result = progress.calculateFamilyCancer(new Date(), ui)

    expect(result.score).to.equal(1)
    expect(result.total).to.equal(1)
    expect(result.updated).to.deep.equal(created)


    done()
  })
})

describe('family cancer progress', function () {
  it('calculate correct progress', function (done) {
    const bundle = new records.RecordBundle()
    const bundleOne = new records.RecordBundle()
    const bundleTwo = new records.RecordBundle()
    const currentPatientId = faker.random.number()
    const memberIdOne = faker.random.number()
    const memberIdTwo = faker.random.number()

    /* current patient bundle */
    bundle.addRecord('C3', {}, {
      status: { responseType: records.RESPONSE_VALUE, value: 'current' },
      'age-start': { responseType: records.RESPONSE_VALUE, value: faker.random.number() } })

    /* bundle for family member 1 */
    bundleOne.addRecord('C3', {}, {
      status: { responseType: records.RESPONSE_VALUE, value: 'resolved' },
      'age-start': { responseType: records.RESPONSE_VALUE, value: faker.random.number() },
      'age-end': { responseType: records.RESPONSE_VALUE, value: faker.random.number() },
      'cancer-stage': { responseType: records.RESPONSE_UNKNOWN } })

    bundleOne.addRecord('C14', {}, {
      status: { responseType: records.RESPONSE_NONE },
      laterality: { responseType: records.RESPONSE_VALUE, value: 'one' },
      'cancer-stage': { responseType: records.RESPONSE_VALUE, value: 'II' } })

    /* bundle for family member 2 */
    bundleTwo.addRecord('C11', {}, {
      status: { responseType: records.RESPONSE_REFUSED },
      'cancer-stage': { responseType: records.RESPONSE_VALUE, value: 'I' } })

    bundleTwo.addRecord('C14', {}, {
      status: { responseType: records.RESPONSE_NONE },
      laterality: { responseType: records.RESPONSE_VALUE, value: 'one' } })

    const pedigree = {
      [currentPatientId]: {
        patientId: currentPatientId,
        PHR: bundle,
      },
      [memberIdOne]: {
        patientId: memberIdOne,
        PHR: bundleOne,
      },
      [memberIdTwo]: {
        patientId: memberIdTwo,
        PHR: bundleTwo,
      },
    }

    const result = progress.calculateFamilyCancerInfo(pedigree, currentPatientId)

    expect(result.score).to.equal(1)
    expect(result.total).to.equal(4)

    done()
  })
  it('should calculate correct progress for each family member', function (done) {
    const bundleOne = new records.RecordBundle()
    const bundleTwo = new records.RecordBundle()

    const currentPatientId = faker.random.number()
    const memberIdOne = faker.random.number()
    const memberIdTwo = faker.random.number()

    /* current patient bundle */
    bundleOne.addRecord('C3', {}, {
      status: { responseType: records.RESPONSE_VALUE, value: 'current' },
      'age-start': { responseType: records.RESPONSE_VALUE, value: faker.random.number() } })

    /* bundle for family member 1 */
    bundleTwo.addRecord('C3', {}, {
      status: { responseType: records.RESPONSE_VALUE, value: 'resolved' },
    })

    bundleTwo.addRecord('C14', {}, {
      status: { responseType: records.RESPONSE_VALUE, value: 'resolved' },
      'age-start': { responseType: records.RESPONSE_VALUE, value: faker.random.number() },
    })

    const pedigree = {
      [memberIdOne]: {
        patientId: memberIdOne,
        PHR: bundleOne,
      },
      [memberIdTwo]: {
        patientId: memberIdTwo,
        PHR: bundleTwo,
      },
    }

    const result = progress.calculateFamilyCancerInfo(pedigree, currentPatientId)

    expect(result.members[memberIdOne].score).to.equal(1)
    expect(result.members[memberIdOne].total).to.equal(1)
    expect(result.members[memberIdTwo].score).to.equal(1)
    expect(result.members[memberIdTwo].total).to.equal(2)

    done()
  })
})

describe('personal progress', function () {
  it('should calculate correct score and total', function (done) {
    const code = 'Life'
    const fakeResponses = {
      height: { responseType: records.RESPONSE_VALUE, value: faker.random.number() },
      weight: { responseType: records.RESPONSE_VALUE, value: faker.random.number() },
      'eye-color': { responseType: records.RESPONSE_UNKNOWN },
      'blood-type': { responseType: records.RESPONSE_NONE },
    }

    const bundle = new records.RecordBundle()

    bundle.addRecord(code, {}, fakeResponses)
    const result = progress.calculatePersonal(bundle)

    expect(result.score).to.equal(3)
    expect(result.total).to.equal(8)

    done()
  })
})

describe('social progress', function () {
  it('should calculate correct progress for non-smoker', function (done) {
    const bundle = new records.RecordBundle()

    bundle.addRecord('Life', {}, {
      'zip-code': { responseType: records.RESPONSE_VALUE, value: faker.random.number() } })
    bundle.addRecord('Drinking', {}, { frequency: { responseType: records.RESPONSE_NONE } })
    bundle.addRecord('Smoking', {}, {
      status: { responseType: records.RESPONSE_VALUE, value: 'never' } })

    const result = progress.calculateSocial(bundle)

    expect(result.score).to.equal(2)
    expect(result.total).to.equal(3)

    done()
  })

  it('should calculate correct progress for current smoker', function (done) {
    const bundle = new records.RecordBundle()

    bundle.addRecord('Life', {}, { 'zip-code': { responseType: records.RESPONSE_NONE } })
    bundle.addRecord('Drinking', {}, {
      frequency: { responseType: records.RESPONSE_VALUE, value: faker.random.number() } })
    bundle.addRecord('Smoking', {}, {
      status: { responseType: records.RESPONSE_VALUE, value: 'current' },
      frequency: { responseType: records.RESPONSE_VALUE, value: 'everyday' },
      'age-start': { responseType: records.RESPONSE_VALUE,
        value: faker.random.number({
          min: 20, max: 30,
        }),
      },
    })
    const result = progress.calculateSocial(bundle)

    expect(result.score).to.equal(3)
    expect(result.total).to.equal(4)

    done()
  })

  it('should calculate correct progress for former smoker', function (done) {
    const bundle = new records.RecordBundle()

    bundle.addRecord('Life', {}, {
      'zip-code': { responseType: records.RESPONSE_VALUE, value: faker.random.number() } })
    bundle.addRecord('Smoking', {}, {
      status: { responseType: records.RESPONSE_VALUE, value: 'resolved' },
      'age-start': { responseType: records.RESPONSE_VALUE,
        value: faker.random.number({
          min: 20, max: 30,
        }),
      },
      'age-end': { responseType: records.RESPONSE_NONE },
    })
    const result = progress.calculateSocial(bundle)

    expect(result.score).to.equal(3)
    expect(result.total).to.equal(5)

    done()
  })
})
