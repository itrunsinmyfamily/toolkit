/* eslint prefer-template: "off" */
const appRoot = require('app-root-path')
const faker = require('faker')
const chai = require('chai')

const expect = chai.expect

const records = appRoot.require('./records').default
const rules = appRoot.require('/rules').default
const types = appRoot.require('/types').default

chai.use(require('dirty-chai'))

describe('Rules', function () {
  it('should validate value structures', function (done) {
    const wellformed = [
      { responseType: records.RESPONSE_VALUE, value: 10 },
      { responseType: records.RESPONSE_VALUE, value: 'xxx' },
      { responseType: records.RESPONSE_VALUE, value: new Date() },
      { responseType: records.RESPONSE_NONE },
      { responseType: records.RESPONSE_UNKNOWN },
      { responseType: records.RESPONSE_REFUSED },
    ]

    const malformed = [
      { responseType: records.RESPONSE_VALUE },
      { responseType: records.RESPONSE_VALUE, value: null },
      { responseType: records.RESPONSE_VALUE, value: 3.43 },
      { responseType: records.RESPONSE_VALUE, value: true },
      { responseType: records.RESPONSE_VALUE, value: [1, 2] },
      { responseType: records.RESPONSE_VALUE, value: {} },
      { responseType: records.RESPONSE_NONE, value: 'xxx' },
      { responseType: records.RESPONSE_UNKNOWN, value: 5 },
      { responseType: 'bogus' },
    ]

    wellformed.forEach(response => {
      expect(rules.validate(response), JSON.stringify(response)).to.be.undefined()
    })
    malformed.forEach(response => {
      expect(rules.validate(response), JSON.stringify(response)).to.be.an('error')
    })

    done()
  })

  it('should evaluate value response type', function (done) {
    const passing = [
      [{ responseType: records.RESPONSE_VALUE, value: 0 }, { }],
      [{ responseType: records.RESPONSE_VALUE, value: 0 }, { response: [records.RESPONSE_VALUE] }],
      [{ responseType: records.RESPONSE_NONE }, { response: [records.RESPONSE_NONE] }],
      [{ responseType: records.RESPONSE_UNKNOWN }, { response: [records.RESPONSE_UNKNOWN] }],
      [{ responseType: records.RESPONSE_REFUSED }, { response: [records.RESPONSE_REFUSED] }],
      [{ responseType: records.RESPONSE_VALUE, value: 0 },
        { response: [records.RESPONSE_VALUE, records.RESPONSE_UNKNOWN, records.RESPONSE_NONE] }],
      [{ responseType: records.RESPONSE_UNKNOWN }, { response: records.RESPONSES }],
    ]

    const failing = [
      [{ responseType: records.RESPONSE_NONE }, { response: [records.RESPONSE_VALUE] }],
      [{ responseType: records.RESPONSE_UNKNOWN }, { response: [records.RESPONSE_VALUE] }],
      [{ responseType: records.RESPONSE_REFUSED },
        { response: [records.RESPONSE_VALUE, records.RESPONSE_UNKNOWN] }],
    ]

    passing.forEach(pair => {
      expect(rules.evaluate(...pair), JSON.stringify(pair)).to.be.true()
    })
    failing.forEach(pair => {
      expect(rules.evaluate(...pair), JSON.stringify(pair)).to.be.false()
    })

    done()
  })

  it('should evaluate value data type', function (done) {
    const passing = [
      [{ responseType: records.RESPONSE_VALUE, value: faker.random.number() }, { }],
      [{ responseType: records.RESPONSE_VALUE, value: faker.random.number() },
        { type: types.TYPE_NUMERIC }],
      [{ responseType: records.RESPONSE_VALUE, value: faker.lorem.word() },
        { type: types.TYPE_TEXT }],
      [{ responseType: records.RESPONSE_VALUE, value: faker.date.past() },
        { type: types.TYPE_DATETIME }],
    ]

    const failing = [
      [{ responseType: records.RESPONSE_VALUE, value: faker.random.number() },
        { type: types.TYPE_TEXT }],
      [{ responseType: records.RESPONSE_VALUE, value: faker.lorem.word() },
        { type: types.TYPE_NUMERIC }],
      [{ responseType: records.RESPONSE_VALUE, value: faker.lorem.word() },
        { type: types.TYPE_DATETIME }],
    ]

    passing.forEach(pair => {
      expect(rules.evaluate(...pair), JSON.stringify(pair)).to.be.true()
    })
    failing.forEach(pair => {
      expect(rules.evaluate(...pair), JSON.stringify(pair)).to.be.false()
    })

    done()
  })

  it('should evaluate value conditions', function (done) {
    const passing = [
      [{ responseType: records.RESPONSE_VALUE, value: faker.random.word() },
       { value: { } }],
      [{ responseType: records.RESPONSE_VALUE, value: 'static' },
       { value: { neq: 'dynamic' } }],
      [{ responseType: records.RESPONSE_VALUE, value: 'static' },
       { value: { eq: 'static' } }],
      [{ responseType: records.RESPONSE_VALUE, value: 10 },
        { value: { btw: [9, 11] } }],
      [{ responseType: records.RESPONSE_VALUE, value: new Date() },
        { value: { gt: faker.date.past() } }],
    ]

    const failing = [
      [{ responseType: records.RESPONSE_VALUE, value: 'static' },
       { value: { eq: 'dynamic' } }],
      [{ responseType: records.RESPONSE_VALUE, value: 'static' },
       { value: { neq: 'static' } }],
      [{ responseType: records.RESPONSE_VALUE, value: 8 },
        { value: { btw: [9, 11] } }],
      [{ responseType: records.RESPONSE_VALUE, value: new Date() },
        { value: { lt: faker.date.past() } }],
    ]

    passing.forEach(pair => {
      expect(rules.evaluate(...pair), JSON.stringify(pair)).to.be.true()
    })
    failing.forEach(pair => {
      expect(rules.evaluate(...pair), JSON.stringify(pair)).to.be.false()
    })

    done()
  })

  it('should report failure causes', function (done) {
    const failures = []

    rules.evaluate({ responseType: records.RESPONSE_UNKNOWN }, {}, failures)
    expect(failures).to.be.empty()

    failures.length = 0
    rules.evaluate({ responseType: records.RESPONSE_UNKNOWN }, { response: [] }, failures)
    expect(failures).to.deep.equal([{ cause: 'response', expected: [], received: 'unknown' }])

    failures.length = 0
    rules.evaluate({ responseType: records.RESPONSE_VALUE, value: 42 },
      { type: types.TYPE_TEXT }, failures)
    expect(failures).to.deep.equal([{ cause: 'type', expected: 'text', received: 'numeric' }])

    failures.length = 0
    rules.evaluate({ responseType: records.RESPONSE_VALUE, value: 42 },
      { value: { gt: 50 } }, failures)
    expect(failures).to.deep.equal([{
      cause: 'value',
      conditions: ['gt'],
      expected: { gt: 50 },
      received: 42,
    }])

    done()
  })
})
