/* eslint no-underscore-dangle: "off" */

const RESPONSE_NONE = 'none'
const RESPONSE_VALUE = 'value'
const RESPONSE_UNKNOWN = 'unknown'
const RESPONSE_REFUSED = 'refused'
const RESPONSES = [RESPONSE_NONE, RESPONSE_VALUE, RESPONSE_UNKNOWN, RESPONSE_REFUSED]

export default {
  RESPONSE_NONE,
  RESPONSE_VALUE,
  RESPONSE_UNKNOWN,
  RESPONSE_REFUSED,
  RESPONSES,

  RecordBundle,
  RecordBundleRecord,
}

function RecordBundle(bundle) {
  this.__records = {}
  Object.assign(this, bundle)
  Object.keys(this.__records).forEach(code => {
    this.__records[code].forEach((record, i) => {
      this.__records[code][i] = new module.exports.default.RecordBundleRecord(record)
    })
  })
}

function RecordBundleRecord(record) {
  this.__changes = {}
  this.__responses = {}
  Object.assign(this, record)
}

RecordBundle.prototype.addRecord = function addRecord(code, source = {}, responses = {}) {
  const records = this.__records[code] = this.__records[code] || []
  const record = new module.exports.default.RecordBundleRecord(source)

  record.setResponses(responses, false)

  records.push(record)
  return record
}

RecordBundle.prototype.getRecords = function getRecords(code) {
  return this.__records[code] || []
}

RecordBundle.prototype.getAllRecords = function getAllRecords() {
  return Object.assign({}, this.__records)
}

RecordBundle.prototype.getChanges = function getChanges() {
  const changedrecords = {}

  Object.keys(this.__records).forEach(key => {
    const records = this.__records[key]
    const changedBundles = []

    records.forEach(record => {
      if (record.isChanged()) {
        changedBundles.push(record)
      }
    })
    if (changedBundles.length > 0) {
      changedrecords[key] = changedBundles
    }
  })

  return changedrecords
}

RecordBundleRecord.prototype.setResponse = function setResponse(code, response, flagChanged) {
  if (RESPONSES.indexOf(response.responseType) < 0) {
    throw new Error(`Invalid responseType ${response.responseType} ` +
                    `in response ${JSON.stringify(response)}`)
  }

  if (flagChanged !== false) {
    this.__changes[code] = true
  }
  this.__responses[code] = response
}

RecordBundleRecord.prototype.setValue = function setValue(code, value, flagChanged) {
  return this.setResponse(code, { responseType: RESPONSE_VALUE, value }, flagChanged)
}


RecordBundleRecord.prototype.setResponses = function setResponses(responses, flagChanged) {
  Object.keys(responses).forEach(code => {
    this.setResponse(code, responses[code], flagChanged)
  })
}

RecordBundleRecord.prototype.setValues = function setValues(values, flagChanged) {
  Object.keys(values).forEach(code => {
    this.setValue(code, values[code], flagChanged)
  })
}

RecordBundleRecord.prototype.getResponse = function getResponse(code) {
  return this.__responses[code]
}

RecordBundleRecord.prototype.getValue = function getValue(code) {
  const response = this.getResponse(code)

  if (response && response.responseType === RESPONSE_VALUE) {
    return response.value
  }

  return undefined
}

RecordBundleRecord.prototype.isChanged = function isChanged() {
  return Object.keys(this.__changes).length > 0
}

RecordBundleRecord.prototype.getChangedResponses = function getChangedResponses() {
  const changed = {}

  Object.keys(this.__changes).forEach(code => {
    changed[code] = this.__responses[code]
  })

  return changed
}

RecordBundleRecord.prototype.clearChanged = function clearChanged() {
  this.__changes = {}
}

RecordBundleRecord.prototype.getResponses = function getResponses() {
  return this.__responses
}

RecordBundleRecord.prototype.getValues = function getValues() {
  const values = {}

  Object.keys(this.__responses).forEach(code => {
    const response = this.__responses[code]

    if (response.responseType === RESPONSE_VALUE) {
      values[code] = response.value
    }
  })

  return values
}
