const records = require('./records').default

export default {
  calculate,
  calculateFamilyTree,
  calculateFamilyInfo,
  calculateFamilyCancer,
  calculateFamilyCancerInfo,
  calculatePersonal,
  calculateSocial,
}

function calculateConceptAttributes(currentPatient, results, healthCreated, member) {
  const patientInfo = {
    total: 0,
    score: 0,
  }
  const restricted = ['Life', 'Smoking', 'Drinking', 'Patient']
  const included = []
  const HEALTH_SCORE_TOPICS = ['age-start']
  const updatedResults = results
  const patientRecords = currentPatient.getAllRecords()

  Object.keys(patientRecords).forEach(conceptId => {
    if (restricted.indexOf(conceptId) === -1) {
      patientRecords[conceptId].forEach(recordInstance => {
        patientInfo.total++
        updatedResults.total++
        included.push(recordInstance)
      })
    }
  })

  included.forEach(disease => {
    const ageStartResponse = disease.getResponse('age-start')

    if (ageStartResponse && ageStartResponse.responseType !== records.RESPONSE_NONE) {
      patientInfo.score++
      updatedResults.score++
    }

    if (disease.values) {
      const healthUpdated = disease.values.filter(l =>
        (HEALTH_SCORE_TOPICS.indexOf(l.attributeId) > -1)).map(l => l.created)

      if (healthUpdated.length) {
        healthCreated.push(healthUpdated.map(p => new Date(p))
          .sort((dateOne, dateTwo) => dateTwo.getTime() - dateOne.getTime())[0])
      }
    }
  })

  if (healthCreated.length) {
    updatedResults.updated = healthCreated.map(p => new Date(p))
  .sort((dateOne, dateTwo) => dateTwo.getTime() - dateOne.getTime())[0]
  }
  updatedResults.members[member.patientId] = patientInfo
  return updatedResults
}

function convertPedigree(pedigree) {
  const patients = []

  Object.keys(pedigree).forEach(patientId => {
    if (pedigree[patientId].PHR) {
      patients.push({ accountId: pedigree[patientId].accountId,
        patientId: pedigree[patientId].patientId,
        PHR: new records.RecordBundle(pedigree[patientId].PHR) })
    }
  })
  return patients
}

function calculate(state) {
  const progressResult = {}
  const currentPatientBundle = new records.RecordBundle(state.pedigree[state.account.patientId].PHR)

  progressResult.familyTree = calculateFamilyTree(state.account.created)
  progressResult.familyInfo = calculateFamilyInfo(state.pedigree)
  progressResult.personalHealth =
      calculateFamilyCancer(state.account.created, state.ui)
  progressResult.familyHealth =
      calculateFamilyCancerInfo(state.pedigree, state.account.patientId, state.account.created)
  progressResult.personal = calculatePersonal(currentPatientBundle)
  progressResult.social = calculateSocial(currentPatientBundle)
  progressResult.score = progressResult.familyTree.score + progressResult.familyInfo.score +
      progressResult.personalHealth.score + progressResult.familyHealth.score +
      progressResult.personal.score + progressResult.social.score
  progressResult.total = progressResult.familyTree.total + progressResult.familyInfo.total +
      progressResult.personalHealth.total + progressResult.familyHealth.total +
      progressResult.personal.total + progressResult.social.total
  return progressResult
}

function calculateFamilyTree(created) {
  return {
    updated: created,
    score: 7,
    total: 7,
  }
}

function calculateFamilyInfo(pedigree) {
  const members = convertPedigree(pedigree)
  const results = {
    updated: null,
    score: 0,
    total: 0,
    members: {},
  }
  const FAMILYINFO_SCORE_TOPICS = ['first-name', 'last-name', 'birth-year', 'death-year']
  const familyInfoCreated = []

  members.forEach(member => {
    const patientInfo = {
      total: 0,
      score: 0,
    }

    const life = member.PHR.getRecords('Life')[0]
    const firstNameResponse = life.getResponse('first-name')
    const lastNameResponse = life.getResponse('last-name')
    const birthYearResponse = life.getResponse('birth-year')
    const deathYearResponse = life.getResponse('death-year')
    const deathYearValue = life.getValue('death-year')
    const aliveStatus = !(deathYearResponse &&
                          deathYearResponse.responseType !== records.RESPONSE_NONE)

    results.total++
    patientInfo.total++
    if ((firstNameResponse && firstNameResponse.responseType !== records.RESPONSE_NONE) ||
    (lastNameResponse && lastNameResponse.responseType !== records.RESPONSE_NONE)) {
      results.score++
      patientInfo.score++
    }

    results.total++
    patientInfo.total++
    if (aliveStatus) {
      if (birthYearResponse && birthYearResponse.responseType !== records.RESPONSE_NONE) {
        results.score++
        patientInfo.score++
      }
    } else if (birthYearResponse &&
               birthYearResponse.responseType !== records.RESPONSE_NONE && deathYearValue) {
      results.score++
      patientInfo.score++
    }

    results.members[member.patientId] = patientInfo

    if (life.values) {
      const familyInfoUpdated = life.values.filter(l =>
          (FAMILYINFO_SCORE_TOPICS.indexOf(l.attributeId) > -1)).map(l => l.created)

      if (familyInfoUpdated.length) {
        familyInfoCreated.push(familyInfoUpdated.map(p => new Date(p))
            .sort((dateOne, dateTwo) => dateTwo.getTime() - dateOne.getTime())[0])
      }
    }
  })

  if (familyInfoCreated.length) {
    results.updated = familyInfoCreated.map(p => new Date(p))
      .sort((dateOne, dateTwo) => dateTwo.getTime() - dateOne.getTime())[0]
  }
  return results
}

function calculateFamilyCancer(created, ui) {
  const results = {
    updated: null,
    score: 0,
    total: 0,
  }

  if (ui.confirmedFamilyHealthHistory) {
    results.score++
    results.total++
    results.updated = created
  }
  return results
}

function calculateFamilyCancerInfo(statePedigree, patientId, created) {
  const pedigree = convertPedigree(statePedigree)
  const results = {
    updated: created,
    score: 0,
    total: 0,
    members: {},
  }

  const familyHealthScore = pedigree.filter(p => p.patientId !== patientId)
    .map(member => calculateConceptAttributes(member.PHR, results, [], member))

  if (!familyHealthScore.length) {
    return results
  }
  return familyHealthScore[0]
}

function calculatePersonal(bundlePatient) {
  const PERSONAL_SCORE_TOPICS = ['height', 'weight', 'eye-color', 'hair-color',
    'skin-tone', 'handedness', 'blood-type', 'rh-type']
  const results = {
    updated: null,
    score: 0,
    total: PERSONAL_SCORE_TOPICS.length,
  }
  const lifeRecord = bundlePatient.getRecords('Life')[0]

  if (!lifeRecord) {
    return results
  }
  PERSONAL_SCORE_TOPICS.forEach(key => {
    const response = lifeRecord.getResponse(key)

    if (response && response.responseType !== records.RESPONSE_NONE) {
      results.score++
    }
  })
  if (lifeRecord.values) {
    const personalUpdated = lifeRecord.values.filter(l =>
        (PERSONAL_SCORE_TOPICS.indexOf(l.attributeId) > -1)).map(l => l.created)

    if (personalUpdated.length) {
      results.updated = personalUpdated.map(p => new Date(p))
    .sort((dateOne, dateTwo) => dateTwo.getTime() - dateOne.getTime())[0]
    }
  }

  return results
}

function calculateSocial(bundlePatient) {
  const SOCIAL_SCORE_TOPICS = ['zip-code', 'frequency', 'smoking-values']
  const SMOKING_TOPICS = ['status', 'frequency', 'age-start', 'age-end']
  const socialUpdated = []
  const results = {
    updated: null,
    score: 0,
    total: SOCIAL_SCORE_TOPICS.length,
  }
  const lifeRecord = bundlePatient.getRecords('Life')[0]
  const smokingRecord = bundlePatient.getRecords('Smoking')[0]
  const drinkingRecord = bundlePatient.getRecords('Drinking')[0]

  if (drinkingRecord) {
    const response = drinkingRecord.getResponse('frequency')

    if (response && response.responseType !== records.RESPONSE_NONE) {
      results.score++
    }

    if (drinkingRecord.values) {
      const drinkingCreated = drinkingRecord.values.map(l => l.created)

      if (drinkingCreated.length) {
        socialUpdated.push(drinkingCreated)
      }
    }
  }

  if (lifeRecord) {
    const response = lifeRecord.getResponse('zip-code')

    if (response && response.responseType !== records.RESPONSE_NONE) {
      results.score++
    }
    if (lifeRecord.values) {
      const lifeCreated = lifeRecord.values.filter(l =>
          (SOCIAL_SCORE_TOPICS.indexOf(l.attributeId) > -1)).map(l => l.created)

      if (lifeCreated.length) {
        socialUpdated.push(lifeCreated)
      }
    }
  }

  if (smokingRecord) {
    const response = smokingRecord.getResponse('status')

    if (response && response.responseType !== records.RESPONSE_NONE) {
      if (response.responseType === records.RESPONSE_VALUE) {
        const status = smokingRecord.getValue('status')

        results.score++
        if (status === 'current') {
          results.total++
          const frequencyResponse = smokingRecord.getResponse('frequency')

          if (frequencyResponse && frequencyResponse.responseType !== records.RESPONSE_NONE) {
            if (frequencyResponse.responseType === records.RESPONSE_VALUE) {
              const frequency = smokingRecord.getValue('frequency')

              if (frequency === 'everyday'
                  || frequency === 'some-days') {
                const ageResponse = smokingRecord.getResponse('age-start')

                if (ageResponse && ageResponse.responseType !== records.RESPONSE_NONE) {
                  results.score++
                }
              }
            } else {
              results.score++
            }
          }
        } else if (status === 'resolved') {
          results.total++
          const ageStartResponse = smokingRecord.getResponse('age-start')
          const ageEndResponse = smokingRecord.getResponse('age-end')

          if (ageStartResponse && ageStartResponse.responseType !== records.RESPONSE_NONE) {
            results.score++
          }
          results.total++
          if (ageEndResponse && ageEndResponse.responseType !== records.RESPONSE_NONE) {
            results.score++
          }
        }
      } else {
        results.score++
      }
    }
    if (smokingRecord.values) {
      const smokingCreated = smokingRecord.values.filter(l =>
          (SMOKING_TOPICS.indexOf(l.attributeId) > -1)).map(l => l.created)

      if (smokingCreated.length) {
        socialUpdated.push(smokingCreated)
      }
    }
  }

  if (socialUpdated.length) {
    results.updated = socialUpdated.map(p => new Date(p))
  .sort((dateOne, dateTwo) => dateTwo.getTime() - dateOne.getTime())[0]
  }
  return results
}
