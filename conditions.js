const types = require('./types').default

const OP_EQ = 'eq'
const OP_NEQ = 'neq'
const OP_GT = 'gt'
const OP_LT = 'lt'
const OP_GTE = 'gte'
const OP_LTE = 'lte'
const OP_BTW = 'btw'
const OP_INQ = 'inq'
const OP_NIN = 'nin'
const OP_REGEX = 're'

const OPERATORS = [OP_EQ, OP_NEQ, OP_GT, OP_LT, OP_GTE, OP_LTE, OP_BTW, OP_INQ, OP_NIN, OP_REGEX]

const INDEXOF_NOT_FOUND = -1

const OP_BTW_VAL_LEN = 2
const OP_REGEX_VAL_LEN = 2

// Validators check whether values match the appropriate shape for their operators
const VALIDATORS = {
  [OP_EQ]: types.isSimpleType,
  [OP_NEQ]: types.isSimpleType,
  [OP_GT]: types.isSimpleType,
  [OP_LT]: types.isSimpleType,
  [OP_GTE]: types.isSimpleType,
  [OP_LTE]: types.isSimpleType,
  [OP_BTW]: value => Array.isArray(value) && value.length === OP_BTW_VAL_LEN,
  [OP_INQ]: Array.isArray,
  [OP_NIN]: Array.isArray,
  [OP_REGEX]: value => {
    if (!Array.isArray(value)) {
      return (typeof value) === 'string'
    }

    // Optionally support two-parameter array, [regexp, flags]
    return value.length === OP_REGEX_VAL_LEN && value.every(v => (typeof v) === 'string')
  },
}

// Typechecks ensure that subject and value conform to required types
const TYPECHECKS = {
  [OP_EQ]: typesMatch,
  [OP_NEQ]: typesMatch,
  [OP_GT]: typesMatch,
  [OP_LT]: typesMatch,
  [OP_GTE]: typesMatch,
  [OP_LTE]: typesMatch,
  [OP_BTW]: (subject, value) => typesMatch(subject, ...value),
  [OP_INQ]: (subject, value) => typesMatch(subject, ...value),
  [OP_NIN]: (subject, value) => typesMatch(subject, ...value),
  [OP_REGEX]: subject => ((typeof subject) === 'string'),
}

// Evaluators actually execute the comparison
const EVALUATORS = {
  [OP_EQ]: (subject, value) => {
    if (subject instanceof Date) {
      return subject.getTime() === value.getTime()
    }

    return subject === value
  },
  [OP_NEQ]: (subject, value) => {
    if (subject instanceof Date) {
      return subject.getTime() !== value.getTime()
    }

    return subject !== value
  },
  [OP_GT]: (subject, value) => subject > value,
  [OP_LT]: (subject, value) => subject < value,
  [OP_GTE]: (subject, value) => subject >= value,
  [OP_LTE]: (subject, value) => subject <= value,
  [OP_BTW]: (subject, value) => subject >= value[0] && subject <= value[1],
  [OP_INQ]: (subject, value) => value.indexOf(subject) !== INDEXOF_NOT_FOUND,
  [OP_NIN]: (subject, value) => value.indexOf(subject) === INDEXOF_NOT_FOUND,
  [OP_REGEX]: (subject, value) => {
    // Optionally support two-parameter array, [regexp, flags]
    const re = Array.isArray(value) ? new RegExp(...value) : new RegExp(value)

    return re.test(subject)
  },
}

export default {
  OPERATORS,
  VALIDATORS,
  TYPECHECKS,
  EVALUATORS,

  validateSet: function validateSet(conditionSet) {
    return Object.keys(conditionSet)
      .map(op => this.validate(op, conditionSet[op]))
      .filter(err => !!err)
  },

  validate: function validate(op, value) {
    if (!VALIDATORS[op]) {
      return new Error(`Invalid operator '${op}'`)
    }
    if (!VALIDATORS[op](value)) {
      return new Error(`Validation failure at ${op} ${value}`)
    }

    return undefined
  },

  // Expects a subject and an object mapping operators to values
  evaluateSet: function evaluateSet(subject, conditions, failures = []) {
    const failed = Object.keys(conditions).filter(op =>
      !this.evaluate(subject, op, conditions[op])
    )

    failures.push(...failed)
    return failed.length === 0
  },

  evaluate: function evaluate(subject, op, value) {
    const err = this.validate(op, value)

    if (err) {
      throw err
    }

    if (subject === undefined) {
      return false
    }

    if (!TYPECHECKS[op](subject, value)) {
      throw new Error(`Typecheck failure at ${subject} ${op} ${value}`)
    }

    return EVALUATORS[op](subject, value)
  },
}

// Checks whether all parameters are the same type
function typesMatch(...params) {
  if (!params.length) {
    return true
  }

  const type = typeof params[0]

  if (type === 'number') {
    // Enforce not only that they are all numbers but that they are ints
    return params.every(Number.isInteger)
  }

  return params.every(p => (typeof p) === type)
}
