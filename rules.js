const conditions = require('./conditions').default
const types = require('./types').default

const records = require('./records').default

// eslint-disable-next-line import/prefer-default-export
const rules = {
  validate: value => {
    const responseType = value.responseType
    const valueAssigned = value.value !== undefined && value.value !== null

    if (records.RESPONSES.indexOf(responseType) < 0) {
      return new Error(`Malformed value: Missing or invalid responseType '${responseType}'`)
    }

    if (valueAssigned && responseType !== records.RESPONSE_VALUE) {
      // For non-value responses no value must be set
      return new Error(
        `Malformed value: Value must not be set for responseType '${responseType}'`)
    }

    if (!valueAssigned && responseType === records.RESPONSE_VALUE) {
      // For value responses a value must be set
      return new Error(
        `Malformed value: Value must be set for responseType '${responseType}'`)
    }

    if (valueAssigned && !types.getSimpleType(value.value)) {
      // Values must be a recognized type
      return new Error(
        `Malformed value: Value '${value.value}' is not of supported type`)
    }

    return undefined
  },

  evaluate: (value, ruleset, failures = []) => {
    const err = rules.validate(value)

    if (err) {
      throw err
    }

    const responseType = value.responseType
    const valueType = types.getSimpleType(value.value)

    if (ruleset.response && (ruleset.response.indexOf(responseType) < 0)) {
      failures.push({ cause: 'response', expected: ruleset.response, received: responseType })
      return false
    }

    // Type and value checks only apply to value responses
    if (responseType !== records.RESPONSE_VALUE) {
      return true
    }

    // Value type must match the ruleset type
    if (ruleset.type && ruleset.type !== valueType) {
      failures.push({ cause: 'type', expected: ruleset.type, received: valueType })
      return false
    }

    const conditionFailures = []

    // Value itself must meet the value conditions
    if (ruleset.value && !conditions.evaluateSet(value.value, ruleset.value, conditionFailures)) {
      failures.push({
        cause: 'value',
        conditions: conditionFailures,
        expected: ruleset.value,
        received: value.value })
      return false
    }

    return true
  },
}

export default rules
