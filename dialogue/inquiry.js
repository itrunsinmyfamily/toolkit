const Parser = require('expr-eval').Parser

class Inquiry {
  constructor(topic, state = {}, filters = {}) {
    this.topic = topic
    this.state = state
    this.filters = filters

    this.type = topic.type
    this.response = undefined
    this.actions = undefined
  }

  get question() {
    return this.interpolate(this.topic.question)
  }

  get content() {
    return this.interpolate(this.topic.content)
  }

  get options() {
    if (Array.isArray(this.topic.options)) {
      return this.topic.options.map(o => Object.assign({}, o, { label: this.interpolate(o.label) }))
    }

    return this.topic.options
  }

  filterResponse(response) {
    this.topic.filters.forEach(filter => {
      // eslint-disable-next-line no-param-reassign
      response = this.eval(filter, { response })
    })

    return response
  }

  validateResponse(response) {
    if (this.type === 'choice') {
      const option = this.topic.options.find(o => o.id === response)

      if (!option) {
        return `Invalid option: ${response}`
      }
    }

    if (!this.topic.validate) {
      return null
    }

    const result = this.eval(this.topic.validate, { response })

    if (!result) {
      return this.topic.validate
    }

    return null
  }

  handleResponse(response) {
    const actions = []

    if (this.topic.actions) {
      actions.push(...this.topic.actions)
    }

    if (this.type === 'choice') {
      const option = this.topic.options.find(o => o.id === response)

      if (option.actions) {
        actions.push(...option.actions)
      }
    }

    this.response = response

    this.actions = actions.filter(a => !a.if || this.eval(a.if))
                          .map(a => this.interpolateAll(a))

    return this.actions
  }

  interpolateAll(obj) {
    const result = {}

    Object.keys(obj).forEach(k => {
      if (obj[k] === null) {
        result[k] = obj[k]
        return
      }
      switch (typeof obj[k]) {
        case 'object':
          result[k] = this.interpolateAll(obj[k])
          break
        case 'string':
          result[k] = this.interpolate(obj[k])
          break
        default:
          result[k] = obj[k]
      }
    })

    return result
  }

  eval(expression, additionalParams) {
    const params = Object.assign({}, this.state, this.filters,
                                 { response: this.response }, additionalParams)

    return Parser.evaluate(expression, params)
  }

  interpolate(input) {
    if (!input) {
      return input
    }

    let result = input
    const matches = input.match(/{{([\s\S]*?)}}/g)

    if (!matches) {
      return result
    }

    matches.forEach(match => {
      const expression = match.replace(/^{{([\s\S]+)}}$/, '$1')

      result = result.replace(match, this.eval(expression))
    })

    return result
  }
}

export { Inquiry as default }
