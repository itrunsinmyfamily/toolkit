// eslint-disable-next-line import/prefer-default-export
export const Filters = {
  trim: value => (typeof value === 'string' ? value.trim() : value),
  parseInt: value => parseInt(value, 10),
  parseFloat: value => parseFloat(value),
  toUpperCase: value => (typeof value === 'string' ? value.toUpperCase() : value),
  toLowerCase: value => (typeof value === 'string' ? value.toLowerCase() : value),
  isSet: value => !!value,
  isNumeric: value => typeof value === 'number' && !Number.isNaN(value),
  isString: value => typeof value === 'string',
  isInteger: value => Filters.isNumeric(value) && Number.isInteger(value),
  getCurrentYear: () => (new Date()).getUTCFullYear(),
}
