const { Filters } = require('./filters')
const Inquiry = require('./inquiry').default
const Parser = require('expr-eval').Parser

class DialogueFlow {
  constructor(script, extensions = {}) {
    this.filters = Object.assign({}, Filters, extensions.filters)

    this.topics = []
    Object.keys(script.topics).forEach(topicId => this.addTopic(topicId, script.topics[topicId]))
  }

  addTopic(topicId, props) {
    const topic = Object.assign({ filters: [] }, props, { id: topicId })

    if (topic.content) {
      topic.content = topic.content.trim()
    }
    if (topic.question) {
      topic.question = topic.question.trim()
    }

    this.topics.push(topic)
  }

  findTopic(topicId) {
    return this.topics.find(t => t.id === topicId)
  }

  findTopicByState(state) {
    // Search for the first topic that satisfies the given state,
    // ignoring topics without conditions.
    return this.topics.find(topic => {
      if (topic.conditions) {
        return Parser.evaluate(topic.conditions, Object.assign({}, state, this.filters))
      }

      return false
    })
  }

  startInquiry(topicId, state) {
    const topic = this.findTopic(topicId)

    if (!topic) {
      throw new Error(`Unknown topic '${topicId}'!`)
    }

    return new Inquiry(topic, state, this.filters)
  }

  nextInquiry(state) {
    const topic = this.findTopicByState(state)

    if (topic) {
      return this.startInquiry(topic.id, state)
    }

    return null
  }
}

export { DialogueFlow as default }
