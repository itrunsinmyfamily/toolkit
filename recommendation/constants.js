/* eslint no-param-reassign: ["error", { "props": false }] */
const generations = {
  gen1: ['parent.parent.parent', 'parent.parent.parent.child'],
  gen2: ['parent.parent', 'parent.parent.child'],
  gen3: ['parent.parent.child', 'parent.parent.child.child'],
  gen4: ['parent', 'parent.child'],
  gen5: ['parent.child', 'parent.child.child'],
  gen6: ['parent.child.child', 'parent.child.child.child'],
  gen7: ['child', 'child.child'],
  gen8: ['child.child', 'child.child.child'],
  gen9: ['parent', 'parent.parent'],
}

export default {
  generations,
}
