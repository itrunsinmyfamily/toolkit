const generations = require('./constants').default.generations
const records = require('../records').default

const cancerCodes = ['C81', 'C25', 'C29', 'C30']
const MIN_CANCER_PATIENTS = 3
const MAX_ONSET_VALUE = 50
const FIRST_DEG_LINKS = 2

export default function amsterdamCriteria(pedigree, currentPatientId) {
  const cancerPatients = []
  let ageStartCheck = false

  Object.keys(pedigree).forEach(patientId => {
    if (patientId === currentPatientId) return

    const phr = new records.RecordBundle(pedigree[patientId].PHR)

    // eslint-disable-next-line no-underscore-dangle
    Object.keys(phr.__records).forEach(conceptId => {
      if (cancerCodes.indexOf(conceptId) === -1) return

      if (cancerPatients.indexOf(pedigree[patientId]) === -1) {
        const record = phr.getRecords(conceptId)[0]

        cancerPatients.push(pedigree[patientId])
        if (record.getValue('age-start') < MAX_ONSET_VALUE && !ageStartCheck) {
          ageStartCheck = true
        }
      }
    })
  })

  if (cancerPatients.length < MIN_CANCER_PATIENTS || !ageStartCheck) {
    return { amsterdamCriteria: false }
  }

  const successiveGenerations = findSuccessiveGen(cancerPatients, pedigree, currentPatientId)

  if (!successiveGenerations) return { amsterdamCriteria: false }

  const firstDegreeCheck = checkFirstDegreeRel(cancerPatients)

  if (!firstDegreeCheck) return { amsterdamCriteria: false }

  return { amsterdamCriteria: true }
}

function findSuccessiveGen(patients, pedigree, currentPatientId) {
  const parentOneGen = []
  const parentTwoGen = []
  const probandsRel = pedigree[currentPatientId].relations
  const [parentOne, parentTwo] = probandsRel.parent
  const parentOneRel = pedigree[parentOne].relations

  patients.forEach(patient => {
    if (patient.patientId === parentOne) {
      parentOneGen.push('parent')
      return
    }

    if (patient.patientId === parentTwo) {
      parentTwoGen.push('parent')
      return
    }

    const relation = Object.keys(probandsRel).find(rel =>
      probandsRel[rel].indexOf(patient.patientId) > -1)

    const isRelatedToParentOne = Object.keys(parentOneRel).find(rel =>
      parentOneRel[rel].indexOf(patient.patientId) > -1)

    if (isRelatedToParentOne) {
      parentOneGen.push(relation)
      return
    }

    parentTwoGen.push(relation)
  })
  return hasSuccessiveGen(parentOneGen, parentTwoGen)
}

function hasSuccessiveGen(parentOneGen, parentTwoGen) {
  let successiveGenCheck = false

  Object.keys(generations).forEach(gen => {
    if (successiveGenCheck) return
    successiveGenCheck = parentOneGen.indexOf(generations[gen][0]) > -1 &&
      parentOneGen.indexOf(generations[gen][1]) > -1

    if (!successiveGenCheck) {
      successiveGenCheck = parentTwoGen.indexOf(generations[gen][0]) > -1 &&
        parentTwoGen.indexOf(generations[gen][1]) > -1
    }
  })

  return successiveGenCheck
}

function checkFirstDegreeRel(patients) {
  const relationsFound = patients.find(patient => {
    const relFound = [patient.patientId]
    const parents = patient.relations.parent
    const children = patient.relations.child
    const siblings = patient.relations['parent.child']
    const firstDegreeRel = [].concat(parents, children, siblings)
    const list = patients.filter(pat => pat !== patient)

    list.forEach(relative => {
      if (firstDegreeRel.indexOf(relative.patientId) > -1) {
        relFound.push(relative.patientId)
      }
    })
    return relFound.length > FIRST_DEG_LINKS
  })

  if (relationsFound) return true

  return false
}
