const helpers = require('./helpers')
const records = require('../records').default

module.exports = (pedigree, probandId, invite) => {
  const familyHistory = {
    resourceType: 'List',
    id: 'genetic',
    status: 'current',
    mode: 'working',
    contained: [],
  }
  const inviterType = invite.patientId ? 'your relative' : 'your doctor'
  const inviter = invite.invitedBy || inviterType
  const formatConditions = patientConditions => {
    if (!patientConditions.length) return null

    return patientConditions.map(patientCondition => {
      const condition = {
        note: 'NASK',
        code: {
          coding: [{
            code: patientCondition.code,
            system: 'ItRuns',
            display: patientCondition.code,
          }],
        },
        onsetAge: 'ASKU',
      }

      if (patientCondition.onsetAge) {
        condition.onsetAge = {
          value: patientCondition.onsetAge,
          unit: 'years',
          system: 'http://unitsofmeasure.org',
        }
      }
      if (patientCondition.status && patientCondition.status === records.RESPONSE_VALUE) {
        condition.outcome = 'not resolved'
      } else if (!patientCondition.status || patientCondition.status === records.RESPONSE_NONE) {
        condition.outcome = 'resolved'
      } else {
        condition.outcome = 'ASKU'
      }

      return condition
    })
  }

  Object.keys(pedigree).forEach(patientId => {
    if (!pedigree[patientId].PHR) {
      return
    }
    const patient = pedigree[patientId]
    const patientInfo = helpers.personInfo(patient)

    familyHistory.contained.push({
      resourceType: 'FamilyMemberHistory',
      identifier: patient.patientId,
      definition: 'Questionnaire',
      status: helpers.personProgress(pedigree, patient, probandId),
      notDone: false,
      notDoneReason: 'NI',
      patient: 'NI',
      date: patientInfo.date,
      name: patientInfo.name || 'ASKU',
      relationship: helpers.relationInfo(pedigree, probandId, patient),
      gender: patientInfo.gender,
      bornDate: patientInfo.birthDate,
      ageAge: helpers.ageInfo(patient),
      estimatedAge: false,
      deceasedBoolean: patientInfo.deceased,
      reasonCode: `Invited to complete family health history by ${inviter}`,
      reasonReference: 'Questionnaire Response',
      note: 'NASK',
      condition: formatConditions(patientInfo.conditions) || 'UNK',
    })
  })
  return familyHistory
}
