const hls = require('./hls')
const fhir = require('./fhir.js')

module.exports = {
  HL7: hls,
  fhir,
}
