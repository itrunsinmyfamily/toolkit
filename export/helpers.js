const records = require('../records').default
const progress = require('../progress').default
const mapping = require('./mappings')

const getAge = (dYear, dMonth, dDay, bYear, bMonth, bDay) => {
  const today = new Date()
  let age = null
  let monthDiff
  let dayCheck

  if (bYear) {
    age = dYear ? (dYear - bYear) : (today.getFullYear() - bYear)
    if (bMonth && bDay) {
      if (dMonth && dDay) {
        monthDiff = dMonth - bMonth
        dayCheck = dDay < bDay
      } else {
        monthDiff = (today.getMonth() + 1) - bMonth
        dayCheck = today.getDate() < bDay
      }

      if (monthDiff <= 0 && dayCheck) {
        age--
      }
    }
  }
  return age
}

const getBirthDate = (bYear, bMonth, bDay) => {
  if (bYear && bMonth && bDay) {
    const bDate = new Date(bYear, bMonth - 1, bDay + 1)

    return bDate
  }
  return 'ASKU'
}

const getConditions = currentPatient => {
  const restricted = ['Life', 'Smoking', 'Drinking', 'Patient']
  const conditions = []
  const included = []
  const patientRecords = currentPatient.getAllRecords()

  Object.keys(patientRecords).forEach(conceptId => {
    if (restricted.indexOf(conceptId) === -1) {
      patientRecords[conceptId].forEach(recordInstance => {
        included.push(recordInstance)
      })
    }
  })

  included.forEach(disease => {
    const ageStartValue = disease.getValue('age-start')
    const status = disease.getResponse('status')
    const condition = {
      code: disease.conceptId,
      onsetAge: ageStartValue,
      status,
    }

    conditions.push(condition)
  })

  return conditions
}
const personProgress = (pedigree, member, proband) => {
  const familyInfo = progress.calculateFamilyInfo(pedigree)
  const infoProgress = familyInfo.members[member.patientId]
  const PHR = new records.RecordBundle(member.PHR)
  const percentMax = 100
  const percentMin = 0
  let result = null

  if (member.patientId === proband) {
    const personalInfo = progress.calculatePersonal(PHR)
    const socialInfo = progress.calculateSocial(PHR)
    const score = personalInfo.score + socialInfo.score + infoProgress.score
    const total = personalInfo.total + socialInfo.total + infoProgress.total

    result = Math.round((score / total) * percentMax)
  } else {
    const healthInfo = progress.calculateFamilyCancerInfo(pedigree, null, null)
    const healthProgress = healthInfo.members[member.patientId]
    const score = infoProgress.score + healthProgress.score
    const total = infoProgress.total + healthProgress.total

    result = Math.round((score / total) * percentMax)
  }

  if (result === percentMax) {
    return 'complete'
  } else if (result === percentMin) {
    return 'health-unknown'
  }

  return 'partial'
}

const personInfo = patient => {
  const PHR = new records.RecordBundle(patient.PHR)
  const Life = PHR.getRecords('Life')[0]
  const deathYearResponse = Life.getResponse('death-year')
  const deathYear = Life.getValue('death-year')
  const deathMonth = Life.getValue('death-month')
  const deathDay = Life.getValue('death-day')
  const birthYear = Life.getValue('birth-year')
  const birthMonth = Life.getValue('birth-month')
  const birthDay = Life.getValue('birth-day')
  const age = getAge(deathYear, deathMonth, deathDay, birthYear, birthMonth, birthDay)
  const conditions = getConditions(PHR)

  return {
    date: Life.modified,
    name: [Life.getValue('first-name'), Life.getValue('last-name')].join(' ').trim(),
    gender: Life.getValue('gender'),
    birthDate: getBirthDate(birthYear, birthMonth, birthDay),
    birthTime: [birthYear, birthMonth, birthDay].join('').trim(),
    deceased: !!(deathYearResponse && deathYearResponse.responseType !== records.RESPONSE_NONE),
    age,
    conditions,
  }
}

const ageInfo = member => {
  const age = personInfo(member).age

  if (!age) return 'ASKU'
  const ageData = {
    value: age,
    unit: 'years',
    system: 'http://unitsofmeasure.org',
  }

  return ageData
}
const familySides = (pedigree, probandId) => {
  const relations = pedigree[probandId].relations
  const family = {
    maternal: [],
    paternal: [],
  }

  if (!relations.parent) return family
  relations.parent.forEach(parent => {
    const data = pedigree[parent]
    const display = personInfo(data).gender

    if (display === 'male') {
      family.paternal.push(parent)
      Object.keys(data.relations).forEach(relation => {
        if (relation.startsWith('parent')) {
          family.paternal.push(...data.relations[relation])
        }
      })
      return
    }

    family.maternal.push(parent)
    Object.keys(data.relations).forEach(relation => {
      if (relation.startsWith('parent')) {
        family.maternal.push(...data.relations[relation])
      }
    })
  })

  return family
}
const relationInfo = (pedigree, probandId, member) => {
  const relations = pedigree[probandId].relations
  const relationsMap = mapping.relationCodes
  const relationData = {
    coding: [{
      system: 'http://hl7.org/fhir/v3/RoleCode',
      code: 'EXT',
      display: '',
    }],
  }

  if (!relations) return relationData
  const familySidesGroup = familySides(pedigree, probandId)
  let relationPath = 'proband'

  Object.keys(relations).forEach(relation => {
    if (relations[relation].indexOf(member.patientId) > -1) {
      relationPath = relation
      return relation
    }
    return relationPath
  })

  const gender = personInfo(member).gender
  const relationship = relationsMap[relationPath][gender]
  let relationCode = null

  if (familySidesGroup.maternal.includes(member.patientId)) {
    relationCode = relationship.code.maternal
  } else {
    relationCode = relationship.code.paternal
  }
  relationData.coding[0].code = relationCode
  relationData.coding[0].display = relationship.display
  return relationData
}

module.exports = {
  personInfo,
  personProgress,
  relationInfo,
  ageInfo,
  getAge,
}
