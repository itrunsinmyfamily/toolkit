const relationCodes = {
  parent: {
    male: {
      code: {
        paternal: 'NFTH',
        maternal: 'NFTH',
      },
      display: 'father',
    },
    female: {
      code: {
        paternal: 'NMTH',
        maternal: 'NMTH',
      },
      display: 'mother',
    },
  },
  'parent.child': {
    male: {
      code: {
        paternal: 'NBRO',
        maternal: 'NBRO',
      },
      display: 'brother',
    },
    female: {
      code: {
        paternal: 'NSIS',
        maternal: 'NSIS',
      },
      display: 'sister',
    },
  },
  'parent.parent': {
    male: {
      code: {
        paternal: 'PGRFTH',
        maternal: 'MGRFTH',
      },
      display: 'grandfather',
    },
    female: {
      code: {
        paternal: 'PGRMTH',
        maternal: 'MGRMTH',
      },
      display: 'grandmother',
    },
  },
  'parent.parent.parent': {
    male: {
      code: {
        paternal: 'PGGRFTH',
        maternal: 'MGGRFTH',
      },
      display: 'great grandfather',
    },
    female: {
      code: {
        paternal: 'PGGRMTH',
        maternal: 'MGGRMTH',
      },
      display: 'great grandmother',
    },
  },
  'parent.child.child': {
    male: {
      code: {
        paternal: 'NEPHEW',
        maternal: 'NEPHEW',
      },
      display: 'nephew',
    },
    female: {
      code: {
        paternal: 'NIECE',
        maternal: 'NIECE',
      },
      display: 'niece',
    },
  },
  'parent.child.child.child': {
    male: {
      code: {
        paternal: 'EXT',
        maternal: 'EXT',
      },
      display: 'grand nephew',
    },
    female: {
      code: {
        paternal: 'EXT',
        maternal: 'EXT',
      },
      display: 'grand niece',
    },
  },
  'parent.parent.child': {
    male: {
      code: {
        paternal: 'PUNCLE',
        maternal: 'MUNCLE',
      },
      display: 'uncle',
    },
    female: {
      code: {
        paternal: 'PAUNT',
        maternal: 'MAUNT',
      },
      display: 'aunt',
    },
  },
  'parent.parent.child.child': {
    male: {
      code: {
        paternal: 'COUSN',
        maternal: 'COUSN',
      },
      display: 'cousin',
    },
    female: {
      code: {
        paternal: 'COUSN',
        maternal: 'COUSN',
      },
      display: 'cousin',
    },
  },
  'parent.parent.parent.child': {
    male: {
      code: {
        paternal: 'EXT',
        maternal: 'EXT',
      },
      display: 'great uncle',
    },
    female: {
      code: {
        paternal: 'EXT',
        maternal: 'EXT',
      },
      display: 'great aunt',
    },
  },
  child: {
    male: {
      code: {
        paternal: 'SON',
        maternal: 'SON',
      },
      display: 'son',
    },
    female: {
      code: {
        paternal: 'DAU',
        maternal: 'DAU',
      },
      display: 'daughter',
    },
  },
  'child.child': {
    male: {
      code: {
        paternal: 'GRNDSON',
        maternal: 'GRNDSON',
      },
      display: 'grandson',
    },
    female: {
      code: {
        paternal: 'GRNDDAU',
        maternal: 'GRNDDAU',
      },
      display: 'granddaughter',
    },
  },
  'child.child.child': {
    male: {
      code: {
        paternal: 'EXT',
        maternal: 'EXT',
      },
      display: 'great grandson',
    },
    female: {
      code: {
        paternal: 'EXT',
        maternal: 'EXT',
      },
      display: 'great granddaughter',
    },
  },
  spousal: {
    male: {
      code: {
        paternal: 'HUSB',
        maternal: 'HUSB',
      },
      display: 'husband',
    },
    female: {
      code: {
        paternal: 'WIFE',
        maternal: 'WIFE',
      },
      display: 'wife',
    },
  },
  proband: {
    male: {
      code: {
        paternal: 'ONESELF',
        maternal: 'ONESELF',
      },
      display: 'You',
    },
    female: {
      code: {
        paternal: 'ONESELF',
        maternal: 'ONESELF',
      },
      display: 'You',
    },
  },
}

module.exports = {
  relationCodes,
}
