const XmlWriter = require('xml-writer')
const helpers = require('./helpers')

const hlsCodeMap = {
  male: 'M',
  female: 'F',
  parent: {
    male: 'NFTH',
    female: 'NMTH',
  },
  'parent.child': {
    male: 'NBRO',
    female: 'NSIS',
  },
  'parent.parent': {
    male: 'GRFTH',
    female: 'GRMTH',
  },
  'parent.parent.parent': {
    male: 'GGRFTH',
    female: 'GGRMTH',
  },
  'parent.child.child': {
    male: 'NEPHEW',
    female: 'NIECE',
  },
  'parent.child.child.child': {
    male: 'NEPHEW',
    female: 'NIECE',
  },
  'parent.parent.child': {
    male: 'UNCLE',
    female: 'AUNT',
  },
  'parent.parent.child.child': {
    male: 'COUSN',
    female: 'COUSN',
  },
  'parent.parent.parent.child': {
    male: 'UNCLE',
    female: 'AUNT',
  },
  child: {
    male: 'SON',
    female: 'DAU',
  },
  'child.child': {
    male: 'GRNDSON',
    female: 'GRNDDAU',
  },
  'child.child.child': {
    male: 'GRNDSON',
    female: 'GRNDDAU',
  },
  spousal: {
    male: 'HUSB',
    female: 'WIFE',
  },
}

function addConditions(relative, patientInfo) {
  const conditions = patientInfo.conditions

  if (conditions && conditions.length) {
    conditions.forEach(condition => {
      const subjectConditions = relative.startElement('subjectOf2')
      .writeAttribute('typeCode', 'SBJ')
      const clinicalObservation = subjectConditions.startElement('clinicalObservation')
      .writeAttribute('classCode', 'OBS')
      .writeAttribute('moodCode', 'EVN')
      .startElement('code')
      .writeAttribute('code', condition.code)
      .writeAttribute('codeSystemName', 'ItRuns')
      .writeAttribute('displayName', condition.code)
      .endElement()

      if (condition.onsetAge) {
        clinicalObservation.startElement('subject')
        .writeAttribute('typeCode', 'SUBJ')
        .startElement('dataEstimatedAge')
        .writeAttribute('classCode', 'OBS')
        .writeAttribute('moodCode', 'EVN')
        .startElement('code')
        .writeAttribute('code', '21611-9')
        .writeAttribute('codeSystemName', 'LOINC')
        .writeAttribute('displayName', 'age at onset in years')
        .endElement()
        .startElement('value')
        .writeAttribute('value', condition.onsetAge)
        .endElement()
        .endElement()
        .endElement()
      }
      clinicalObservation.endElement()
      subjectConditions.endElement()
    })
  }
}
function addEstimatedAge(relative, patientInfo) {
  if (patientInfo.deceased && patientInfo.age) {
    relative.startElement('subjectOf1')
    .writeAttribute('typeCode', 'SBJ')
    .startElement('deceasedEstimatedAge')
    .writeAttribute('classCode', 'OBS')
    .writeAttribute('moodCode', 'EVN')
    .startElement('code')
    .writeAttribute('displayName', 'age in years')
    .endElement()
    .startElement('value')
    .writeAttribute('value', patientInfo.age)
    .endElement()
    .endElement()
    .endElement()
  }
  if (patientInfo.age) {
    relative.startElement('subjectOf1')
    .writeAttribute('typeCode', 'SBJ')
    .startElement('livingEstimatedAge')
    .writeAttribute('classCode', 'OBS')
    .writeAttribute('moodCode', 'EVN')
    .startElement('code')
    .writeAttribute('code', '21611-9')
    .writeAttribute('codeSystemName', 'LOINC')
    .writeAttribute('displayName', 'ESTIMATED AGE')
    .endElement()
    .startElement('value')
    .writeAttribute('value', patientInfo.age)
    .endElement()
    .endElement()
    .endElement()
  }
}
function createRelative(element, pedigree, patientId, type) {
  const patient = pedigree[patientId]
  const patientInfo = helpers.personInfo(patient)
  const relative = element.startElement('relative')
  .writeAttribute('classCode', 'PRS')
  const relations = patient.relations

  relative.startElement('code')
  .writeAttribute('code', hlsCodeMap[type][patientInfo.gender])
  .endElement()
  const relationshipHolder = relative.startElement('relationshipHolder')
  .writeAttribute('classCode', 'PSN')
  .writeAttribute('determinerCode', 'INSTANCE')
  .startElement('id')
  .writeAttribute('extension', patient.patientId)
  .endElement()
  .writeElement('name', patientInfo.name)
  .startElement('administrativeGenderCode')
  .writeAttribute('code', hlsCodeMap[patientInfo.gender])
  .endElement()
  .startElement('birthTime')
  .writeAttribute('value', patientInfo.birthTime)
  .endElement()
  .startElement('deceasedInd')
  .writeAttribute('value', `${patientInfo.deceased}`)
  .endElement()

  if (relations && relations.parent) {
    relations.parent.forEach(parentId => {
      createRelative(relationshipHolder, pedigree, parentId, 'parent')
    })
  }
  relationshipHolder.endElement()
  addEstimatedAge(relative, patientInfo)
  addConditions(relative, patientInfo)

  relative.endElement()
  return relative
}

module.exports = (pedigree, probandId) => {
  const builder = new XmlWriter()

  builder.startDocument()
  const proband = pedigree[probandId]
  const probandInfo = helpers.personInfo(proband)
  const relations = proband.relations
  const FamilyHistory = builder.startElement('FamilyHistory')
  .writeAttribute('classCode', 'OBS')
  .writeAttribute('moodCode', 'EVN')

  FamilyHistory.startElement('code')
  .writeAttribute('code', '10157-6')
  .writeAttribute('codeSystemName', 'LOINC')
  .endElement()

  const subject = FamilyHistory.startElement('subject')
  .writeAttribute('typeCode', 'SBJ')
  const patient = subject.startElement('patient')
  .writeAttribute('classCode', 'PAT')
  const patientPerson = patient.startElement('patientPerson')
  .writeAttribute('classCode', 'PSN')
  .writeAttribute('determinerCode', 'INSTANCE')

  patientPerson.startElement('id')
  .writeAttribute('extension', proband.patientId).endElement()
  patientPerson.writeElement('name', probandInfo.name)
  patientPerson.startElement('administrativeGenderCode')
  .writeAttribute('code', hlsCodeMap[probandInfo.gender])
  .endElement()
  patientPerson.startElement('birthTime')
  .writeAttribute('value', probandInfo.birthTime)
  .endElement()
  patientPerson.startElement('deceasedInd')
  .writeAttribute('value', `${probandInfo.deceased}`)
  .endElement()

  if (relations) {
    Object.keys(relations).forEach(relationType => {
      const patients = relations[relationType]

      patients.forEach(patientId => {
        createRelative(patientPerson, pedigree, patientId, relationType)
      })
    })
  }
  patientPerson.endElement()
  addEstimatedAge(patient, probandInfo)
  addConditions(patient, probandInfo)
  builder.endDocument()
  return builder.toString()
}
