const conditions = require('./conditions').default

export default {
  evaluate: (bundle, constraints, failures = []) => {
    if (!constraints.length) {
      return true
    }

    // Constraints must be reorganized into sets grouped by Concept. This is
    // to ensure that evaluation of constraints within the same Concept are
    // applied to a single record in the PHR, not mix/matching.
    const groupedConstraints = groupConstraints(constraints)

    // 1. For every constraint group (by concept)...
    return Object.keys(groupedConstraints).every(conceptId => {
      const constraintGroup = groupedConstraints[conceptId]
      const records = bundle.getRecords(conceptId) || []

      // 2. At least one record...
      return records.some(record =>
        // 3. Must satisfy every constraint.
        constraintGroup.every(constraint => {
          const value = record.getValue(constraint.attributeId)
          const conditionFailures = []
          const success = conditions.evaluateSet(value, constraint.value, conditionFailures)

          if (!success) {
            failures.push({
              conceptId,
              attributeId: constraint.attributeId,
              conditions: conditionFailures,
              expected: constraint.value,
            })
          }

          return success
        })
      )
    })
  },
}

function groupConstraints(constraints) {
  const bundle = {}

  constraints.forEach(constraint => {
    const conceptId = constraint.conceptId
    const constraintGroup = bundle[conceptId] = bundle[conceptId] || []

    constraintGroup.push(constraint)
  })

  return bundle
}
