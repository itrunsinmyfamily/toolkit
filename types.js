const TYPE_NUMERIC = 'numeric'
const TYPE_TEXT = 'text'
const TYPE_DATETIME = 'datetime'
const TYPES = [TYPE_NUMERIC, TYPE_TEXT, TYPE_DATETIME]

// eslint-disable-next-line import/prefer-default-export
const types = {
  TYPE_NUMERIC,
  TYPE_TEXT,
  TYPE_DATETIME,
  TYPES,

  getSimpleType: value => {
    const type = typeof value

    if (type === 'string') {
      return TYPE_TEXT
    }
    if (value instanceof Date) {
      return TYPE_DATETIME
    }
    if (type === 'number' && Number.isInteger(value)) {
      return TYPE_NUMERIC
    }

    return undefined
  },

  isSimpleType: value => !!types.getSimpleType(value),
}

export default types
